/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>

#include "dns_methods.h"

#define ME "dns"
#define string_empty(X) ((X == NULL) || (*X == '\0'))

static struct {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
} dns;

static const char* get_prefix(void) {
    amxc_var_t* prefix = amxo_parser_get_config(dns.parser, "prefix_");
    return GET_CHAR(prefix, NULL);
}

static amxd_object_t* host_template(void) {
    return amxd_dm_findf(dns.dm, "DNS.%sHost.", get_prefix());
}

static inline amxd_object_t* forwarding_template(void) {
    return amxd_dm_findf(dns.dm, "DNS.Relay.Forwarding.");
}

static inline amxd_object_t* server_template(void) {
    return amxd_dm_findf(dns.dm, "DNS.Client.Server.");
}

static amxd_status_t common_set_instance(amxd_object_t* templ, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* instance = NULL;
    amxc_var_t* interface_arg = GET_ARG(args, "Interface");
    amxc_var_t* dnsserver_arg = GET_ARG(args, "DNSServer");
    amxc_var_t* type_arg = GET_ARG(args, "Type");
    amxc_var_t* enable = GET_ARG(args, "Enable");
    const char* alias = GET_CHAR(args, "Alias");
    const char* type = GET_CHAR(type_arg, NULL);
    const char* interface = GET_CHAR(interface_arg, NULL);
    const char* dnsserver = GET_CHAR(dnsserver_arg, NULL);
    bool add_instance = GET_BOOL(args, "AddInstance");

    amxd_trans_init(&trans);

    instance = amxd_object_findf(templ, "[Alias=='%s'].", alias);

    if(!add_instance && (instance == NULL)) {
        status = amxd_status_object_not_found;
        goto exit;
    }

    if(instance == NULL) {
        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true); // Type is read-only
        amxd_trans_select_object(&trans, templ);
        amxd_trans_add_inst(&trans, 0, string_empty(alias) ? NULL : alias);
    } else {
        amxd_trans_select_object(&trans, instance);
    }

    if(interface_arg != NULL) {
        amxd_trans_set_value(cstring_t, &trans, "Interface", interface);
    }
    if(dnsserver_arg != NULL) {
        amxd_trans_set_value(cstring_t, &trans, "DNSServer", dnsserver);
    }
    if(type_arg != NULL) {
        amxd_trans_set_value(cstring_t, &trans, "Type", type);
    }
    if(enable != NULL) {
        amxd_trans_set_value(bool, &trans, "Enable", GET_BOOL(enable, NULL));
    }

    if(amxd_status_ok != amxd_trans_apply(&trans, dns.dm)) {
        SAH_TRACEZ_ERROR(ME, "Failed to set object[%s] (type=%s, dnsserver=%s, intf=%s)", alias, type, dnsserver, interface);
        status = amxd_status_invalid_function_argument;
        goto exit;
    }

    if(string_empty(alias)) {
        amxc_var_for_each(var, (&trans.retvals)) {
            alias = GETP_CHAR(var, "name");
            if(!string_empty(alias)) {
                break;
            }
        }
    }

    amxc_var_set(cstring_t, ret, alias);

    status = amxd_status_ok;

exit:
    amxd_trans_clean(&trans);
    return status;
}

static amxd_status_t common_delete_instance(amxd_object_t* templ, amxc_var_t* args, const char* templ_name) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_status_t trans_rv = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;
    const char* alias = GETP_CHAR(args, "Alias");

    amxd_trans_init(&trans);

    obj = amxd_object_findf(templ, "[Alias=='%s'].", alias);
    if(obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "No %s instance with alias '%s' to remove", templ_name, alias);
        status = amxd_status_object_not_found;
        goto exit;
    }

    amxd_trans_select_object(&trans, templ);
    amxd_trans_del_inst(&trans, 0, alias);
    trans_rv = amxd_trans_apply(&trans, dns.dm);
    when_false_trace(trans_rv == amxd_status_ok, exit, ERROR, "Transaction failed: %d", trans_rv);

    status = amxd_status_ok;
exit:
    amxd_trans_clean(&trans);
    return status;
}

amxd_status_t _SetForwarding(UNUSED amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    return common_set_instance(forwarding_template(), args, ret);
}

amxd_status_t _DeleteForwarding(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    return common_delete_instance(forwarding_template(), args, "Forwarding");
}

static const char* convert_type[] = {
    "Static",
    "DHCPv4",
    "DHCPv6",
    "IPCP",
    "RouterAdvertisement",
    "3GPP-NAS",
};

amxd_status_t _DeleteForwardings(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_status_t trans_rv = amxd_status_unknown_error;
    amxd_object_t* templ = forwarding_template();
    amxd_trans_t trans;
    const char* type = GET_CHAR(args, "Type");
    uint32_t count_servers = 0;
    uint32_t count_deletions = 0;

    amxd_trans_init(&trans);

    if(string_empty(type)) {
        type = "Static";
    } else {
        const char* type_check = NULL;
        for(size_t i = 0; i < sizeof(convert_type) / sizeof(convert_type[0]); i++) {
            if(strcmp(convert_type[i], type) == 0) {
                type_check = type;
                break;
            }
        }
        if(type_check == NULL) {
            SAH_TRACEZ_ERROR(ME, "Unknown type '%s'", type);
            status = amxd_status_invalid_function_argument;
            goto exit;
        }
    }

    amxd_trans_select_object(&trans, templ);

    amxc_var_for_each(var, GET_ARG(args, "DNSServers")) {
        const char* interface = amxc_var_key(var);
        amxc_var_for_each(subvar, var) {
            const char* dnsserver = GET_CHAR(subvar, NULL);
            amxd_object_t* obj = amxd_object_findf(templ, "[Interface=='%s' && Type=='%s' && DNSServer=='%s'].", interface, type, dnsserver);
            count_servers++;
            if(obj == NULL) {
                continue;
            }
            count_deletions++;
            amxd_trans_del_inst(&trans, amxd_object_get_index(obj), NULL);
        }
    }

    trans_rv = amxd_trans_apply(&trans, dns.dm);
    when_false_trace(trans_rv == amxd_status_ok, exit, ERROR, "Transaction failed: %d", trans_rv);

    if(count_servers != count_deletions) {
        SAH_TRACEZ_WARNING(ME, "Deletion count %u != servers-in-DNSServers count %u", count_deletions, count_servers);
    }

    status = amxd_status_ok;

exit:
    amxd_trans_clean(&trans);
    return status;
}

amxd_status_t _SetServer(UNUSED amxd_object_t* object,
                         UNUSED amxd_function_t* func,
                         amxc_var_t* args,
                         amxc_var_t* ret) {
    return common_set_instance(server_template(), args, ret);
}

amxd_status_t _DeleteServer(UNUSED amxd_object_t* object,
                            UNUSED amxd_function_t* func,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    return common_delete_instance(server_template(), args, "Server");
}

static void set_host_add_optional_args(amxd_trans_t* trans, amxc_var_t* args) {
    amxc_var_t* var = NULL;

    var = GET_ARG(args, "Interface");
    if(var != NULL) {
        amxd_trans_set_value(cstring_t, trans, "Interface", GET_CHAR(var, NULL));
    }
    var = GET_ARG(args, "Name");
    if(var != NULL) {
        amxd_trans_set_value(cstring_t, trans, "Name", GET_CHAR(var, NULL));
    }
    var = GET_ARG(args, "Origin");
    if(var != NULL) {
        amxd_trans_set_value(cstring_t, trans, "Origin", GET_CHAR(var, NULL));
    }
    var = GET_ARG(args, "Enable");
    if(var != NULL) {
        amxd_trans_set_value(bool, trans, "Enable", GET_BOOL(var, NULL));
    }
}

amxd_status_t _SetHost(UNUSED amxd_object_t* object,
                       UNUSED amxd_function_t* func,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_status_t trans_rv = amxd_status_unknown_error;
    amxd_object_t* templ = host_template();
    amxd_object_t* host = NULL;
    amxd_trans_t trans;
    amxc_var_t* ip_addresses = GET_ARG(args, "IPAddresses");
    const char* alias = GET_CHAR(args, "Alias");
    bool exclusive = GET_BOOL(args, "Exclusive");

    amxd_trans_init(&trans);

    amxc_var_cast(ip_addresses, AMXC_VAR_ID_LIST);

    if(!string_empty(alias)) {
        host = amxd_object_findf(templ, "[Alias=='%s'].", alias);
    }
    if(host == NULL) {
        amxd_trans_select_object(&trans, templ);
        amxd_trans_add_inst(&trans, 0, alias);

        set_host_add_optional_args(&trans, args);

        amxd_trans_select_pathf(&trans, ".IPAddress.");
        amxc_var_for_each(ip_address, ip_addresses) {
            amxd_trans_add_inst(&trans, 0, NULL);
            amxd_trans_set_value(cstring_t, &trans, "IPAddress", GET_CHAR(ip_address, NULL));
            amxd_trans_select_pathf(&trans, ".^");
        }

        trans_rv = amxd_trans_apply(&trans, dns.dm);
        when_false_trace(trans_rv == amxd_status_ok, exit, ERROR, "Transaction failed: %d", trans_rv);

        if(string_empty(alias)) {
            amxc_var_for_each(var, (&trans.retvals)) {
                alias = GETP_CHAR(var, "name");
                if(!string_empty(alias)) { // is there a better way?
                    break;
                }
            }
        }
    } else {
        amxd_trans_select_object(&trans, host);

        set_host_add_optional_args(&trans, args);

        amxd_trans_select_pathf(&trans, ".IPAddress.");

        if(exclusive) {
            amxd_object_for_each(instance, it, amxd_object_findf(host, ".%s.", "IPAddress")) {
                amxd_object_t* obj = amxc_llist_it_get_data(it, amxd_object_t, it);
                amxd_trans_del_inst(&trans, 0, amxd_object_get_name(obj, AMXD_OBJECT_NAMED));
            }
            amxc_var_for_each(ip_address, ip_addresses) {
                amxd_trans_add_inst(&trans, 0, NULL);
                amxd_trans_set_value(cstring_t, &trans, "IPAddress", GET_CHAR(ip_address, NULL));
                amxd_trans_select_pathf(&trans, ".^");
            }
            trans_rv = amxd_trans_apply(&trans, dns.dm);
            when_false_trace(trans_rv == amxd_status_ok, exit, ERROR, "Transaction failed: %d", trans_rv);
        } else {
            amxc_var_for_each(ip_address, ip_addresses) {
                const char* buf = GET_CHAR(ip_address, NULL);
                if(NULL == amxd_object_findf(host, "IPAddress.[IPAddress == '%s']", buf)) {
                    amxd_trans_add_inst(&trans, 0, NULL);
                    amxd_trans_set_value(cstring_t, &trans, "IPAddress", buf);
                    amxd_trans_select_pathf(&trans, ".^");
                }
            }
            trans_rv = amxd_trans_apply(&trans, dns.dm);
            when_false_trace(trans_rv == amxd_status_ok, exit, ERROR, "Transaction failed: %d", trans_rv);
        }
    }

    amxc_var_set(cstring_t, ret, alias);

    status = amxd_status_ok;
exit:
    amxd_trans_clean(&trans);
    return status;
}

amxd_status_t _RemoveHost(UNUSED amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    return common_delete_instance(host_template(), args, "Host");
}

amxd_status_t _RemoveIPFromHost(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_status_t trans_rv = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_object_t* host = NULL;
    amxc_var_t* ip_addresses = GET_ARG(args, "IPAddresses");
    const char* alias = GET_CHAR(args, "Alias");
    uint32_t count = 0;

    amxd_trans_init(&trans);
    amxc_var_cast(ip_addresses, AMXC_VAR_ID_LIST);

    host = amxd_object_findf(host_template(), "[Alias=='%s'].", alias);
    if(host == NULL) {
        SAH_TRACEZ_ERROR(ME, "No Host instance with alias '%s' to remove", alias);
        status = amxd_status_object_not_found;
        goto exit;
    }

    amxd_trans_select_object(&trans, host);
    amxd_trans_select_pathf(&trans, ".IPAddress.");

    amxc_var_for_each(ip_address, ip_addresses) {
        const char* ip_address_str = GET_CHAR(ip_address, NULL);
        amxd_object_t* obj = amxd_object_findf(host, "IPAddress.[IPAddress == '%s']", ip_address_str);
        if(obj != NULL) {
            amxd_trans_del_inst(&trans, 0, amxd_object_get_name(obj, AMXD_OBJECT_NAMED));
            count++;
        }
    }

    if(count == 0) {
        SAH_TRACEZ_ERROR(ME, "No Host instance with alias '%s' and '%s' to remove", alias, GET_CHAR(args, "IPAddresses"));
        status = amxd_status_object_not_found;
        goto exit;
    }

    trans_rv = amxd_trans_apply(&trans, dns.dm); // allowed to fail if instance with alias doesn't exist?
    when_false_trace(trans_rv == amxd_status_ok, exit, ERROR, "Transaction failed: %d", trans_rv);

    status = amxd_status_ok;
exit:
    amxd_trans_clean(&trans);
    return status;
}

static bool compare_type(const char* mode, const char* type) {
    return (type != NULL) && (strcmp(mode, type) == 0);
}

static bool compare_dynamic(UNUSED const char* mode, const char* type) {
    return (type != NULL) && (strcmp(type, "Static") != 0);
}

static bool compare_any(UNUSED const char* mode, UNUSED const char* type) {
    return true;
}

amxd_status_t _SetMode(UNUSED amxd_object_t* object,
                       UNUSED amxd_function_t* func,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_status_t trans_rv = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_object_t* templ = forwarding_template();
    const char* mode = GET_CHAR(args, "Mode");
    bool (* compare)(const char*, const char*) = NULL;
    bool enable = false;

    SAH_TRACEZ_WARNING(ME, "DNS.SetMode() is deprecated and this api will be removed, use DNS.Relay.Config.i.[IPv6]DNSMode");

    when_str_empty_trace(mode, exit, ERROR, "Missing mode");

    if(strcmp(mode, "Any") == 0) {
        compare = compare_any;
    } else if(strcmp(mode, "Dynamic") == 0) {
        compare = compare_dynamic;
    } else {
        compare = compare_type;
    }

    amxd_trans_init(&trans);

    enable = compare_type(mode, "Static");
    amxc_var_for_each(var, GET_ARG(args, "DNSServers")) {
        const char* interface = amxc_var_key(var);
        amxc_var_for_each(subvar, var) {
            const char* dnsserver = GET_CHAR(subvar, NULL);
            amxd_object_t* obj = amxd_object_findf(templ, "[Type=='Static' && DNSServer=='%s' && Interface=='%s'].", dnsserver, interface);
            if(obj != NULL) {
                continue;
            }
            amxd_trans_select_object(&trans, templ);
            amxd_trans_add_inst(&trans, 0, NULL);
            amxd_trans_set_value(cstring_t, &trans, "Interface", interface);
            amxd_trans_set_value(cstring_t, &trans, "DNSServer", dnsserver);
            amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
            amxd_trans_set_value(bool, &trans, "Enable", enable);
        }
    }

    amxd_object_for_each(instance, it, forwarding_template()) {
        amxd_object_t* obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        const char* type = GET_CHAR(amxd_object_get_param_value(obj, "Type"), NULL);
        const char* tag = GET_CHAR(amxd_object_get_param_value(obj, "Tag"), NULL);
        if((tag != NULL) && (strcmp(tag, "Failover") == 0)) {
            continue; // should the failover servers also be enabled/disabled?
        }

        amxd_trans_select_object(&trans, obj);
        amxd_trans_set_value(bool, &trans, "Enable", compare(mode, type));
    }

    trans_rv = amxd_trans_apply(&trans, dns.dm);
    amxd_trans_clean(&trans);
    when_false_trace(trans_rv == amxd_status_ok, exit, ERROR, "Transaction failed: %d", trans_rv);

    status = amxd_status_ok;

exit:
    return status;
}

int _dns_methods_main(int reason,
                      amxd_dm_t* dm,
                      amxo_parser_t* parser) {
    int rv = -1;

    switch(reason) {
    case AMXO_START:
        dns.dm = dm;
        dns.parser = parser;
        rv = 0;
        break;
    case AMXO_STOP:
        dns.dm = NULL;
        dns.parser = NULL;
        rv = 0;
        break;
    }

    return rv;
}
