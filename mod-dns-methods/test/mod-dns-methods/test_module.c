/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include "mock.h"
#include "mock_dns.h"

#include "test_module.h"

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;

int test_setup(void** state) {
    dns_unit_test_setup(state);
    parser = amxut_bus_parser();
    dm = amxut_bus_dm();

    return 0;
}

int test_teardown(void** state) {
    dns_unit_test_teardown(state);
    parser = NULL;
    dm = NULL;

    return 0;
}

static inline void expect_check_forwarding_add(const char* alias, const char* dnsserver, const char* interface, const char* type, bool enable) {
    expect_check(_unit_test_forwarding_added, data, data_equal_check, new_add_forwarding(alias, dnsserver, interface, type, enable));
}

static inline void expect_check_forwarding_changed(const char* alias, const char* dnsserver, const char* interface, bool enable) {
    expect_check(_unit_test_forwarding_changed, data, data_equal_check, new_change_forwarding(alias, dnsserver, interface, enable));
}

static inline void expect_check_forwarding_removed(const char* alias, const char* dnsserver, const char* interface, const char* type, bool enable) {
    expect_check(_unit_test_forwarding_removed, data, data_equal_check, new_remove_forwarding(alias, dnsserver, interface, type, enable));
}

static inline void expect_check_server_added(const char* alias, const char* dnsserver, const char* interface, const char* type, bool enable) {
    expect_check(_unit_test_server_added, data, data_equal_check, new_add_server(alias, interface, dnsserver, type, enable));
}

static inline void expect_check_server_changed(const char* alias, const char* dnsserver, const char* interface, bool enable) {
    expect_check(_unit_test_server_changed, data, data_equal_check, new_change_server(alias, dnsserver, interface, enable));
}

static inline void expect_check_server_removed(const char* alias, const char* dnsserver, const char* interface, const char* type, bool enable) {
    expect_check(_unit_test_server_removed, data, data_equal_check, new_remove_server(alias, dnsserver, interface, type, enable));
}

static inline void expect_check_host_added(const char* alias, const char* dnsserver, const char* interface, const char* origin, bool enable) {
    expect_check(_unit_test_host_added, data, data_equal_check, new_add_host(alias, interface, dnsserver, origin, enable));
}

static inline void expect_check_host_changed(const char* alias, const char* dnsserver, const char* interface, const char* origin, bool enable) {
    expect_check(_unit_test_host_changed, data, data_equal_check, new_change_host(alias, dnsserver, interface, origin, enable));
}

static inline void expect_check_host_removed(const char* alias, const char* dnsserver, const char* interface, const char* origin, bool enable) {
    expect_check(_unit_test_host_removed, data, data_equal_check, new_remove_host(alias, dnsserver, interface, origin, enable));
}

static inline void expect_check_host_address_added(const char* alias, const char* ip_address) {
    expect_check(_unit_test_host_address_added, data, data_equal_check, new_add_host_address(alias, ip_address));
}

static inline void expect_check_host_address_removed(const char* alias, const char* ip_address) {
    expect_check(_unit_test_host_address_removed, data, data_equal_check, new_remove_host_address(alias, ip_address));
}

void test_set_forwarding(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", "forwarding-1");
    amxc_var_add_key(bool, &args, "AddInstance", true);

    print_message("Call SetForwarding with Alias should pass if AddInstance==true\n");
    rv = amxb_call(bus, "DNS.", "SetForwarding", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_forwarding_add("forwarding-1", "", "", "", false);
    amxut_bus_handle_events();

    assert_non_null(amxd_dm_findf(dm, "DNS.Relay.Forwarding.forwarding-1"));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_forwarding_to_change_instance(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", "forwarding-1");
    amxc_var_add_key(cstring_t, &args, "DNSServer", "8.8.8.8");
    amxc_var_add_key(cstring_t, &args, "Interface", "Device.IP.Interface.2.");
    amxc_var_add_key(bool, &args, "Enable", true);

    print_message("Call SetForwarding with Alias to modify existing instance\n");
    rv = amxb_call(bus, "DNS.", "SetForwarding", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_forwarding_changed("forwarding-1", "8.8.8.8", "Device.IP.Interface.2.", true);
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_forwarding_to_change_type(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", "forwarding-1");
    amxc_var_add_key(cstring_t, &args, "Type", "DHCPv4");

    rv = amxb_call(bus, "DNS.", "SetForwarding", &args, &ret, 1);
    assert_int_not_equal(rv, AMXB_STATUS_OK);
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_delete_forwarding(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", "forwarding-1");

    print_message("Call DeleteForwarding with Alias\n");
    rv = amxb_call(bus, "DNS.", "DeleteForwarding", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_forwarding_removed("forwarding-1", "8.8.8.8", "Device.IP.Interface.2.", "", true);
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_server(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", "server-1");
    amxc_var_add_key(bool, &args, "AddInstance", true);

    print_message("Call SetServer with Alias should pass if AddInstance==true\n");
    rv = amxb_call(bus, "DNS.", "SetServer", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_server_added("server-1", "", "", "", false);
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_server_to_change_instance(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", "server-1");
    amxc_var_add_key(cstring_t, &args, "DNSServer", "8.8.8.8");
    amxc_var_add_key(cstring_t, &args, "Interface", "Device.IP.Interface.2.");
    amxc_var_add_key(bool, &args, "Enable", true);

    print_message("Call SetServer with Alias to modify existing instance\n");
    rv = amxb_call(bus, "DNS.", "SetServer", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_server_changed("server-1", "8.8.8.8", "Device.IP.Interface.2.", true);
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_delete_server(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", "server-1");

    print_message("Call DeleteServer with Alias\n");
    rv = amxb_call(bus, "DNS.", "DeleteServer", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_server_removed("server-1", "8.8.8.8", "Device.IP.Interface.2.", "", true);
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_host(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", "host-1");

    print_message("Call SetHost with Alias should pass\n");
    rv = amxb_call(bus, "DNS.", "SetHost", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_host_added("host-1", "", "", "", false);
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_host_to_change_instance(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", "host-1");
    amxc_var_add_key(cstring_t, &args, "Name", "example.com");
    amxc_var_add_key(cstring_t, &args, "Interface", "Device.Logical.Interface.3.");
    amxc_var_add_key(cstring_t, &args, "Origin", "Static");
    amxc_var_add_key(cstring_t, &args, "IPAddresses", "192.168.1.100,192.168.1.101,192.168.1.102");
    amxc_var_add_key(bool, &args, "Enable", true);

    print_message("Call SetHost with Alias should pass\n");
    rv = amxb_call(bus, "DNS.", "SetHost", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_host_changed("host-1", "example.com", "Device.Logical.Interface.3.", "Static", true);
    expect_check_host_address_added("host-1", "192.168.1.100");
    expect_check_host_address_added("host-1", "192.168.1.101");
    expect_check_host_address_added("host-1", "192.168.1.102");
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_remove_ip_from_host(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", "host-1");
    amxc_var_add_key(cstring_t, &args, "IPAddresses", "192.168.1.101");

    print_message("Call RemoveIPFromHost with Alias should pass\n");
    rv = amxb_call(bus, "DNS.", "RemoveIPFromHost", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_host_address_removed("host-1", "192.168.1.101");
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_host_to_change_address_exclusive(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    const char* alias = "host-1";
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", alias);
    amxc_var_add_key(cstring_t, &args, "IPAddresses", "192.168.1.101,192.168.1.103");
    amxc_var_add_key(bool, &args, "Exclusive", true);

    print_message("Call SetHost with Alias should pass\n");
    rv = amxb_call(bus, "DNS.", "SetHost", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_host_address_removed(alias, "192.168.1.100");
    expect_check_host_address_removed(alias, "192.168.1.102");
    expect_check_host_address_added(alias, "192.168.1.101");
    expect_check_host_address_added(alias, "192.168.1.103");
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_host_add_ip_address(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    const char* alias = "host-1";
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", alias);
    amxc_var_add_key(cstring_t, &args, "IPAddresses", "192.168.1.102");

    print_message("Call SetHost with Alias should pass\n");
    rv = amxb_call(bus, "DNS.", "SetHost", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_host_address_added(alias, "192.168.1.102");
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_remove_host(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    const char* alias = "host-1";
    int rv = -1;

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", alias);

    print_message("Call RemoveHost with Alias should pass\n");
    rv = amxb_call(bus, "DNS.", "RemoveHost", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_host_address_removed(alias, "192.168.1.101");
    expect_check_host_address_removed(alias, "192.168.1.103");
    expect_check_host_address_removed(alias, "192.168.1.102");
    expect_check_host_removed(alias, "example.com", "Device.Logical.Interface.3.", "Static", true);
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

typedef struct {
    const char* alias;
    const char* dnsserver;
    const char* interface;
    const char* type;
    bool enable;
} na_t;

void test_set_mode(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;
    amxd_trans_t trans;

    assert_non_null(bus);

    amxd_trans_init(&trans);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Mode", "Any");

    print_message("Call SetMode(Mode=Any) without forwarding instances\n");
    rv = amxb_call(bus, "DNS.", "SetMode", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    amxut_bus_handle_events();

    na_t forwarding1[] = {
        { .alias = "static-1", .dnsserver = "", .interface = "", .type = "Static", .enable = true},
        { .alias = "static-2", .dnsserver = "", .interface = "", .type = "Static", .enable = true},
        { .alias = "dhcpv4-1", .dnsserver = "", .interface = "", .type = "DHCPv4", .enable = true},
        { .alias = "dhcpv4-2", .dnsserver = "", .interface = "", .type = "DHCPv4", .enable = true},
        { .alias = "dhcpv6-1", .dnsserver = "", .interface = "", .type = "DHCPv6", .enable = true},
        { .alias = "dhcpv6-2", .dnsserver = "", .interface = "", .type = "DHCPv6", .enable = true},
        { .alias = "ppp-1", .dnsserver = "", .interface = "", .type = "IPCP", .enable = true},
        { .alias = "ppp-2", .dnsserver = "", .interface = "", .type = "IPCP", .enable = true},
        { .alias = "ra-1", .dnsserver = "", .interface = "", .type = "RouterAdvertisement", .enable = true},
        { .alias = "ra-2", .dnsserver = "", .interface = "", .type = "RouterAdvertisement", .enable = true},
        {}
    };

    print_message("For each type add forwarding instances\n");
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    for(int i = 0; forwarding1[i].alias != NULL; i++) {
        amxd_trans_add_inst(&trans, 0, forwarding1[i].alias);
        amxd_trans_set_value(cstring_t, &trans, "Type", forwarding1[i].type);
        amxd_trans_set_value(bool, &trans, "Enable", true);
        amxd_trans_select_pathf(&trans, ".^");
        expect_check_forwarding_add(forwarding1[i].alias, forwarding1[i].dnsserver, forwarding1[i].interface, forwarding1[i].type, forwarding1[i].enable);
    }
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    na_t forwarding2[] = {
        { .alias = "static-1", .dnsserver = "", .interface = "", .type = "Static", .enable = false},
        { .alias = "static-2", .dnsserver = "", .interface = "", .type = "Static", .enable = false},
        { .alias = "dhcpv6-1", .dnsserver = "", .interface = "", .type = "DHCPv6", .enable = false},
        { .alias = "dhcpv6-2", .dnsserver = "", .interface = "", .type = "DHCPv6", .enable = false},
        { .alias = "ppp-1", .dnsserver = "", .interface = "", .type = "IPCP", .enable = false},
        { .alias = "ppp-2", .dnsserver = "", .interface = "", .type = "IPCP", .enable = false},
        { .alias = "ra-1", .dnsserver = "", .interface = "", .type = "RouterAdvertisement", .enable = false},
        { .alias = "ra-2", .dnsserver = "", .interface = "", .type = "RouterAdvertisement", .enable = false},
        {}
    };

    print_message("Call SetMode(Mode=DHCPv4)\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Mode", "DHCPv4");
    rv = amxb_call(bus, "DNS.", "SetMode", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    for(int i = 0; forwarding2[i].alias; i++) {
        expect_check_forwarding_changed(forwarding2[i].alias, forwarding2[i].dnsserver, forwarding2[i].interface, forwarding2[i].enable);
    }
    amxut_bus_handle_events();

    na_t forwarding3[] = {
        { .alias = "dhcpv4-1", .dnsserver = "", .interface = "", .type = "DHCPv4", .enable = false},
        { .alias = "dhcpv4-2", .dnsserver = "", .interface = "", .type = "DHCPv4", .enable = false},
        { .alias = "dhcpv6-1", .dnsserver = "", .interface = "", .type = "DHCPv6", .enable = true},
        { .alias = "dhcpv6-2", .dnsserver = "", .interface = "", .type = "DHCPv6", .enable = true},
        {}
    };

    print_message("Call SetMode(Mode=DHCPv6)\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Mode", "DHCPv6");
    rv = amxb_call(bus, "DNS.", "SetMode", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    for(int i = 0; forwarding3[i].alias; i++) {
        expect_check_forwarding_changed(forwarding3[i].alias, forwarding3[i].dnsserver, forwarding3[i].interface, forwarding3[i].enable);
    }
    amxut_bus_handle_events();

    na_t forwarding4[] = {
        { .alias = "dhcpv6-1", .dnsserver = "", .interface = "", .type = "DHCPv6", .enable = false},
        { .alias = "dhcpv6-2", .dnsserver = "", .interface = "", .type = "DHCPv6", .enable = false},
        { .alias = "ppp-1", .dnsserver = "", .interface = "", .type = "IPCP", .enable = true},
        { .alias = "ppp-2", .dnsserver = "", .interface = "", .type = "IPCP", .enable = true},
        {}
    };

    print_message("Call SetMode(Mode=IPCP)\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Mode", "IPCP");
    rv = amxb_call(bus, "DNS.", "SetMode", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    for(int i = 0; forwarding4[i].alias; i++) {
        expect_check_forwarding_changed(forwarding4[i].alias, forwarding4[i].dnsserver, forwarding4[i].interface, forwarding4[i].enable);
    }
    amxut_bus_handle_events();

    na_t forwarding5[] = {
        { .alias = "ppp-1", .dnsserver = "", .interface = "", .type = "IPCP", .enable = false},
        { .alias = "ppp-2", .dnsserver = "", .interface = "", .type = "IPCP", .enable = false},
        { .alias = "ra-1", .dnsserver = "", .interface = "", .type = "RouterAdvertisement", .enable = true},
        { .alias = "ra-2", .dnsserver = "", .interface = "", .type = "RouterAdvertisement", .enable = true},
        {}
    };

    print_message("Call SetMode(Mode=RouterAdvertisement)\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Mode", "RouterAdvertisement");
    rv = amxb_call(bus, "DNS.", "SetMode", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    for(int i = 0; forwarding5[i].alias; i++) {
        expect_check_forwarding_changed(forwarding5[i].alias, forwarding5[i].dnsserver, forwarding5[i].interface, forwarding5[i].enable);
    }
    amxut_bus_handle_events();

    na_t forwarding6[] = {
        { .alias = "static-1", .dnsserver = "", .interface = "", .type = "Static", .enable = true},
        { .alias = "static-2", .dnsserver = "", .interface = "", .type = "Static", .enable = true},
        { .alias = "ra-1", .dnsserver = "", .interface = "", .type = "RouterAdvertisement", .enable = false},
        { .alias = "ra-2", .dnsserver = "", .interface = "", .type = "RouterAdvertisement", .enable = false},
        {}
    };

    print_message("Call SetMode(Mode=Static)\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Mode", "Static");
    rv = amxb_call(bus, "DNS.", "SetMode", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    for(int i = 0; forwarding6[i].alias; i++) {
        expect_check_forwarding_changed(forwarding6[i].alias, forwarding6[i].dnsserver, forwarding6[i].interface, forwarding6[i].enable);
    }
    amxut_bus_handle_events();

    na_t forwarding7[] = {
        { .alias = "dhcpv4-1", .dnsserver = "", .interface = "", .type = "DHCPv4", .enable = true},
        { .alias = "dhcpv4-2", .dnsserver = "", .interface = "", .type = "DHCPv4", .enable = true},
        { .alias = "dhcpv6-1", .dnsserver = "", .interface = "", .type = "DHCPv6", .enable = true},
        { .alias = "dhcpv6-2", .dnsserver = "", .interface = "", .type = "DHCPv6", .enable = true},
        { .alias = "ppp-1", .dnsserver = "", .interface = "", .type = "IPCP", .enable = true},
        { .alias = "ppp-2", .dnsserver = "", .interface = "", .type = "IPCP", .enable = true},
        { .alias = "ra-1", .dnsserver = "", .interface = "", .type = "RouterAdvertisement", .enable = true},
        { .alias = "ra-2", .dnsserver = "", .interface = "", .type = "RouterAdvertisement", .enable = true},
        {}
    };

    print_message("Call SetMode(Mode=Any)\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Mode", "Any");
    rv = amxb_call(bus, "DNS.", "SetMode", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    for(int i = 0; forwarding7[i].alias; i++) {
        expect_check_forwarding_changed(forwarding7[i].alias, forwarding7[i].dnsserver, forwarding7[i].interface, forwarding7[i].enable);
    }
    amxut_bus_handle_events();

    print_message("Remove all forwarding instances\n");
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    for(int i = 0; forwarding1[i].alias != NULL; i++) {
        amxd_trans_del_inst(&trans, 0, forwarding1[i].alias);
        expect_check_forwarding_removed(forwarding1[i].alias, forwarding1[i].dnsserver, forwarding1[i].interface, forwarding1[i].type, forwarding1[i].enable);
    }
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_mode_with_dnsservers(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;
    amxd_trans_t trans;
    amxc_var_t* dnsservers = NULL;
    amxc_var_t* list = NULL;

    assert_non_null(bus);

    amxd_trans_init(&trans);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Mode", "Any");

    na_t forwarding1[] = {
        { .alias = "static-1", .dnsserver = "1.1.1.1", .interface = "Device.Logical.Interface.2.", .type = "Static", .enable = false},
        { .alias = "static-2", .dnsserver = "1.1.4.4", .interface = "Device.Logical.Interface.3.", .type = "Static", .enable = false},
        { .alias = "dhcpv4-1", .dnsserver = "8.8.8.8", .interface = "Device.Logical.Interface.2.", .type = "DHCPv4", .enable = true},
        { .alias = "dhcpv4-2", .dnsserver = "8.8.4.4", .interface = "Device.Logical.Interface.2.", .type = "DHCPv4", .enable = true},
        {}
    };

    print_message("For each type add forwarding instances\n");
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    for(int i = 0; forwarding1[i].alias != NULL; i++) {
        amxd_trans_add_inst(&trans, 0, forwarding1[i].alias);
        amxd_trans_set_value(cstring_t, &trans, "Type", forwarding1[i].type);
        amxd_trans_set_value(cstring_t, &trans, "DNSServer", forwarding1[i].dnsserver);
        amxd_trans_set_value(cstring_t, &trans, "Interface", forwarding1[i].interface);
        amxd_trans_set_value(bool, &trans, "Enable", forwarding1[i].enable);
        amxd_trans_select_pathf(&trans, ".^");
        expect_check_forwarding_add(forwarding1[i].alias, forwarding1[i].dnsserver, forwarding1[i].interface, forwarding1[i].type, forwarding1[i].enable);
    }
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    na_t forwarding2[] = {
        { .alias = "cpe-Forwarding-16", .dnsserver = "3.3.3.3", .interface = "Device.Logical.Interface.2.", .type = "Static", .enable = true},
        { .alias = "cpe-Forwarding-17", .dnsserver = "4.4.4.4", .interface = "Device.Logical.Interface.2.", .type = "Static", .enable = true},
        { .alias = "cpe-Forwarding-18", .dnsserver = "5.5.5.5", .interface = "Device.Logical.Interface.5.", .type = "Static", .enable = true},
        {}
    };

    na_t forwarding3[] = {
        // dnsserver and interface are empty so that the unit tests (new_change_forwarding) don't add those parameters to the event data to be compared
        { .alias = "static-1", .dnsserver = "", .interface = "", .type = "Static", .enable = true},
        { .alias = "static-2", .dnsserver = "", .interface = "", .type = "Static", .enable = true},
        { .alias = "dhcpv4-1", .dnsserver = "", .interface = "", .type = "DHCPv4", .enable = false},
        { .alias = "dhcpv4-2", .dnsserver = "", .interface = "", .type = "DHCPv4", .enable = false},
        {}
    };

    print_message("Call SetMode(Mode=Static, DNSServers={\"Device.Logical.Interface.2.\":[\"3.3.3.3\",\"4.4.4.4\"], \"Device.Logical.Interface.5.\":[\"5.5.5.5\"]})\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Mode", "Static");
    dnsservers = amxc_var_add_key(amxc_htable_t, &args, "DNSServers", NULL);
    list = amxc_var_add_key(amxc_llist_t, dnsservers, "Device.Logical.Interface.2.", NULL);
    amxc_var_add(cstring_t, list, "3.3.3.3");
    amxc_var_add(cstring_t, list, "4.4.4.4");
    list = amxc_var_add_key(amxc_llist_t, dnsservers, "Device.Logical.Interface.5.", NULL);
    amxc_var_add(cstring_t, list, "5.5.5.5");
    rv = amxb_call(bus, "DNS.", "SetMode", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    for(int i = 0; forwarding2[i].alias; i++) {
        expect_check_forwarding_add(forwarding2[i].alias, forwarding2[i].dnsserver, forwarding2[i].interface, forwarding2[i].type, forwarding2[i].enable);
    }
    for(int i = 0; forwarding3[i].alias; i++) {
        expect_check_forwarding_changed(forwarding3[i].alias, forwarding3[i].dnsserver, forwarding3[i].interface, forwarding3[i].enable);
    }
    amxut_bus_handle_events();

    na_t forwarding4[] = {
        { .alias = "static-1", .dnsserver = "1.1.1.1", .interface = "Device.Logical.Interface.2.", .type = "Static", .enable = true},
        { .alias = "static-2", .dnsserver = "1.1.4.4", .interface = "Device.Logical.Interface.3.", .type = "Static", .enable = true},
        { .alias = "dhcpv4-1", .dnsserver = "8.8.8.8", .interface = "Device.Logical.Interface.2.", .type = "DHCPv4", .enable = false},
        { .alias = "dhcpv4-2", .dnsserver = "8.8.4.4", .interface = "Device.Logical.Interface.2.", .type = "DHCPv4", .enable = false},
        { .alias = "cpe-Forwarding-16", .dnsserver = "3.3.3.3", .interface = "Device.Logical.Interface.2.", .type = "Static", .enable = true},
        { .alias = "cpe-Forwarding-17", .dnsserver = "4.4.4.4", .interface = "Device.Logical.Interface.2.", .type = "Static", .enable = true},
        { .alias = "cpe-Forwarding-18", .dnsserver = "5.5.5.5", .interface = "Device.Logical.Interface.5.", .type = "Static", .enable = true},
        {}
    };

    print_message("Remove all forwarding instances\n");
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    for(int i = 0; forwarding4[i].alias != NULL; i++) {
        amxd_trans_del_inst(&trans, 0, forwarding4[i].alias);
        expect_check_forwarding_removed(forwarding4[i].alias, forwarding4[i].dnsserver, forwarding4[i].interface, forwarding4[i].type, forwarding4[i].enable);
    }
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_delete_forwardings(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;
    amxd_trans_t trans;
    amxc_var_t* dnsservers = NULL;
    amxc_var_t* list = NULL;

    assert_non_null(bus);

    amxd_trans_init(&trans);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    na_t forwarding1[] = {
        { .alias = "static-1", .dnsserver = "1.1.1.1", .interface = "Device.Logical.Interface.2.", .type = "Static", .enable = true},
        { .alias = "static-2", .dnsserver = "1.1.4.4", .interface = "Device.Logical.Interface.2.", .type = "Static", .enable = false},
        { .alias = "static-3", .dnsserver = "2.2.2.2", .interface = "Device.Logical.Interface.3.", .type = "Static", .enable = true},
        { .alias = "static-4", .dnsserver = "2.2.4.4", .interface = "Device.Logical.Interface.3.", .type = "Static", .enable = true},
        { .alias = "dhcpv4-2", .dnsserver = "8.8.4.4", .interface = "Device.Logical.Interface.2.", .type = "DHCPv4", .enable = true},
        {}
    };

    print_message("Add forwarding instances\n");
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    for(int i = 0; forwarding1[i].alias != NULL; i++) {
        amxd_trans_add_inst(&trans, 0, forwarding1[i].alias);
        amxd_trans_set_value(cstring_t, &trans, "Type", forwarding1[i].type);
        amxd_trans_set_value(cstring_t, &trans, "DNSServer", forwarding1[i].dnsserver);
        amxd_trans_set_value(cstring_t, &trans, "Interface", forwarding1[i].interface);
        amxd_trans_set_value(bool, &trans, "Enable", forwarding1[i].enable);
        amxd_trans_select_pathf(&trans, ".^");
        expect_check_forwarding_add(forwarding1[i].alias, forwarding1[i].dnsserver, forwarding1[i].interface, forwarding1[i].type, forwarding1[i].enable);
    }
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    print_message("Call DeleteForwardings with type that is not supported\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Type", "TypeDoesNotExist");
    dnsservers = amxc_var_add_key(amxc_htable_t, &args, "DNSServers", NULL);
    list = amxc_var_add_key(amxc_llist_t, dnsservers, "Device.Logical.Interface.2.", NULL);
    amxc_var_add(cstring_t, list, "1.1.1.1");
    amxc_var_add(cstring_t, list, "1.1.4.4");
    list = amxc_var_add_key(amxc_llist_t, dnsservers, "Device.Logical.Interface.3.", NULL);
    amxc_var_add(cstring_t, list, "2.2.2.2");
    rv = amxb_call(bus, "DNS.", "DeleteForwardings", &args, &ret, 1);
    assert_int_not_equal(rv, AMXB_STATUS_OK);
    amxut_bus_handle_events();

    print_message("Call DeleteForwardings with interface that does not exist\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    dnsservers = amxc_var_add_key(amxc_htable_t, &args, "DNSServers", NULL);
    list = amxc_var_add_key(amxc_llist_t, dnsservers, "Device.Logical.Interface.200.", NULL);
    amxc_var_add(cstring_t, list, "1.1.1.1");
    amxc_var_add(cstring_t, list, "1.1.4.4");
    rv = amxb_call(bus, "DNS.", "DeleteForwardings", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    amxut_bus_handle_events();

    print_message("Call DeleteForwardings with empty list of dnsservers\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    dnsservers = amxc_var_add_key(amxc_htable_t, &args, "DNSServers", NULL);
    list = amxc_var_add_key(amxc_llist_t, dnsservers, "Device.Logical.Interface.2.", NULL);
    rv = amxb_call(bus, "DNS.", "DeleteForwardings", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    amxut_bus_handle_events();

    print_message("Call DeleteForwardings with dnsservers that doesn't exist\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    dnsservers = amxc_var_add_key(amxc_htable_t, &args, "DNSServers", NULL);
    list = amxc_var_add_key(amxc_llist_t, dnsservers, "Device.Logical.Interface.2.", NULL);
    amxc_var_add(cstring_t, list, "100.100.100.100");
    rv = amxb_call(bus, "DNS.", "DeleteForwardings", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    amxut_bus_handle_events();

    na_t forwarding3[] = {
        { .alias = "static-1", .dnsserver = "1.1.1.1", .interface = "Device.Logical.Interface.2.", .type = "Static", .enable = true},
        { .alias = "static-2", .dnsserver = "1.1.4.4", .interface = "Device.Logical.Interface.2.", .type = "Static", .enable = false},
        { .alias = "static-3", .dnsserver = "2.2.2.2", .interface = "Device.Logical.Interface.3.", .type = "Static", .enable = true},
        {}
    };

    print_message("Call DeleteForwardings(DNSServers={\"Device.Logical.Interface.2.\":[\"1.1.1.1\",\"1.1.4.4\"], \"Device.Logical.Interface.3.\":[\"2.2.2.2\"]})\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    dnsservers = amxc_var_add_key(amxc_htable_t, &args, "DNSServers", NULL);
    list = amxc_var_add_key(amxc_llist_t, dnsservers, "Device.Logical.Interface.2.", NULL);
    amxc_var_add(cstring_t, list, "1.1.1.1");
    amxc_var_add(cstring_t, list, "1.1.4.4");
    list = amxc_var_add_key(amxc_llist_t, dnsservers, "Device.Logical.Interface.3.", NULL);
    amxc_var_add(cstring_t, list, "2.2.2.2");
    rv = amxb_call(bus, "DNS.", "DeleteForwardings", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    for(int i = 0; forwarding3[i].alias; i++) {
        expect_check_forwarding_removed(forwarding3[i].alias, forwarding3[i].dnsserver, forwarding3[i].interface, forwarding3[i].type, forwarding3[i].enable);
    }
    amxut_bus_handle_events();

    na_t forwarding4[] = {
        { .alias = "dhcpv4-2", .dnsserver = "8.8.4.4", .interface = "Device.Logical.Interface.2.", .type = "DHCPv4", .enable = true},
        {}
    };

    print_message("Call DeleteForwardings(DNSServers={\"Device.Logical.Interface.2.\":[\"8.8.4.4\"]}, Type=\"DHCPv4\")\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Type", "DHCPv4");
    dnsservers = amxc_var_add_key(amxc_htable_t, &args, "DNSServers", NULL);
    list = amxc_var_add_key(amxc_llist_t, dnsservers, "Device.Logical.Interface.2.", NULL);
    amxc_var_add(cstring_t, list, "8.8.4.4");
    rv = amxb_call(bus, "DNS.", "DeleteForwardings", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    for(int i = 0; forwarding4[i].alias; i++) {
        expect_check_forwarding_removed(forwarding4[i].alias, forwarding4[i].dnsserver, forwarding4[i].interface, forwarding4[i].type, forwarding4[i].enable);
    }
    amxut_bus_handle_events();

    na_t forwarding5[] = {
        { .alias = "static-4", .dnsserver = "2.2.4.4", .interface = "Device.Logical.Interface.3.", .type = "Static", .enable = true},
        {}
    };

    print_message("Call DeleteForwardings(DNSServers={\"Device.Logical.Interface.3.\":[\"2.2.4.4\"]}, Type=\"Static\")\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Type", "Static");
    dnsservers = amxc_var_add_key(amxc_htable_t, &args, "DNSServers", NULL);
    list = amxc_var_add_key(amxc_llist_t, dnsservers, "Device.Logical.Interface.3.", NULL);
    amxc_var_add(cstring_t, list, "2.2.4.4");
    rv = amxb_call(bus, "DNS.", "DeleteForwardings", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    for(int i = 0; forwarding5[i].alias; i++) {
        expect_check_forwarding_removed(forwarding5[i].alias, forwarding5[i].dnsserver, forwarding5[i].interface, forwarding5[i].type, forwarding5[i].enable);
    }
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_set_non_persistent_host(UNUSED void** state) {
    amxb_bus_ctx_t* bus = amxb_be_who_has("DNS.");
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;
    amxd_object_t* object = NULL;
    const char* alias = "persistent-1";

    assert_non_null(bus);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", alias);
    amxc_var_add_key(bool, &args, "Persistent", false);

    print_message("Call SetHost with Alias should pass\n");
    rv = amxb_call(bus, "DNS.", "SetHost", &args, &ret, 1);
    assert_int_equal(rv, AMXB_STATUS_OK);
    expect_check_host_added("persistent-1", "", "", "", false);
    amxut_bus_handle_events();

    object = amxd_dm_findf(dm, "DNS.Host.%s.", alias);
    assert_non_null(object);
    assert_false(amxd_object_is_attr_set(object, amxd_oattr_persistent));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
