/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>

#include "mock.h"

int data_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    amxc_var_t* a = (amxc_var_t*) value;
    amxc_var_t* b = (amxc_var_t*) check_value_data;
    int result = -1;
    int rv = 0;

    print_message("< data from plugin>\n");
    amxc_var_dump(a, 1);
    print_message("< data from unit test>\n");
    amxc_var_dump(b, 1);

    if((amxc_var_compare(GET_ARG(a, "object"), GET_ARG(b, "object"), &result) != 0) ||
       (result != 0)) {
        goto exit;
    }

    if((amxc_var_compare(GET_ARG(a, "parameters"), GET_ARG(b, "parameters"), &result) != 0) ||
       (result != 0)) {
        goto exit;
    }

    rv = 1;

exit:
    amxc_var_delete(&b);
    return rv;
}

static amxc_var_t* new_add(const char* object, const char* alias, const char* dnsserver, const char* interface, const char* type, bool enable) {
    amxc_var_t* s1 = NULL;
    amxc_var_t* parameters = NULL;

    amxc_var_new(&s1);
    amxc_var_set_type(s1, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, s1, "object", object);
    parameters = amxc_var_add_key(amxc_htable_t, s1, "parameters", NULL);
    amxc_var_add_key(cstring_t, parameters, "Alias", alias);
    amxc_var_add_key(cstring_t, parameters, "DNSServer", dnsserver);
    amxc_var_add_key(cstring_t, parameters, "Interface", interface);
    amxc_var_add_key(cstring_t, parameters, "Type", type);
    amxc_var_add_key(bool, parameters, "Enable", enable);

    return s1;
}

static amxc_var_t* new_change(const char* object, const char* dnsserver, const char* interface, bool enable) {
    amxc_var_t* server = NULL;
    amxc_var_t* parameters = NULL;
    amxc_var_t* var = NULL;

    amxc_var_new(&server);
    amxc_var_set_type(server, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, server, "object", object);
    parameters = amxc_var_add_key(amxc_htable_t, server, "parameters", NULL);
    if(dnsserver[0] != '\0') {
        var = amxc_var_add_key(amxc_htable_t, parameters, "DNSServer", NULL);
        amxc_var_add_key(cstring_t, var, "to", dnsserver);
        amxc_var_add_key(cstring_t, var, "from", "");
    }
    if(interface[0] != '\0') {
        var = amxc_var_add_key(amxc_htable_t, parameters, "Interface", NULL);
        amxc_var_add_key(cstring_t, var, "to", interface);
        amxc_var_add_key(cstring_t, var, "from", "");
    }
    var = amxc_var_add_key(amxc_htable_t, parameters, "Enable", NULL);
    amxc_var_add_key(bool, var, "to", enable);
    amxc_var_add_key(bool, var, "from", !enable);

    return server;
}

amxc_var_t* new_add_server(const char* alias, const char* dnsserver, const char* interface, const char* type, bool enable) {
    return new_add("DNS.Client.Server.", alias, dnsserver, interface, type, enable);
}

amxc_var_t* new_remove_server(const char* alias, const char* dnsserver, const char* interface, const char* type, bool enable) {
    return new_add("DNS.Client.Server.", alias, dnsserver, interface, type, enable); // remove is same as add
}

amxc_var_t* new_change_server(const char* alias, const char* dnsserver, const char* interface, bool enable) {
    amxc_var_t* server = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "DNS.Client.Server.%s.", alias);

    server = new_change(amxc_string_get(&path, 0), dnsserver, interface, enable);

    amxc_string_clean(&path);

    return server;
}

amxc_var_t* new_add_forwarding(const char* alias, const char* dnsserver, const char* interface, const char* type, bool enable) {
    return new_add("DNS.Relay.Forwarding.", alias, dnsserver, interface, type, enable);
}

amxc_var_t* new_remove_forwarding(const char* alias, const char* dnsserver, const char* interface, const char* type, bool enable) {
    return new_add("DNS.Relay.Forwarding.", alias, dnsserver, interface, type, enable); // remove is same as add
}

amxc_var_t* new_change_forwarding(const char* alias, const char* dnsserver, const char* interface, bool enable) {
    amxc_var_t* server = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "DNS.Relay.Forwarding.%s.", alias);

    server = new_change(amxc_string_get(&path, 0), dnsserver, interface, enable);

    amxc_string_clean(&path);

    return server;
}

amxc_var_t* new_add_host(const char* alias, const char* name, const char* interface, const char* origin, bool enable) {
    amxc_var_t* h = NULL;
    amxc_var_t* parameters = NULL;

    amxc_var_new(&h);
    amxc_var_set_type(h, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, h, "object", "DNS.Host.");
    parameters = amxc_var_add_key(amxc_htable_t, h, "parameters", NULL);
    amxc_var_add_key(cstring_t, parameters, "Alias", alias);
    amxc_var_add_key(cstring_t, parameters, "Name", name);
    amxc_var_add_key(cstring_t, parameters, "Interface", interface);
    amxc_var_add_key(cstring_t, parameters, "Origin", origin);
    amxc_var_add_key(bool, parameters, "Enable", enable);

    return h;
}

amxc_var_t* new_remove_host(const char* alias, const char* name, const char* interface, const char* origin, bool enable) {
    return new_add_host(alias, name, interface, origin, enable); // same as add
}

amxc_var_t* new_change_host(const char* alias, const char* name, const char* interface, const char* origin, bool enable) {
    amxc_var_t* h = NULL;
    amxc_var_t* parameters = NULL;
    amxc_var_t* var = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "DNS.Host.%s.", alias);

    amxc_var_new(&h);
    amxc_var_set_type(h, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, h, "object", amxc_string_get(&path, 0));
    parameters = amxc_var_add_key(amxc_htable_t, h, "parameters", NULL);
    var = amxc_var_add_key(amxc_htable_t, parameters, "Name", NULL);
    amxc_var_add_key(cstring_t, var, "to", name);
    amxc_var_add_key(cstring_t, var, "from", "");
    var = amxc_var_add_key(amxc_htable_t, parameters, "Interface", NULL);
    amxc_var_add_key(cstring_t, var, "to", interface);
    amxc_var_add_key(cstring_t, var, "from", "");
    var = amxc_var_add_key(amxc_htable_t, parameters, "Origin", NULL);
    amxc_var_add_key(cstring_t, var, "to", origin);
    amxc_var_add_key(cstring_t, var, "from", "");
    var = amxc_var_add_key(amxc_htable_t, parameters, "Enable", NULL);
    amxc_var_add_key(bool, var, "to", enable);
    amxc_var_add_key(bool, var, "from", !enable);

    amxc_string_clean(&path);

    return h;
}

amxc_var_t* new_add_host_address(const char* host_alias, const char* ip_address) {
    amxc_var_t* ret = NULL;
    amxc_var_t* parameters = NULL;
    amxc_string_t path;
    const char* object = NULL;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "DNS.Host.%s.IPAddress.", host_alias);
    object = amxc_string_get(&path, 0);

    amxc_var_new(&ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, ret, "object", object);
    parameters = amxc_var_add_key(amxc_htable_t, ret, "parameters", NULL);
    amxc_var_add_key(cstring_t, parameters, "IPAddress", ip_address);

    amxc_string_clean(&path);

    return ret;
}

amxc_var_t* new_remove_host_address(const char* host_alias, const char* ip_address) {
    return new_add_host_address(host_alias, ip_address); // same as add
}