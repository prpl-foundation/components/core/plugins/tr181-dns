MACHINE = $(shell $(CC) -dumpmachine)

OBJDIR = $(realpath ../../output/$(MACHINE)/mod_dns_methods_coverage)
INCDIR = $(realpath ../include ../common)
MOCK_SRCDIR = $(realpath ../common)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(MOCK_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
		  --std=gnu99 -fPIC -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		   -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread -DUNIT_TEST \
		  -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500 -DMOD_DIR='"$(realpath $(OBJDIR))"'

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxb -lamxc -lamxd -lamxm -lamxo -lamxp -lamxut -ldl -lsahtrace -lpthread