# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v3.3.7 - 2024-11-27(11:15:50 +0000)

### Other

- - [tr181-dns] Reduce regex usage in signal handling

## Release v3.3.6 - 2024-11-21(15:21:06 +0000)

### Other

- - Intermittent internet connectivity loss after advanced settings change

## Release v3.3.5 - 2024-10-10(10:49:15 +0000)

### Other

- - tr181-dns : risk of dereference a pointer after free

## Release v3.3.4 - 2024-10-10(10:09:31 +0000)

### Other

- Loss of DNS Server Configuration and Internet Access After Static IP DNS using DM

## Release v3.3.3 - 2024-09-26(09:19:01 +0000)

### Other

- tr181 v2.17 compliance DNS non-breaking changes

## Release v3.3.2 - 2024-09-10(08:30:48 +0000)

### Other

- [Security][AppArmor] Apparmor must be available on PRPL builds

## Release v3.3.1 - 2024-09-05(14:30:26 +0000)

### Other

- Add dns flags to NetModel

## Release v3.3.0 - 2024-07-25(08:58:19 +0000)

### New

- Add support for DNS diagnostics (implementation part)

## Release v3.2.8 - 2024-07-23(07:48:46 +0000)

### Fixes

- Better shutdown script

## Release v3.2.7 - 2024-07-08(06:56:59 +0000)

## Release v3.2.6 - 2024-06-25(13:19:29 +0000)

### Other

- host for loopback interface

## Release v3.2.5 - 2024-06-20(08:36:19 +0000)

### Other

- -[tr181-dns] Add support for DNS diagnostics (data-model part)

## Release v3.2.4 - 2024-06-18(14:18:46 +0000)

### Other

- - amx plugin should not run as root user

## Release v3.2.3 - 2024-06-18(09:36:26 +0000)

### Other

- DNS.Client.Server.lo in status Error

## Release v3.2.2 - 2024-06-17(15:44:18 +0000)

### Other

- DomainName should be supported and should also be used for DNS suffix

## Release v3.2.1 - 2024-06-17(15:35:23 +0000)

### Fixes

- amx plugin should not run as root user

## Release v3.2.0 - 2024-06-14(09:52:11 +0000)

### New

- - [Cellular] [DNS] It must be possible to retrieve the DNS servers from the Cellular DM

## Release v3.1.3 - 2024-06-13(09:12:08 +0000)

### Other

- Static WAN IPv4 DNS Server Not Applied Correctly

## Release v3.1.2 - 2024-06-06(11:13:50 +0000)

### Other

- Unbound is restarted for every listen interface address change
- dns config subobject is missing

## Release v3.1.1 - 2024-05-31(09:27:19 +0000)

### Other

- tr181-dns crash after migration

## Release v3.1.0 - 2024-05-16(07:34:58 +0000)

### New

- add support for flexible bridge interfaces

## Release v3.0.2 - 2024-04-11(09:11:39 +0000)

### Fixes

- Device.DNS.X_PRPL-COM_RebindProtection.IPExceptions.{i}.Address not accepting IP from 127.0.0.x/x

## Release v3.0.1 - 2024-04-10(07:07:28 +0000)

### Changes

- Make amxb timeouts configurable

## Release v3.0.0 - 2024-03-27(16:37:14 +0000)

### Breaking

- some instances should not be reboot/upgrade persistent

## Release v2.11.0 - 2024-03-23(13:25:01 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v2.10.3 - 2024-03-22(14:37:17 +0000)

### Fixes

- no DNS after upgrade

## Release v2.10.2 - 2024-03-21(09:05:10 +0000)

### Fixes

- After reboot all hosts are disconnected (AKA amb timeouts)

## Release v2.10.1 - 2024-03-16(17:12:34 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v2.10.0 - 2024-02-20(11:44:13 +0000)

### New

- DUT doesn't resolve its hostname

## Release v2.9.0 - 2024-02-15(10:57:28 +0000)

### New

- [amxrt][no-root-user][capability drop] All amx plugins must be adapted to run as non-root and lmited capabilities

## Release v2.8.3 - 2024-02-08(09:04:19 +0000)

### Fixes

- DNSMode and IPv6DNSMode should be upgrade persistent

## Release v2.8.2 - 2024-02-08(08:58:37 +0000)

### Changes

- The DUT uses the primary IPv4 DNS server address instead of IPv6

## Release v2.8.1 - 2024-02-06(14:46:33 +0000)

### Changes

- doc string breaks markdown of confluence

## Release v2.8.0 - 2024-02-01(15:46:36 +0000)

### New

- Unable to set static DNS Mode for WAN IPv4 and IPv6 separately

## Release v2.7.6 - 2024-01-26(09:14:40 +0000)

### Changes

- add dependency on libamxut for unit tests

## Release v2.7.5 - 2024-01-25(14:29:01 +0000)

### Fixes

- getdebuginfo is missing protected parameters

## Release v2.7.4 - 2024-01-24(16:11:43 +0000)

### Fixes

- refactor spaghetti code

## Release v2.7.3 - 2024-01-23(10:40:22 +0000)

### Fixes

- no case to set Status to Disabled

## Release v2.7.2 - 2023-12-12(11:34:41 +0000)

### Fixes

- [Repeater Config][IPv6] Implement a proper IPv6 config of the repeater

## Release v2.7.1 - 2023-11-30(10:58:46 +0000)

### Changes

- provide default configuration for failover

## Release v2.7.0 - 2023-11-27(12:36:32 +0000)

### New

- generic DNS failover mechanism

## Release v2.6.1 - 2023-11-24(14:40:40 +0000)

### Fixes

- - DNS name is not upgrade persistent

## Release v2.6.0 - 2023-11-23(13:26:10 +0000)

### New

- [DNS] Allow adding non-persistent Host instances

## Release v2.5.0 - 2023-11-23(13:21:53 +0000)

### New

- prplOS DNS rebinding exception validator

## Release v2.4.1 - 2023-11-20(12:17:51 +0000)

### Other

- [AP Config] [DNS] Configure DNS.Client.Server based on DHCPv4,6, ra options.

## Release v2.4.0 - 2023-11-20(10:11:25 +0000)

### New

- prplOS DNS rebinding

## Release v2.3.5 - 2023-10-13(14:12:51 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v2.3.4 - 2023-10-11(12:36:39 +0000)

### Other

- [AP Config][DNS] if DNS Relay is used, it must not be visible/exposed on its bridge interface.

## Release v2.3.3 - 2023-09-07(10:17:14 +0000)

### Other

- [AP WNC] dns is not working on wnc repeater

## Release v2.3.2 - 2023-08-24(08:30:40 +0000)

### Other

- - "on event regexp(...)" without regex

## Release v2.3.1 - 2023-08-11(11:56:30 +0000)

### Fixes

- sometimes the status parameter is Error when dns is functional

## Release v2.3.0 - 2023-08-10(18:53:06 +0000)

### New

- [amx][DNS] Add support for Forward Zones in the DNS plugin (and tr181 dm)

## Release v2.2.0 - 2023-07-13(12:32:06 +0000)

### New

- dns failover after 800ms

## Release v2.1.0 - 2023-06-28(08:25:21 +0000)

### New

- use amx to communicate with Unbound

## Release v2.0.0 - 2023-06-08(17:15:02 +0000)

### Breaking

- Implement DNS caching

## Release v1.1.11 - 2023-05-11(09:01:17 +0000)

### Other

- [Coverage] Remove SAHTRACE defines in order to increase branching coverage

## Release v1.1.10 - 2023-04-28(06:31:59 +0000)

### Fixes

- Crash during boot

## Release v1.1.9 - 2023-04-21(06:33:19 +0000)

### Fixes

- service protocol should be integers

## Release v1.1.8 - 2023-03-21(14:24:42 +0000)

### Fixes

- Add missing DNS.Relay.LANInterface for AP

## Release v1.1.7 - 2023-03-20(12:36:46 +0000)

### Fixes

- random: firewall rule allowing DNS traffic from LAN is missing

## Release v1.1.6 - 2023-03-17(18:42:49 +0000)

### Other

- [baf] Correct typo in config option

## Release v1.1.5 - 2023-03-16(14:28:54 +0000)

### Fixes

- DNS servers are disabled in wanmode ppp

### Other

- Add AP config files

## Release v1.1.4 - 2023-03-09(12:02:47 +0000)

### Other

- tr181-components: missing explicit dependency on rpcd service providing ubus uci backend
- [Config] enable configurable coredump generation

## Release v1.1.3 - 2023-02-27(12:28:40 +0000)

### Fixes

- [CDROUTER][DNS] The DNS proxy seems not working on the Box LAN GUA IPv6@

## Release v1.1.2 - 2023-02-01(09:58:39 +0000)

### Fixes

- tr181-dns process doesn't stop with SIGTERM (stuck in ps state Rs)

## Release v1.1.1 - 2023-02-01(09:35:44 +0000)

### Changes

- add more debug info

## Release v1.1.0 - 2023-01-14(08:39:09 +0000)

### New

- move part of DNSMode functionality to tr181-dns

## Release v1.0.1 - 2023-01-12(11:17:39 +0000)

### Fixes

- no DNS: router advertisement option RDNSS parsing is wrong

## Release v1.0.0 - 2023-01-06(14:05:21 +0000)

### Breaking

- Hosts, HostName and DomainName configuration

## Release v0.6.5 - 2022-12-14(08:14:42 +0000)

### Fixes

- Use correct option tag when fetching IPv6 DNS server addresses

## Release v0.6.4 - 2022-12-09(09:21:04 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.6.3 - 2022-11-10(11:26:34 +0000)

### Fixes

- PPP DNS servers are not configured

## Release v0.6.2 - 2022-10-06(08:04:00 +0000)

### Other

- set import-dbg to false

## Release v0.6.1 - 2022-09-12(06:11:08 +0000)

### Changes

- enable doc-check

## Release v0.6.0 - 2022-08-29(16:06:00 +0000)

### New

- Dynamic handling of network events (PPP)

## Release v0.5.7 - 2022-08-23(06:56:45 +0000)

### Fixes

- support multiple servers of DHCP/ppp/router-advertisement

## Release v0.5.6 - 2022-08-04(08:03:34 +0000)

### Other

- [CDRouter][TOP 100 IPv6][DNS] Open firewall DNS ports on IPv6 address for Lan interface [FIX]

## Release v0.5.5 - 2022-07-07(06:58:54 +0000)

### Changes

- split top level ODL

## Release v0.5.4 - 2022-07-06(14:25:25 +0000)

### Fixes

- default ODL configures "mod-dns-unbound" but "mod-dns-uci" (dnsmasq) is used

## Release v0.5.3 - 2022-07-04(11:16:11 +0000)

### Other

- Opensource component

## Release v0.5.2 - 2022-06-23(17:18:03 +0000)

### Other

- [amxrt] All amx plugins should start with the -D option

## Release v0.5.1 - 2022-05-12(06:24:23 +0000)

### Changes

- integrate DNS advanced

## Release v0.5.0 - 2022-04-28(11:46:59 +0000)

### New

- must support failover scenarios

## Release v0.4.1 - 2022-04-07(11:18:14 +0000)

### Other

- Fix dependency on mod-fw-amx

## Release v0.4.0 - 2022-04-07(05:32:41 +0000)

### New

- It must be possible to support 'natively' a caching dnsserver

## Release v0.3.4 - 2022-04-06(06:32:45 +0000)

### Fixes

- Static DNS Server(s) not taken into account

## Release v0.3.3 - 2022-03-29(10:33:52 +0000)

### Changes

- add lcm interface to config

## Release v0.3.2 - 2022-03-24(10:46:05 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.3.1 - 2022-03-22(15:21:16 +0000)

### Fixes

- Crash when adding default static DNS servers to ODL

## Release v0.3.0 - 2022-03-21(14:58:12 +0000)

### New

- add API for Server and Forwarding

## Release v0.2.0 - 2022-03-14(15:54:43 +0000)

### New

- Implementation of a basic tr181 compatible DNS plugin

## Release v0.1.3 - 2022-02-25(11:51:09 +0000)

### Other

- Enable core dumps by default

## Release v0.1.2 - 2022-02-22(10:49:07 +0000)

### Fixes

- Implementation of a basic tr181 compatible DNS plugin

## Release v0.1.1 - 2022-02-14(10:00:05 +0000)

### Fixes

- Implementation of a basic tr181 compatible DNS plugin

## Release v0.1.0 - 2022-02-11(08:41:38 +0000)

### New

- Implementation of a basic tr181 compatible DNS plugin

