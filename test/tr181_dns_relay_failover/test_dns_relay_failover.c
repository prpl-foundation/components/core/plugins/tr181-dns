/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "tr181-dns.h"
#include "test_dns_relay_failover.h"
#include "mock.h"

int test_setup(void** state) {
    dns_unit_test_setup(state); // will include test.odl
    return 0;
}

int test_teardown(void** state) {
    dns_unit_test_teardown(state);
    return 0;
}

void test_mod_failover_is_used(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t* ut_var = NULL;
    amxc_var_t* var = NULL;

    // let mod-failover-dummy whitelist only forwarding-2
    mod_failover_dummy_whitelist = "forwarding-2";

    // add DNS.Relay.Forwarding.1
    // plugin should call mod-failover's add_server(forwarding-1) but mod-failover won't call mod-core's set_server because forwarding-1 is not whitelisted
    // add DNS.Relay.Forwarding.2
    // plugin should call mod-failover's add_server(forwarding-2) and mod-failover will call mod-core's set_server(forwarding-2)
    amxc_var_new(&ut_var);
    amxc_var_set_type(ut_var, AMXC_VAR_ID_HTABLE);
    var = amxc_var_add_key(amxc_htable_t, ut_var, "servers", NULL);
    assert_non_null(var);
    var = amxc_var_add_key(amxc_htable_t, var, "forwarding-2", NULL);
    assert_non_null(var);
    amxc_var_add_key(bool, var, "enable", true);
    amxc_var_add_key(cstring_t, var, "interface", "eth0");
    amxc_var_add_key(cstring_t, var, "name", "forwarding-2");
    amxc_var_add_key(cstring_t, var, "server", "8.8.4.4");
    amxc_var_add_key(cstring_t, var, "tag", "");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "forwarding-1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "forwarding-2");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_set_server(&ut_var);
    amxut_bus_handle_events();

    // disable Forwarding.1
    // plugin should call mod-failover's remove_server(forwarding-1) BUT mod-failover won't call mod-core's set-server because forwarding-1 is not whitelisted
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.forwarding-1.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    // disable Forwarding.2
    // plugin should call mod-failover's remove_server(forwarding-2) and mod-failover will call mod-core's set_server({empty list})
    amxc_var_new(&ut_var);
    amxc_var_set_type(ut_var, AMXC_VAR_ID_HTABLE);
    var = amxc_var_add_key(amxc_htable_t, ut_var, "servers", NULL);
    assert_non_null(var);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.forwarding-2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_set_server(&ut_var);
    amxut_bus_handle_events();
}

void test_mod_failover_with_forwarding_blocked_by_dnsmode(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t* ut_var = NULL;
    amxc_var_t* servers = NULL;
    amxc_var_t* var = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    int amxm_rv = 0;

    mod_failover_dummy_whitelist = "forwarding-1,forwarding-2";

    // add DNS.Relay.Config.config-1. with DNSMode = Dynamic
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.");
    amxd_trans_add_inst(&trans, 0, "config-1");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Dynamic");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    // add DNS.Relay.Forwarding.forwarding-1. with Type = DHCPv4 (dynamic)
    amxc_var_new(&ut_var);
    amxc_var_set_type(ut_var, AMXC_VAR_ID_HTABLE);
    servers = amxc_var_add_key(amxc_htable_t, ut_var, "servers", NULL);
    assert_non_null(servers);
    var = amxc_var_add_key(amxc_htable_t, servers, "forwarding-1", NULL);
    assert_non_null(var);
    amxc_var_add_key(bool, var, "enable", true);
    amxc_var_add_key(cstring_t, var, "interface", "eth0");
    amxc_var_add_key(cstring_t, var, "name", "forwarding-1");
    amxc_var_add_key(cstring_t, var, "server", "8.8.8.8");
    amxc_var_add_key(cstring_t, var, "tag", "");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "forwarding-1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_set_server(&ut_var);
    amxut_bus_handle_events();

    // add DNS.Relay.Forwarding.forwarding-2. with Type = Static
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "forwarding-2");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "8.8.4.4");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    // forwarding-2 is blocked by DNS.Relay.Config.config-1.DNSMode so plugin won't call mod-failover's add-server
    amxut_bus_handle_events();

    // Normally if plugin calls mod-failover's server-failure, mod-failover will call mod-core's set-server(forwarding-2)
    // but this would add a lot of implementation specific test code so this test calls it directly.
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    var = amxc_var_add_key(amxc_htable_t, &args, "servers", NULL);
    var = amxc_var_add_key(amxc_htable_t, var, "forwarding-2", NULL);
    amxm_rv = amxm_execute_function(NULL, "core", "set-server", &args, &ret);
    assert_int_equal(amxm_rv, -1); // set-server should return -1 because forwarding-2 is blocked by DNS.Config.config-1.DNSMode
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // unblock forwarding-2, plugin should call mod-failover's add-server(forwarding-2) and mod-failover should call mod-core's set-server(forwarding-1,forwarding-2)
    amxc_var_new(&ut_var);
    amxc_var_set_type(ut_var, AMXC_VAR_ID_HTABLE);
    servers = amxc_var_add_key(amxc_htable_t, ut_var, "servers", NULL);
    assert_non_null(servers);
    var = amxc_var_add_key(amxc_htable_t, servers, "forwarding-1", NULL);
    assert_non_null(var);
    amxc_var_add_key(bool, var, "enable", true);
    amxc_var_add_key(cstring_t, var, "interface", "eth0");
    amxc_var_add_key(cstring_t, var, "name", "forwarding-1");
    amxc_var_add_key(cstring_t, var, "server", "8.8.8.8");
    amxc_var_add_key(cstring_t, var, "tag", "");
    var = amxc_var_add_key(amxc_htable_t, servers, "forwarding-2", NULL);
    assert_non_null(var);
    amxc_var_add_key(bool, var, "enable", true);
    amxc_var_add_key(cstring_t, var, "interface", "eth0");
    amxc_var_add_key(cstring_t, var, "name", "forwarding-2");
    amxc_var_add_key(cstring_t, var, "server", "8.8.4.4");
    amxc_var_add_key(cstring_t, var, "tag", "");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Any");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_set_server(&ut_var);
    amxut_bus_handle_events();
}

void test_mod_failover_with_forwarding_blocked_by_ipv6dnsmode(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t* ut_var = NULL;
    amxc_var_t* servers = NULL;
    amxc_var_t* var = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    int amxm_rv = 0;

    mod_failover_dummy_whitelist = "forwarding-1,forwarding-2";

    // add DNS.Relay.Config.config-1. with DNSMode = Dynamic
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.");
    amxd_trans_add_inst(&trans, 0, "config-1");
    amxd_trans_set_value(cstring_t, &trans, "IPv6DNSMode", "Dynamic");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    // add DNS.Relay.Forwarding.forwarding-1. with Type = RouterAdvertisement (dynamic)
    amxc_var_new(&ut_var);
    amxc_var_set_type(ut_var, AMXC_VAR_ID_HTABLE);
    servers = amxc_var_add_key(amxc_htable_t, ut_var, "servers", NULL);
    assert_non_null(servers);
    var = amxc_var_add_key(amxc_htable_t, servers, "forwarding-1", NULL);
    assert_non_null(var);
    amxc_var_add_key(bool, var, "enable", true);
    amxc_var_add_key(cstring_t, var, "interface", "eth0");
    amxc_var_add_key(cstring_t, var, "name", "forwarding-1");
    amxc_var_add_key(cstring_t, var, "server", "2a02:1802:94:4200::10");
    amxc_var_add_key(cstring_t, var, "tag", "");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "forwarding-1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Type", "RouterAdvertisement");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_set_server(&ut_var);
    amxut_bus_handle_events();

    // add DNS.Relay.Forwarding.forwarding-2. with Type = Static
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "forwarding-2");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "2a02:1802:94:4200::2");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    // forwarding-2 is blocked by DNS.Relay.Config.config-1.DNSMode so plugin won't call mod-failover's add-server
    amxut_bus_handle_events();

    // Normally if plugin calls mod-failover's server-failure, mod-failover will call mod-core's set-server(forwarding-2)
    // but this would add a lot of implementation specific test code so this test calls it directly.
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    var = amxc_var_add_key(amxc_htable_t, &args, "servers", NULL);
    var = amxc_var_add_key(amxc_htable_t, var, "forwarding-2", NULL);
    amxm_rv = amxm_execute_function(NULL, "core", "set-server", &args, &ret);
    assert_int_equal(amxm_rv, -1); // set-server should return -1 because forwarding-2 is blocked by DNS.Config.config-1.DNSMode
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    // unblock forwarding-2, plugin should call mod-failover's add-server(forwarding-2) and mod-failover should call mod-core's set-server(forwarding-1,forwarding-2)
    amxc_var_new(&ut_var);
    amxc_var_set_type(ut_var, AMXC_VAR_ID_HTABLE);
    servers = amxc_var_add_key(amxc_htable_t, ut_var, "servers", NULL);
    assert_non_null(servers);
    var = amxc_var_add_key(amxc_htable_t, servers, "forwarding-1", NULL);
    assert_non_null(var);
    amxc_var_add_key(bool, var, "enable", true);
    amxc_var_add_key(cstring_t, var, "interface", "eth0");
    amxc_var_add_key(cstring_t, var, "name", "forwarding-1");
    amxc_var_add_key(cstring_t, var, "server", "2a02:1802:94:4200::10");
    amxc_var_add_key(cstring_t, var, "tag", "");
    var = amxc_var_add_key(amxc_htable_t, servers, "forwarding-2", NULL);
    assert_non_null(var);
    amxc_var_add_key(bool, var, "enable", true);
    amxc_var_add_key(cstring_t, var, "interface", "eth0");
    amxc_var_add_key(cstring_t, var, "name", "forwarding-2");
    amxc_var_add_key(cstring_t, var, "server", "2a02:1802:94:4200::2");
    amxc_var_add_key(cstring_t, var, "tag", "");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6DNSMode", "Any");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_set_server(&ut_var);
    amxut_bus_handle_events();
}

void test_mod_failover_disable_failover(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* ut_var = NULL;
    amxc_var_t* servers = NULL;
    amxc_var_t* var = NULL;
    int amxm_rv = 0;

    // let mod-failover whitelist forwarding-2
    mod_failover_dummy_whitelist = "forwarding-2";

    // add DNS.Relay.Config.config-1. with DNSMode=Static
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.");
    amxd_trans_add_inst(&trans, 0, "config-1");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Static");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    // add DNS.Relay.Forwarding.forwarding-1. with Type=Static
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "forwarding-1");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "8.8.8.8");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    // mod-failover will not call mod-core's set-server because failover-1 is not whitelisted
    amxut_bus_handle_events();

    // add DNS.Relay.Forwarding.forwarding-2. with Type=DHCPv4
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "forwarding-2");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    // mod-core will not call mod-failover's add-server because failover-2 is blocked by DNS.Relay.Config.config-1.
    amxut_bus_handle_events();

    // disable mod-failover by calling mod-core's failover-enable-changed(false), plugin will call mod-dns's set-server(forwarding-1) not including forwarding-2 because it is blocked by DNS.Relay.Config.config-1.DNSMode
    amxc_var_new(&ut_var);
    amxc_var_set_type(ut_var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ut_var, "timeout", 0);
    amxc_var_add_key(uint32_t, ut_var, "repeat", 0);
    servers = amxc_var_add_key(amxc_htable_t, ut_var, "servers", NULL);
    assert_non_null(servers);
    var = amxc_var_add_key(amxc_htable_t, servers, "forwarding-1", NULL);
    assert_non_null(var);
    amxc_var_add_key(bool, var, "enable", true);
    amxc_var_add_key(cstring_t, var, "interface", "eth0");
    amxc_var_add_key(cstring_t, var, "name", "forwarding-1");
    amxc_var_add_key(cstring_t, var, "server", "8.8.8.8");
    amxc_var_add_key(cstring_t, var, "tag", "");

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set(bool, &args, false);
    expect_check_set_server(&ut_var);
    amxm_rv = amxm_execute_function(NULL, "core", "failover-enable-changed", &args, &ret);
    assert_int_equal(amxm_rv, 0);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxut_bus_handle_events();
}