/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "tr181-dns.h"
#include "test_dns_host.h"
#include "mock.h"

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;

int test_setup(void** state) {
    dns_unit_test_setup(state);
    dm = amxut_bus_dm();
    parser = amxut_bus_parser();
    return 0;
}

int test_teardown(void** state) {
    dns_unit_test_teardown(state);
    dm = NULL;
    parser = NULL;
    return 0;
}

void test_firewall_open(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: change 'DNS.Relay.Config' to 2 instances\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.");
    amxd_trans_add_inst(&trans, 0, "lan-1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.Logical.Interface.2.");
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "lan-2");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.Logical.Interface.3.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: two firewall service instances are added: dns-lan and dns-guest (1 for each interface in 'DNS.Relay.Config')\n");
    expect_check_set_interface("br-lan");
    expect_check_set_interface("br-guest");
    expect_check_set_service("dns-1", "Device.Logical.Interface.2.", "17,6", true, 0, 53);
    expect_check_set_service("dns-2", "Device.Logical.Interface.3.", "17,6", true, 0, 53);
    amxut_bus_handle_events();
}

void test_host_add(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add 1 instance of 'DNS.Host.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.");
    amxd_trans_add_inst(&trans, 0, "host-1");
    amxd_trans_set_value(cstring_t, &trans, "Name", "example.com");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.Logical.Interface.2.");
    print_message("expectation: 'add_host' of the dns controller is not called because there are no instances of IPAddress\n");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_host_address_add(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add 1 instance of 'DNS.Host.host-1.IPAddress'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.host-1.IPAddress");
    amxd_trans_add_inst(&trans, 0, "address-1");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.1.100");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'add_host' of the dns controller is called because there are instances of IPAddress\n");
    expect_check_add_host("example.com", "br-lan", "192.168.1.100", "");
    amxut_bus_handle_events();

    amxd_object_t* obj = NULL;
    obj = amxd_dm_findf(dm, "DNS.Host.host-1.IPAddress.1.");
    assert_non_null(obj);
    amxd_param_t* param = NULL;
    param = amxd_object_get_param_def(obj, "IPAddress");
    assert_true(amxd_param_has_flag(param, "usersetting"));
    assert_true(amxd_param_is_attr_set(param, amxd_pattr_persistent));
}

void test_host_change_enable(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: change parameter 'DNS.Host.host-1.Enable' to false\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.host-1.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_check_remove_host("example.com", "br-lan");
    amxd_trans_clean(&trans);
    print_message("expectation: 'remove_host' of the dns controller is called\n");
    amxut_bus_handle_events();

    print_message("step: change parameter 'DNS.Host.host-1.Enable' to enable\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.host-1.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'add_host' of the dns controller is called\n");
    expect_check_add_host("example.com", "br-lan", "192.168.1.100", "");
    amxut_bus_handle_events();
}

void test_host_change_name(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: change parameter 'DNS.Host.host-1.Name'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.host-1.");
    amxd_trans_set_value(cstring_t, &trans, "Name", "another.com");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_check_remove_host("example.com", "br-lan");
    amxd_trans_clean(&trans);
    print_message("expectation: 'remove_host' and then 'add_host' of the dns controller are called\n");
    expect_check_add_host("another.com", "br-lan", "192.168.1.100", "");
    amxut_bus_handle_events();
}

void test_host_change_interface(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: change parameter 'DNS.Host.host-1.Interface'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.host-1.");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.Logical.Interface.3.");
    expect_check_remove_host("another.com", "br-lan");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'remove_host' and then 'add_host' of the dns controller are called\n");
    expect_check_add_host("another.com", "br-guest", "192.168.1.100", "");
    amxut_bus_handle_events();
}

void test_host_address_add_second(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add another instance of 'DNS.Host.host-1.IPAddress'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.host-1.IPAddress");
    amxd_trans_add_inst(&trans, 0, "address-2");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.1.200");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'add_host' of the dns controller is called\n");
    expect_check_add_host("another.com", "br-guest", "192.168.1.200", "");
    amxut_bus_handle_events();
}

void test_host_address_change_second(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: change parameter 'DNS.Host.host-1.IPAddress.address-2.IPAddress'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.host-1.IPAddress.address-2.");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.1.102");
    expect_check_remove_host_address("another.com", "br-guest", "192.168.1.200", "");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'remove_host_address' and then 'add_host' of the dns controller is called\n");
    expect_check_add_host("another.com", "br-guest", "192.168.1.102", "");
    amxut_bus_handle_events();
}

void test_host_address_remove_second(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: remove 'DNS.Host.host-1.IPAddress.address-2.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.host-1.IPAddress.");
    amxd_trans_del_inst(&trans, 0, "address-2");
    expect_check_remove_host_address("another.com", "br-guest", "192.168.1.102", "");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'remove_host_address' of the dns controller is called\n");
    amxut_bus_handle_events();
}

void test_host_remove(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: remove 'DNS.Host.host-1.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.");
    amxd_trans_del_inst(&trans, 0, "host-1");
    expect_check_remove_host_address("another.com", "br-guest", "192.168.1.100", "");
    expect_check_remove_host("another.com", "br-guest");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'remove_host' of the dns controller is called\n");
    amxut_bus_handle_events();
}

void test_2_hosts_with_same_interface(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add 2 instances of 'DNS.Host.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.");
    amxd_trans_add_inst(&trans, 0, "host-1");
    amxd_trans_set_value(cstring_t, &trans, "Name", "host1.com");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.Logical.Interface.2.");
    amxd_trans_select_pathf(&trans, ".IPAddress.");
    amxd_trans_add_inst(&trans, 0, "address-1");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "::21");
    amxd_trans_select_pathf(&trans, ".^.^.^");
    amxd_trans_add_inst(&trans, 0, "host-2");
    amxd_trans_set_value(cstring_t, &trans, "Name", "host2.com");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.Logical.Interface.2.");
    amxd_trans_select_pathf(&trans, ".IPAddress.");
    amxd_trans_add_inst(&trans, 0, "address-1");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "::22");
    print_message("expectation: 'add_host' of the dns controller is called for each host\n");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_host("host1.com", "br-lan", "", "::21");
    expect_check_add_host("host2.com", "br-lan", "", "::22");
    amxut_bus_handle_events();

    print_message("step: change parameter 'DNS.Host.host-2.Interface'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.host-2.");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.Logical.Interface.3.");
    print_message("expectation: 'remove_host_address' and then 'add_host' of the dns controller is called\n");
    expect_check_remove_host("host2.com", "br-lan");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_host("host2.com", "br-guest", "", "::22");
    amxut_bus_handle_events();

    print_message("step: remove 2 instances of 'DNS.Host.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.");
    amxd_trans_del_inst(&trans, 0, "host-1");
    amxd_trans_del_inst(&trans, 0, "host-2");
    print_message("expectation: 'remove_host' of the dns controller is called for each host\n");
    expect_check_remove_host_address("host2.com", "br-guest", "", "::22");
    expect_check_remove_host("host2.com", "br-guest");
    expect_check_remove_host_address("host1.com", "br-lan", "", "::21");
    expect_check_remove_host("host1.com", "br-lan");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_firewall_close(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: remove all instances of 'DNS.Relay.Config.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.");
    amxd_trans_del_inst(&trans, 0, "lan-1");
    amxd_trans_del_inst(&trans, 0, "lan-2");
    print_message("expectation: two firewall service instances are removed: dns-lan and dns-guest (1 for each interface in 'DNS.Relay.Config')\n");
    expect_check_remove_interface("br-guest");
    expect_check_remove_interface("br-lan");
    expect_check_delete_service("dns-2");
    expect_check_delete_service("dns-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_host_with_queried_addresses(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;
    amxd_param_t* param = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_pathf(&trans, "DNS.Host.");
    amxd_trans_add_inst(&trans, 0, "host-1");
    amxd_trans_set_value(cstring_t, &trans, "Name", "prplOS");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.Logical.Interface.2.");
    amxd_trans_set_value(bool, &trans, "QueryAddresses", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_host("prplOS", "br-lan", "192.168.1.1", "fe80::2ca0:2bff:fec2:3b91,2a02:1802:94:4330::1");
    amxut_bus_handle_events();

    obj = amxd_dm_findf(dm, "DNS.Host.host-1.IPAddress.1.");
    assert_non_null(obj);
    param = amxd_object_get_param_def(obj, "IPAddress");
    assert_false(amxd_param_has_flag(param, "usersetting"));
    assert_false(amxd_param_is_attr_set(param, amxd_pattr_persistent));
}

void test_host_origin_dhcpv4(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.");
    amxd_trans_add_inst(&trans, 0, "dhcpv4-1");
    amxd_trans_set_value(cstring_t, &trans, "Name", "example.com");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.Logical.Interface.2.");
    amxd_trans_set_value(cstring_t, &trans, "Origin", "DHCPv4");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Host.dhcpv4-1.IPAddress");
    amxd_trans_add_inst(&trans, 0, "address-1");
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", "192.168.1.100");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_host("example.com", "br-lan", "192.168.1.100", "");
    amxut_bus_handle_events();

    amxd_object_t* obj = NULL;
    obj = amxd_dm_findf(dm, "DNS.Host.dhcpv4-1.IPAddress.1.");
    assert_non_null(obj);
    amxd_param_t* param = NULL;
    param = amxd_object_get_param_def(obj, "IPAddress");
    assert_false(amxd_param_has_flag(param, "usersetting"));
    assert_false(amxd_param_is_attr_set(param, amxd_pattr_persistent));
}