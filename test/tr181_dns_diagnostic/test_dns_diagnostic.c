/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_dm.h>

#include "tr181-dns.h"
#include "test_dns_diagnostic.h"
#include "nslookup.h"
#include "mock.h"

#include <debug/sahtrace.h>


static void clean(void) {
    nslookup_query_t* query = get_query();
    free(query->domain);
    free(query->server);
    amxp_timer_delete(&query->timer_next_query);
    amxp_timer_delete(&query->timer_timeout_server);
    amxp_timer_delete(&query->timer_timeout_network);
    amxp_timer_delete(&query->timer_repetition);
    amxp_timer_delete(&query->timer_clean);
    if(query->ares_fds != NULL) {
        amxc_var_for_each(var, query->ares_fds) {
            amxp_connection_remove(GET_INT32(var, NULL));
        }
        amxc_var_delete(&query->ares_fds);
    }
    ares_destroy(query->channel);
    free(query);
}

int test_setup(void** state) {
    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_INFO);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    dns_unit_test_setup(state);
    return 0;
}

int test_teardown(void** state) {
    dns_unit_test_teardown(state);
    return 0;
}

void test_diagnostic_error_domain(UNUSED void** state) {
    amxd_trans_t trans;
    const char* domain = "domainnotfound";

    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_INFO);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    expect_string(__wrap_ares_query, name, domain);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Diagnostics.NSLookupDiagnostics");
    amxd_trans_set_value(cstring_t, &trans, "DiagnosticsState", "Requested");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "");
    // Hostname doesn't exist
    amxd_trans_set_value(cstring_t, &trans, "HostName", domain);
    amxd_trans_set_value(uint32_t, &trans, "NumberOfRepetitions", 1);
    amxd_trans_set_value(uint32_t, &trans, "Timeout", 1000);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    amxut_dm_param_equals(cstring_t, "DNS.Diagnostics.NSLookupDiagnostics", "DiagnosticsState", "Error_Other");

    clean();
}

void test_diagnostic_error_server(UNUSED void** state) {
    amxd_trans_t trans;
    const char* domain = "google.com";

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Diagnostics.NSLookupDiagnostics");
    amxd_trans_set_value(cstring_t, &trans, "DiagnosticsState", "Requested");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "");
    // Server hostname doesn't exist
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "servernotfound");
    amxd_trans_set_value(cstring_t, &trans, "HostName", domain);
    amxd_trans_set_value(uint32_t, &trans, "NumberOfRepetitions", 1);
    amxd_trans_set_value(uint32_t, &trans, "Timeout", 1000);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    amxut_dm_param_equals(cstring_t, "DNS.Diagnostics.NSLookupDiagnostics", "DiagnosticsState", "Error_DNSServerNotResolved");
}

void test_diagnostic_error_interface(UNUSED void** state) {
    amxd_trans_t trans;
    const char* domain = "google.com";

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Diagnostics.NSLookupDiagnostics");
    amxd_trans_set_value(cstring_t, &trans, "DiagnosticsState", "Requested");
    // Interface 'Device.IP.Interface.2' doesn't exist
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", domain);
    amxd_trans_set_value(cstring_t, &trans, "HostName", domain);
    amxd_trans_set_value(uint32_t, &trans, "NumberOfRepetitions", 1);
    amxd_trans_set_value(uint32_t, &trans, "Timeout", 1000);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    amxut_dm_param_equals(cstring_t, "DNS.Diagnostics.NSLookupDiagnostics", "DiagnosticsState", "Error_Internal");
}