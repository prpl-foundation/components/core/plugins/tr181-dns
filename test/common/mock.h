/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __COMMON_MOCK_H__
#define __COMMON_MOCK_H__

#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_dm.h>
#include <ares.h>
#include <netdb.h>
#include "nslookup.h"

extern const char* mod_failover_dummy_whitelist;

int __wrap_amxm_close_all(void);

void dns_unit_test_setup(void** state);
void dns_unit_test_teardown(void** state);

void change_netdevname(const char* tr181_interface, amxc_var_t* result);
void change_dhcp_option(const char* tr181_interface, const char* dnsservers);
void change_dnsservers(const char* tr181_interface, const char* dnsservers);

int var_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data);

int unit_tests_set_service(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_delete_service(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_add_server(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_set_server(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_remove_server(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_set_interface(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_remove_interface(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_add_host(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_remove_host(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_remove_host_address(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_enable_changed(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int unit_tests_rebind_changed(const char* function_name, amxc_var_t* args, amxc_var_t* ret);

amxc_var_t* new_set_service(const char* id, const char* interface, const char* protocol, bool enable, uint32_t destination_port, uint32_t ipversion);
amxc_var_t* new_delete_service(const char* id);
amxc_var_t* new_add_server(const char* name, const char* interface, const char* server, bool enable, const char* zonename);
amxc_var_t* new_remove_server(const char* name, const char* zonename);
amxc_var_t* new_set_interface(const char* interface);
amxc_var_t* new_remove_interface(const char* interface);
amxc_var_t* new_add_host(const char* name, const char* interface, const char* ipv4_addresses, const char* ipv6_addresses);
amxc_var_t* new_remove_host(const char* name, const char* interface);
amxc_var_t* new_remove_host_address(const char* name, const char* interface, const char* ipv4_addresses, const char* ipv6_addresses);

int __wrap_getaddrinfo(const char* node,
                       const char* service,
                       const struct addrinfo hints,
                       struct addrinfo** res);

void __wrap_ares_getaddrinfo(ares_channel channel,
                             const char* node,
                             const char* service,
                             const struct ares_addrinfo_hints* hints,
                             ares_addrinfo_callback callback,
                             void* arg);

void __wrap_ares_query(ares_channel channel,
                       const char* name,
                       int dnsclass,
                       int type,
                       ares_callback callback,
                       void* arg);

int __wrap_ares_parse_ns_reply(const unsigned char* abuf,
                               int alen,
                               struct hostent** host);

amxc_var_t* __wrap_netmodel_getFirstParameter(const char* intf,
                                              const char* name,
                                              const char* flag,
                                              const char* traverse);


nslookup_query_t* get_query(void);

static inline void expect_check_set_service(const char* id, const char* interface, const char* protocol, bool enable, uint32_t ipversion, uint32_t destination_port) {
    expect_check(unit_tests_set_service, args, var_equal_check, new_set_service(id, interface, protocol, enable, destination_port, ipversion));
}

static inline void expect_check_delete_service(const char* id) {
    expect_check(unit_tests_delete_service, args, var_equal_check, new_delete_service(id));
}

static inline void expect_check_set_interface(const char* interface) {
    expect_check(unit_tests_set_interface, args, var_equal_check, new_set_interface(interface));
}

static inline void expect_check_remove_interface(const char* interface) {
    expect_check(unit_tests_remove_interface, args, var_equal_check, new_remove_interface(interface));
}

static inline void expect_check_add_server(bool enable, const char* interface, const char* name, const char* server) {
    expect_check(unit_tests_add_server, args, var_equal_check, new_add_server(name, interface, server, enable, NULL));
}

static inline void expect_check_set_server(amxc_var_t** ut_var) {
    assert_non_null(ut_var);
    assert_non_null(*ut_var);
    expect_check(unit_tests_set_server, args, var_equal_check, *ut_var);
    *ut_var = NULL;
}

static inline void expect_check_remove_server(const char* name) {
    expect_check(unit_tests_remove_server, args, var_equal_check, new_remove_server(name, NULL));
}

static inline void expect_check_add_server_fwzone(bool enable, const char* interface, const char* name, const char* server, const char* zonename) {
    amxc_var_t* ut_var = new_add_server(name, interface, server, enable, zonename);
    amxc_var_t* tag = GET_ARG(ut_var, "tag");
    amxc_var_delete(&tag);
    expect_check(unit_tests_add_server, args, var_equal_check, ut_var);
}

static inline void expect_check_remove_server_fwzone(const char* name, const char* zonename) {
    amxc_var_t* ut_var = new_remove_server(name, zonename);
    amxc_var_t* tag = GET_ARG(ut_var, "tag");
    amxc_var_delete(&tag);
    expect_check(unit_tests_remove_server, args, var_equal_check, ut_var);
}

static inline void expect_check_add_host(const char* name, const char* interface, const char* ipv4_addresses, const char* ipv6_addresses) {
    expect_check(unit_tests_add_host, args, var_equal_check, new_add_host(name, interface, ipv4_addresses, ipv6_addresses));
}

static inline void expect_check_remove_host(const char* name, const char* interface) {
    expect_check(unit_tests_remove_host, args, var_equal_check, new_remove_host(name, interface));
}

static inline void expect_check_remove_host_address(const char* name, const char* interface, const char* ipv4_addresses, const char* ipv6_addresses) {
    expect_check(unit_tests_remove_host_address, args, var_equal_check, new_remove_host_address(name, interface, ipv4_addresses, ipv6_addresses));
}

#endif // __COMMON_MOCK_H__
