/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include <amxb/amxb.h>

#include "mock_netmodel.h"
#include "mock.h"

static amxc_var_t dummy_open_queries;

typedef void (* get_result_t)(amxc_var_t* args, amxc_var_t* ret);

typedef struct {
    const char* path;
    const char* netmodel_name;
    get_result_t get_result;
} convert_path_t;

static void ip_wan_get_result(amxc_var_t* args, amxc_var_t* ret);
static void lan_get_result(amxc_var_t* args, amxc_var_t* ret);
static void guest_get_result(amxc_var_t* args, amxc_var_t* ret);
static void dslite_get_result(amxc_var_t* args, amxc_var_t* ret);
static void lo_get_result(amxc_var_t* args, amxc_var_t* ret);

static convert_path_t converter[] = {
    {"Device.IP.Interface.2.", "ip-wan", ip_wan_get_result},
    {"Device.Logical.Interface.2.", "lan", lan_get_result},
    {"Device.Logical.Interface.3.", "guest", guest_get_result},
    {"Device.IP.Interface.3.", "dslite", dslite_get_result},
    {"Device.IP.Interface.1.", "lo", lo_get_result},
    {NULL, NULL, NULL}
};

static void ip_wan_get_result(amxc_var_t* args, amxc_var_t* ret) {
    const char* class = GET_CHAR(args, "class");
    assert_non_null(class);
    if(strcmp(class, "getAddrs") == 0) {
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        amxc_var_t* entry = amxc_var_add(amxc_htable_t, ret, NULL);
        amxc_var_add_key(cstring_t, entry, "NetDevName", "eth0");
    } else if(strcmp(class, "getFirstParameter") == 0) {
        const char* parameter = GET_CHAR(args, "name");
        assert_non_null(parameter);
        if(strcmp(parameter, "DNSServers") == 0) {
            amxc_var_set(cstring_t, ret, "1.2.3.4,5.6.7.8");
        } else if(strcmp(parameter, "NetDevName") == 0) {
            amxc_var_set(cstring_t, ret, "eth0");
        }
    } else if(strcmp(class, "getDHCPOption") == 0) {
        uint32_t tag = GET_UINT32(args, "tag");
        const char* type = GET_CHAR(args, "type");
        if((tag == 6) && (strcmp(type, "req") == 0)) {
            amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
            amxc_var_add(cstring_t, ret, "8.8.8.8");
            amxc_var_add(cstring_t, ret, "8.8.4.4");
        } else if((tag == 25) && (strcmp(type, "ra") == 0)) {
            amxc_var_t* servers = NULL;
            amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
            amxc_var_add_key(uint32_t, ret, "Lifetime", 120);
            servers = amxc_var_add_key(amxc_llist_t, ret, "Servers", NULL);
            amxc_var_add(cstring_t, servers, "2a02:1802:94:4200::10");
        } else {
            assert_non_null(NULL); // unsupported type or tag ?
        }
    }
}

static void lan_get_result(amxc_var_t* args, amxc_var_t* ret) {
    const char* class = GET_CHAR(args, "class");
    assert_non_null(class);
    if(strcmp(class, "getAddrs") == 0) {
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        amxc_var_t* entry = amxc_var_add(amxc_htable_t, ret, NULL);
        amxc_var_add_key(cstring_t, entry, "NetDevName", "br-lan");
        amxc_var_add_key(cstring_t, entry, "Address", "192.168.1.1");
        entry = amxc_var_add(amxc_htable_t, ret, NULL);
        amxc_var_add_key(cstring_t, entry, "NetDevName", "br-lan");
        amxc_var_add_key(cstring_t, entry, "Address", "2a02:1802:94:4330::1");
        entry = amxc_var_add(amxc_htable_t, ret, NULL);
        amxc_var_add_key(cstring_t, entry, "NetDevName", "br-lan");
        amxc_var_add_key(cstring_t, entry, "Address", "fe80::2ca0:2bff:fec2:3b91");
    } else if(strcmp(class, "getFirstParameter") == 0) {
        const char* parameter = GET_CHAR(args, "name");
        assert_non_null(parameter);
        if(strcmp(parameter, "NetDevName") == 0) {
            amxc_var_set(cstring_t, ret, "br-lan");
        }
    }
}

static void lo_get_result(amxc_var_t* args, amxc_var_t* ret) {
    const char* class = GET_CHAR(args, "class");
    assert_non_null(class);
    if(strcmp(class, "getFirstParameter") == 0) {
        const char* parameter = GET_CHAR(args, "name");
        assert_non_null(parameter);
        if(strcmp(parameter, "NetDevName") == 0) {
            amxc_var_set(cstring_t, ret, "lo");
        }
    }
}

static void guest_get_result(amxc_var_t* args, amxc_var_t* ret) {
    const char* class = GET_CHAR(args, "class");
    assert_non_null(class);
    if(strcmp(class, "getAddrs") == 0) {
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        amxc_var_t* entry = amxc_var_add(amxc_htable_t, ret, NULL);
        amxc_var_add_key(cstring_t, entry, "NetDevName", "br-guest");
    } else if(strcmp(class, "getFirstParameter") == 0) {
        const char* parameter = GET_CHAR(args, "name");
        assert_non_null(parameter);
        if(strcmp(parameter, "NetDevName") == 0) {
            amxc_var_set(cstring_t, ret, "br-guest");
        }
    }
}

static void dslite_get_result(amxc_var_t* args, amxc_var_t* ret) {
    const char* class = GET_CHAR(args, "class");
    assert_non_null(class);
    if(strcmp(class, "getAddrs") == 0) {
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        amxc_var_t* entry = amxc_var_add(amxc_htable_t, ret, NULL);
        amxc_var_add_key(cstring_t, entry, "NetDevName", "dslite0");
    } else if(strcmp(class, "getFirstParameter") == 0) {
        const char* parameter = GET_CHAR(args, "name");
        assert_non_null(parameter);
        if(strcmp(parameter, "DNSServers") == 0) {
            amxc_var_set(cstring_t, ret, "1.1.1.1,2.2.2.2");
        } else if(strcmp(parameter, "NetDevName") == 0) {
            amxc_var_set(cstring_t, ret, "dslite0");
        }
    } else if(strcmp(class, "getDHCPOption") == 0) {
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        amxc_var_add(cstring_t, ret, "4.4.4.4");
        amxc_var_add(cstring_t, ret, "4.4.8.8");
    }
}

static amxd_status_t _getResult(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    amxd_object_t* netmodel_intf_obj = amxd_object_findf(object, ".^.^");
    amxc_var_t* query = NULL;
    uint32_t index = amxd_object_get_index(object);
    const char* netmodel_intf_obj_name = amxd_object_get_name(netmodel_intf_obj, AMXD_OBJECT_NAMED);

    amxc_var_for_each(var, &dummy_open_queries) {
        const char* path = GET_CHAR(var, "netmodel_intf_name");
        assert_non_null(path);
        if((strncmp(netmodel_intf_obj_name, path, strlen(path)) == 0) &&
           (index == GET_UINT32(var, "query_index"))) {
            query = var;
            break;
        }
    }
    assert_non_null(query);

    for(int i = 0; converter[i].netmodel_name != NULL; i++) {
        if(strcmp(converter[i].netmodel_name, netmodel_intf_obj_name) == 0) {
            converter[i].get_result(query, ret);
        }
    }

    return amxd_status_ok;
}

static amxd_status_t _openQuery(amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    const char* object_name = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    amxc_var_t* var = NULL;
    amxd_trans_t trans;
    uint32_t index = -1;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_select_pathf(&trans, ".Query.");
    amxd_trans_add_inst(&trans, 0, NULL);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxc_var_for_each(subvar, &trans.retvals) {
        if(GET_ARG(subvar, "index") != NULL) {
            index = GET_UINT32(subvar, "index");
            break;
        }
    }
    assert_int_not_equal(index, -1);

    var = amxc_var_add_new(&dummy_open_queries);
    amxc_var_copy(var, args);
    amxc_var_add_key(cstring_t, var, "netmodel_intf_name", object_name);
    amxc_var_add_key(int32_t, var, "query_index", index);

    amxc_var_set(uint32_t, ret, index);

    amxd_trans_clean(&trans);

    return amxd_status_ok;
}

static amxd_status_t _closeQuery(amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    const char* class_a = GET_CHAR(args, "class");
    const char* flag_a = GET_CHAR(args, "flag");
    const char* name_a = GET_CHAR(args, "name");
    const char* type_a = GET_CHAR(args, "type");
    uint32_t tag_a = GET_UINT32(args, "tag");
    const char* name = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    amxc_var_t* query = NULL;

    amxc_var_for_each(var, &dummy_open_queries) {
        const char* class_b = GET_CHAR(var, "class");
        const char* netmodel_intf_name = GET_CHAR(var, "netmodel_intf_name");
        if((strcmp(name, netmodel_intf_name) != 0) ||
           (strcmp(class_a, class_b) != 0)) {
            continue;
        }

        if(strcmp(class_a, "getAddrs") == 0) {
            const char* flag_b = GET_CHAR(var, "flag");
            if((strcmp(flag_a, flag_b) != 0)) {
                continue;
            }

            query = var;
            break;
        } else if(strcmp(class_a, "getFirstParameter") == 0) {
            const char* flag_b = GET_CHAR(var, "flag");
            const char* name_b = GET_CHAR(var, "name");
            if((strcmp(name_a, name_b) != 0) ||
               (strcmp(flag_a, flag_b) != 0)) {
                continue;
            }

            if((strcmp(class_b, "getFirstParameter") == 0) &&
               (strcmp(name_a, name_b) != 0)) {
                continue;
            }

            query = var;
            break;
        } else if(strcmp(class_a, "getDHCPOption") == 0) {
            const char* type_b = GET_CHAR(var, "type");
            uint32_t tag_b = GET_UINT32(var, "tag");
            if((strcmp(type_a, type_b) != 0) ||
               (tag_a != tag_b)) {
                continue;
            }
            query = var;
            break;
        } else {
            assert_true(false); // unsupported query?
        }
    }

    assert_non_null(query);
    amxc_var_delete(&query);

    return amxd_status_ok;
}

static amxd_status_t _resolvePath(UNUSED amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  UNUSED amxc_var_t* args,
                                  amxc_var_t* ret) {
    const char* path = GET_CHAR(args, "path");

    for(int i = 0; converter[i].netmodel_name != NULL; i++) {
        if(strcmp(path, converter[i].path) == 0) {
            amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
            amxc_var_add(cstring_t, ret, converter[i].netmodel_name);
            break;
        }
    }
    return amxd_status_ok;
}

void change_netdevname(const char* tr181_interface, amxc_var_t* result) {
    amxc_var_t sig_data;
    amxc_var_t* data = NULL;
    amxc_var_t* query = NULL;
    amxd_object_t* query_obj = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    amxc_var_init(&sig_data);
    amxc_var_set_type(&sig_data, AMXC_VAR_ID_HTABLE);
    data = amxc_var_add_new_key(&sig_data, "Result");
    amxc_var_copy(data, result);

    amxc_var_for_each(var, &dummy_open_queries) {
        const char* class = GET_CHAR(var, "class");
        const char* flag = GET_CHAR(var, "flag");
        const char* name = GET_CHAR(var, "name");
        const char* netmodel_intf_name = GET_CHAR(var, "netmodel_intf_name");
        if((strcmp(netmodel_intf_name, tr181_interface) != 0) ||
           ((strcmp(class, "getAddrs") != 0) && ((strcmp(class, "getFirstParameter") != 0) || (strcmp(name, "NetDevName") != 0))) ||
           (strcmp(flag, "ipv4 || ipv6") != 0)) {
            continue;
        }

        query = var;
    }
    assert_non_null(query);

    amxc_string_setf(&path, "NetModel.Intf.%s.Query.%d.", tr181_interface, GET_INT32(query, "query_index"));
    query_obj = amxd_object_findf(amxd_dm_get_root(amxut_bus_dm()), amxc_string_get(&path, 0));
    assert_non_null(query_obj);

    amxd_object_send_signal(query_obj, "query", &sig_data, true);
    amxc_var_clean(&sig_data);
    amxc_string_clean(&path);
}

void change_dhcp_option(const char* tr181_interface, const char* dnsservers) {
    amxc_var_t sig_data;
    amxc_var_t* data = NULL;
    amxc_var_t* query = NULL;
    amxd_object_t* query_obj = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    amxc_var_for_each(var, &dummy_open_queries) {
        const char* class = GET_CHAR(var, "class");
        int tag = GET_INT32(var, "tag");
        const char* type = GET_CHAR(var, "type");
        const char* netmodel_intf_name = GET_CHAR(var, "netmodel_intf_name");
        if((strcmp(netmodel_intf_name, tr181_interface) != 0) ||
           (strcmp(class, "getDHCPOption") != 0) ||
           (strcmp(type, "req") != 0) ||
           (tag != 6)) {
            continue;
        }
        query = var;
    }
    assert_non_null(query);

    amxc_string_setf(&path, "NetModel.Intf.%s.Query.%d.", tr181_interface, GET_INT32(query, "query_index"));
    query_obj = amxd_object_findf(amxd_dm_get_root(amxut_bus_dm()), amxc_string_get(&path, 0));

    amxc_var_init(&sig_data);
    amxc_var_set_type(&sig_data, AMXC_VAR_ID_HTABLE);
    data = amxc_var_add_new_key(&sig_data, "Result");

    amxc_var_set(cstring_t, data, dnsservers);
    amxc_var_cast(data, AMXC_VAR_ID_LIST);

    amxd_object_send_signal(query_obj, "query", &sig_data, true);

    amxc_var_clean(&sig_data);
    amxc_string_clean(&path);
}

void change_dnsservers(const char* tr181_interface, const char* dnsservers) {
    amxc_var_t sig_data;
    amxc_var_t* data = NULL;
    amxc_var_t* query = NULL;
    amxd_object_t* query_obj = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    amxc_var_for_each(var, &dummy_open_queries) {
        const char* class = GET_CHAR(var, "class");
        const char* flag = GET_CHAR(var, "flag");
        const char* name = GET_CHAR(var, "name");
        const char* netmodel_intf_name = GET_CHAR(var, "netmodel_intf_name");
        if((strcmp(netmodel_intf_name, tr181_interface) != 0) ||
           (strcmp(class, "getFirstParameter") != 0) ||
           (strcmp(flag, "ppp-up") != 0) ||
           (strcmp(name, "DNSServers") != 0)) {
            continue;
        }
        query = var;
    }
    assert_non_null(query);

    amxc_string_setf(&path, "NetModel.Intf.%s.Query.%d.", tr181_interface, GET_INT32(query, "query_index"));
    query_obj = amxd_object_findf(amxd_dm_get_root(amxut_bus_dm()), amxc_string_get(&path, 0));

    amxc_var_init(&sig_data);
    amxc_var_set_type(&sig_data, AMXC_VAR_ID_HTABLE);
    data = amxc_var_add_new_key(&sig_data, "Result");

    amxc_var_set(cstring_t, data, dnsservers);

    amxd_object_send_signal(query_obj, "query", &sig_data, true);

    amxc_var_clean(&sig_data);
    amxc_string_clean(&path);
}

void mock_netmodel_init(amxo_parser_t* parser, amxd_object_t* root) {
    amxc_var_init(&dummy_open_queries);
    amxc_var_set_type(&dummy_open_queries, AMXC_VAR_ID_LIST);

    amxo_resolver_ftab_add(parser, "getResult", AMXO_FUNC(_getResult));
    amxo_resolver_ftab_add(parser, "openQuery", AMXO_FUNC(_openQuery));
    amxo_resolver_ftab_add(parser, "closeQuery", AMXO_FUNC(_closeQuery));
    amxo_resolver_ftab_add(parser, "resolvePath", AMXO_FUNC(_resolvePath));
    assert_int_equal(amxo_parser_parse_file(parser, "../common/mock_netmodel.odl", root), 0);
}

void mock_netmodel_clean(void) {
    amxc_var_clean(&dummy_open_queries);
}

amxc_var_t* __wrap_netmodel_getFirstParameter(const char* intf,
                                              const char* name,
                                              const char* flag,
                                              const char* traverse) {
    amxc_var_t* param = NULL;

    assert_non_null(intf);
    assert_string_equal(name, "NetDevName");
    assert_string_equal(flag, "netdev-up");
    assert_non_null(traverse);
    amxc_var_new(&param);

    if(strcmp(intf, "Device.IP.Interface.1.") == 0) {
        amxc_var_set(cstring_t, param, "dummyIntf");
    } else {
        amxc_var_set(cstring_t, param, "");
    }

    return param;
}