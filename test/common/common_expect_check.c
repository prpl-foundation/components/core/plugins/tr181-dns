/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>

#include "mock.h"
#include "common_test_dns_rebind.h"

int var_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    amxc_var_t* a = (amxc_var_t*) value;
    amxc_var_t* b = (amxc_var_t*) check_value_data;
    int result = -1;
    int rv = 0;

    print_message("<from plugin>\n");
    amxc_var_dump(a, 1);
    print_message("<from unit test>\n");
    amxc_var_dump(b, 1);

    if((amxc_var_compare(a, b, &result) != 0) ||
       (result != 0)) {
        goto exit;
    }

    rv = 1;

exit:
    amxc_var_delete(&b);
    return rv;
}

int unit_tests_set_service(UNUSED const char* function_name,
                           amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_delete_service(UNUSED const char* function_name,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_add_server(UNUSED const char* function_name,
                          UNUSED amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    amxc_var_t* var = GET_ARG(args, "path");
    if(var != NULL) { // unit tests call expect_check_add_server before amxd_trans_apply is called so at that time the instance doesn't exist and guessing the path isn't easy for transactions that add multiple instances
        amxc_var_delete(&var);
    }
    check_expected(args);
    return 0;
}

int unit_tests_set_server(UNUSED const char* function_name,
                          UNUSED amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_remove_server(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    amxc_var_t* var = GET_ARG(args, "path");
    if(var != NULL) { // don't check like unit_tests_add_server
        amxc_var_delete(&var);
    }
    check_expected(args);
    return 0;
}

int unit_tests_set_interface(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_remove_interface(UNUSED const char* function_name,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_add_host(UNUSED const char* function_name,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_remove_host(UNUSED const char* function_name,
                           amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_remove_host_address(UNUSED const char* function_name,
                                   amxc_var_t* args,
                                   UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_enable_changed(UNUSED const char* function_name,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    check_expected(args);
    return 0;
}

int unit_tests_rebind_changed(UNUSED const char* function_name,
                              UNUSED amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    if(dns_unit_test_rebind_expecting_event) {
        check_expected(args);
    }
    return 0;
}

amxc_var_t* new_set_service(const char* id, const char* interface, const char* protocol, bool enable, uint32_t destination_port, uint32_t ipversion) {
    amxc_var_t* var = NULL;
    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, var, "id", id);
    amxc_var_add_key(cstring_t, var, "interface", interface);
    amxc_var_add_key(cstring_t, var, "protocol", protocol);
    amxc_var_add_key(bool, var, "enable", enable);
    amxc_var_add_key(uint32_t, var, "destination_port", destination_port);
    amxc_var_add_key(uint32_t, var, "ipversion", ipversion);
    return var;
}

amxc_var_t* new_delete_service(const char* id) {
    amxc_var_t* var = NULL;
    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, var, "id", id);
    return var;
}

amxc_var_t* new_add_server(const char* name, const char* interface, const char* server, bool enable, const char* zonename) {
    amxc_var_t* var = NULL;
    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, var, "name", name);
    amxc_var_add_key(cstring_t, var, "interface", interface);
    amxc_var_add_key(cstring_t, var, "server", server);
    amxc_var_add_key(bool, var, "enable", enable);
    amxc_var_add_key(cstring_t, var, "tag", "");
    if(zonename != NULL) {
        amxc_var_add_key(cstring_t, var, "zone_name", zonename);
    }
    return var;
}

amxc_var_t* new_remove_server(const char* name, const char* zonename) {
    amxc_var_t* var = NULL;
    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, var, "name", name);
    if(zonename != NULL) {
        amxc_var_add_key(cstring_t, var, "zone_name", zonename);
    }
    return var;
}

amxc_var_t* new_set_interface(const char* interface) {
    amxc_var_t* var = NULL;
    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, var, "interface", interface);
    return var;
}

amxc_var_t* new_remove_interface(const char* interface) {
    return new_set_interface(interface); // same as add
}

amxc_var_t* new_add_host(const char* name, const char* interface, const char* ipv4_addresses, const char* ipv6_addresses) {
    amxc_var_t* var = NULL;
    amxc_var_t ip_addresses;

    amxc_var_init(&ip_addresses);

    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, var, "name", name);
    amxc_var_add_key(cstring_t, var, "interface", interface);

    amxc_var_set(cstring_t, &ip_addresses, ipv4_addresses);
    amxc_var_cast(&ip_addresses, AMXC_VAR_ID_LIST);
    amxc_var_add_key(amxc_llist_t, var, "ipv4_addresses", amxc_var_constcast(amxc_llist_t, &ip_addresses));

    amxc_var_set(cstring_t, &ip_addresses, ipv6_addresses);
    amxc_var_cast(&ip_addresses, AMXC_VAR_ID_LIST);
    amxc_var_add_key(amxc_llist_t, var, "ipv6_addresses", amxc_var_constcast(amxc_llist_t, &ip_addresses));

    amxc_var_clean(&ip_addresses);
    return var;
}

amxc_var_t* new_remove_host(const char* name, const char* interface) {
    amxc_var_t* var = NULL;
    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, var, "name", name);
    amxc_var_add_key(cstring_t, var, "interface", interface);
    return var;
}

amxc_var_t* new_remove_host_address(const char* name, const char* interface, const char* ipv4_addresses, const char* ipv6_addresses) {
    return new_add_host(name, interface, ipv4_addresses, ipv6_addresses); // same as add_host
}