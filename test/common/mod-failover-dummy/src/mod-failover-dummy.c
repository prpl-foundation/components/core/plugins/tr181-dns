/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>

static amxd_dm_t* g_dm = NULL;
static amxo_parser_t* g_parser = NULL;

static amxc_var_t added_servers;

static char* wl = NULL;

static const char* update_whitelist(void) {
    int amxm_rv = 0;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxm_rv = amxm_execute_function(NULL, "unit_tests", "failover_get_whitelist", &args, &ret);
    assert_int_equal(amxm_rv, 0);
    free(wl);
    wl = strdup(GET_CHAR(&ret, NULL));
    assert_non_null(wl);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return wl;
}

static int not_implemented(UNUSED const char* function_name,
                           UNUSED amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    return 0;
}

static void dummy_call_set_server(void) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* servers = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    servers = amxc_var_add_key(amxc_htable_t, &args, "servers", NULL);

    amxc_var_for_each(server, &added_servers) {
        amxc_var_add_key(amxc_htable_t, servers, GET_CHAR(server, NULL), NULL);
    }

    amxm_execute_function(NULL, "core", "set-server", &args, &ret);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static int dummy_add_server(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    const char* name = GET_CHAR(args, "name");
    amxc_var_t* var = NULL;
    assert_non_null(name);
    when_null(strstr(update_whitelist(), name), exit); // not interested in this server, should not call set_server
    var = GET_ARG(&added_servers, name);
    if(var == NULL) {
        amxc_var_add_key(cstring_t, &added_servers, name, name);
    }
    dummy_call_set_server(); // the module assumes any call to add_server indicates something changed
exit:
    return 0;
}

static int dummy_remove_server(UNUSED const char* function_name,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    const char* name = GET_CHAR(args, "name");
    amxc_var_t* var = GET_ARG(&added_servers, name);
    assert_non_null(name);
    if(var != NULL) {
        amxc_var_delete(&var);
        dummy_call_set_server();
    } // else ok if null, mod-failover knows which servers it configured
    return 0;
}

static int dummy_is_failover_activated(UNUSED const char* function_name,
                                       UNUSED amxc_var_t* args,
                                       amxc_var_t* ret) {
    amxc_var_set(bool, ret, true);
    return 0;
}

typedef struct {
    const char* name;
    amxm_callback_t amxm_cb;
} function_t;

static void module_init(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;
    function_t modfunction[] = {
        {"add-server", dummy_add_server},
        {"remove-server", dummy_remove_server},
        {"server-failure", not_implemented},
        {"is-failover-activated", dummy_is_failover_activated},
        {"set-options", not_implemented},
        {NULL, 0}
    };

    assert_int_equal(amxm_module_register(&mod, so, "failover"), 0);

    for(size_t i = 0; modfunction[i].name != NULL; i++) {
        assert_int_equal(amxm_module_add_function(mod, modfunction[i].name, modfunction[i].amxm_cb), 0);
    }

    amxc_var_init(&added_servers);
    amxc_var_set_type(&added_servers, AMXC_VAR_ID_HTABLE);
}

int _dummy_failover_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);

int _dummy_failover_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    amxm_module_t* mod = NULL;
    switch(reason) {
    case 0:     // START
        g_dm = dm;
        g_parser = parser;
        module_init();
        break;
    case 1:     // STOP
        g_parser = NULL;
        g_dm = NULL;
        mod = amxm_so_get_module(NULL, "failover");
        amxm_module_deregister(&mod);
        amxc_var_clean(&added_servers);
        free(wl);
        wl = NULL;
        break;
    default:
        break;
    }

    return 0;
}

