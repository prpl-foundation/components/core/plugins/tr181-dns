/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_path.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <amxm/amxm.h>

#include "mock.h"
#include "mock_netmodel.h"
#include "dm_relay.h"
#include "dm_client.h"
#include "dm_common.h"
#include "dm_host.h"
#include "dm_fwzone.h"
#include "dm_rebind.h"
#include "dm_diagnostics.h"
#include "tr181-dns.h"
#include "common_test_dns_rebind.h"
#include "dm_validators.h"

const char* mod_failover_dummy_whitelist = NULL;

static int unit_tests_failover_get_whitelist(UNUSED const char* function_name,
                                             UNUSED amxc_var_t* args,
                                             amxc_var_t* ret) {
    assert_non_null(mod_failover_dummy_whitelist);
    amxc_var_set(cstring_t, ret, mod_failover_dummy_whitelist);
    return 0;
}

int __wrap_amxm_close_all(void) {
    // calling amxm_close_all closes "self" which is bad for unit tests
    amxc_llist_for_each(it, amxm_get_so_list()) {
        amxm_shared_object_t* so = amxc_llist_it_get_data(it, amxm_shared_object_t, it);
        if(strcmp(so->name, "self") == 0) {
            amxm_module_t* mod = amxm_so_get_module(NULL, "core");
            amxm_module_deregister(&mod);
            continue;
        }
        amxm_so_close(&so);
    }
    return 0;
}

void dns_unit_test_setup(void** state) {
    amxd_object_t* root = NULL;
    amxm_module_t* mod = NULL;
    amxd_dm_t* g_dm = NULL;
    amxo_parser_t* g_parser = NULL;
    int retval = 0;
    const char* odl =
        "%config {"
        "    prefix_ = \"\";"
        "    include-dirs = ["
        "        \"../../odl/\","
        "        \".\""
        "    ];"
        "    modules = {"
        "        dns-directory = \"../common/modules/\","
        "        fw-directory = \"../common/modules/\""
        "    };"
        "}"
        "include \"tr181-dns_definition.odl\";"
        "#include \"test.odl\";"
        "%define {"
        "}"
        "%populate {"
        "    object DNS {"
        "        parameter SupportedDNSControllers = \"mod-dns-dummy\";"
        "        parameter DNSController = \"mod-dns-dummy\";"
        "    }"
        "}";

    amxut_bus_setup(state);
    g_dm = amxut_bus_dm();
    assert_non_null(g_dm);
    g_parser = amxut_bus_parser();
    assert_non_null(g_parser);

    root = amxd_dm_get_root(g_dm);
    assert_non_null(root);

    //events
    amxo_resolver_ftab_add(g_parser, "app_start", AMXO_FUNC(_app_start));
    amxo_resolver_ftab_add(g_parser, "core_added", AMXO_FUNC(_core_added));
    amxo_resolver_ftab_add(g_parser, "common_cleanup", AMXO_FUNC(_common_cleanup));
    amxo_resolver_ftab_add(g_parser, "core_changed", AMXO_FUNC(_core_changed));
    amxo_resolver_ftab_add(g_parser, "config_added", AMXO_FUNC(_config_added));
    amxo_resolver_ftab_add(g_parser, "config_changed", AMXO_FUNC(_config_changed));
    amxo_resolver_ftab_add(g_parser, "config_on_destroy", AMXO_FUNC(_config_on_destroy));
    amxo_resolver_ftab_add(g_parser, "enable_changed", AMXO_FUNC(_enable_changed));
    amxo_resolver_ftab_add(g_parser, "dnsmode_changed", AMXO_FUNC(_dnsmode_changed));

    amxo_resolver_ftab_add(g_parser, "fwzone_added", AMXO_FUNC(_fwzone_added));
    amxo_resolver_ftab_add(g_parser, "fwzone_name_changed", AMXO_FUNC(_fwzone_name_changed));
    amxo_resolver_ftab_add(g_parser, "fwzone_cleanup", AMXO_FUNC(_fwzone_cleanup));

    amxo_resolver_ftab_add(g_parser, "host_added", AMXO_FUNC(_host_added));
    amxo_resolver_ftab_add(g_parser, "host_changed", AMXO_FUNC(_host_changed));
    amxo_resolver_ftab_add(g_parser, "host_removed", AMXO_FUNC(_host_removed));
    amxo_resolver_ftab_add(g_parser, "host_address_added", AMXO_FUNC(_host_address_added));
    amxo_resolver_ftab_add(g_parser, "host_address_changed", AMXO_FUNC(_host_address_changed));
    amxo_resolver_ftab_add(g_parser, "host_address_removed", AMXO_FUNC(_host_address_removed));

    amxo_resolver_ftab_add(g_parser, "rebind_changed", AMXO_FUNC(_rebind_changed));
    amxo_resolver_ftab_add(g_parser, "rebind_config_event", AMXO_FUNC(_rebind_config_event));

    amxo_resolver_ftab_add(g_parser, "is_valid_ip_range", AMXO_FUNC(_is_valid_ip_range));
    amxo_resolver_ftab_add(g_parser, "is_private_ip_range", AMXO_FUNC(_is_private_ip_range));

    amxo_resolver_ftab_add(g_parser, "FlushCache", AMXO_FUNC(_FlushCache));

    amxo_resolver_ftab_add(g_parser, "state_changed", AMXO_FUNC(_state_changed));
    amxo_resolver_ftab_add(g_parser, "option_changed", AMXO_FUNC(_option_changed));

    // to be able to use cmocka's expect_check
    amxm_module_register(&mod, NULL, "unit_tests"); // will be called by dummy modules
    amxm_module_add_function(mod, "fw_set_service", unit_tests_set_service);
    amxm_module_add_function(mod, "fw_delete_service", unit_tests_delete_service);
    amxm_module_add_function(mod, "dns_add_server", unit_tests_add_server);
    amxm_module_add_function(mod, "dns_set_server", unit_tests_set_server);
    amxm_module_add_function(mod, "dns_remove_server", unit_tests_remove_server);
    amxm_module_add_function(mod, "dns_set_interface", unit_tests_set_interface);
    amxm_module_add_function(mod, "dns_remove_interface", unit_tests_remove_interface);
    amxm_module_add_function(mod, "dns_add_host", unit_tests_add_host);
    amxm_module_add_function(mod, "dns_remove_host", unit_tests_remove_host);
    amxm_module_add_function(mod, "dns_remove_host_address", unit_tests_remove_host_address);
    amxm_module_add_function(mod, "dns_enable_changed", unit_tests_enable_changed);
    amxm_module_add_function(mod, "dns_rebind_changed", unit_tests_rebind_changed);
    amxm_module_add_function(mod, "failover_get_whitelist", unit_tests_failover_get_whitelist);

    mock_netmodel_init(g_parser, root);

    retval = amxo_parser_parse_string(g_parser, odl, root);
    printf("PARSER MESSAGE = %s\n", amxc_string_get(&g_parser->msg, 0));
    assert_int_equal(retval, 0);

    retval = amxo_parser_invoke_entry_points(g_parser, g_dm, AMXO_START);
    assert_int_equal(retval, 0);
    amxut_bus_handle_events();

    assert_int_equal(_tr181_dns_main(AMXO_START, g_dm, g_parser), 0);
    amxut_bus_handle_events();

    amxp_sigmngr_emit_signal(&g_dm->sigmngr, "app:start", NULL);
    amxut_bus_handle_events();
}

void dns_unit_test_teardown(void** state) {
    amxd_dm_t* g_dm = NULL;
    int retval = 0;

    g_dm = amxut_bus_dm();

    retval = amxo_parser_invoke_entry_points(amxut_bus_parser(), g_dm, AMXO_STOP);
    assert_int_equal(retval, 0);
    amxut_bus_handle_events();
    assert_int_equal(_tr181_dns_main(AMXO_STOP, g_dm, amxut_bus_parser()), 0);
    amxut_bus_handle_events();

    amxp_sigmngr_emit_signal(&g_dm->sigmngr, "app:stop", NULL);
    amxut_bus_handle_events();

    mock_netmodel_clean();

    amxut_bus_teardown(state);
}