/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <netdb.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_object_event.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include <ares.h>
#include <arpa/nameser.h>

#include "mock.h"
#include "nslookup.h"

static nslookup_query_t* query;


int __wrap_getaddrinfo(const char* node,
                       UNUSED const char* service,
                       UNUSED const struct addrinfo hints,
                       struct addrinfo** res) {
    struct addrinfo* result = NULL;
    int rv = 1;
    const char* ip = "129.129.129.129";

    check_expected(node);
    assert_non_null(res);

    if(strcmp(node, "servernotfound") != 0) {
        result = calloc(1, sizeof(struct addrinfo));
        result->ai_family = AF_INET;
        result->ai_addr = (struct sockaddr*) ip;
        rv = 0;
    }

    *res = result;

    return rv;
}

void __wrap_ares_getaddrinfo(ares_channel channel,
                             const char* node,
                             UNUSED const char* service,
                             UNUSED const struct ares_addrinfo_hints* hints,
                             ares_addrinfo_callback callback,
                             void* arg) {
    int status;

    assert_non_null(channel);
    check_expected(node);
    assert_non_null(callback);
    assert_non_null(arg);

    query = (nslookup_query_t*) arg;

    if(strcmp(node, "notfounddomain") == 0) {
        status = ARES_ENOTFOUND;
    } else {
        status = ARES_SUCCESS;
    }

    callback(arg, status, 0, NULL);
}

void __wrap_ares_query(ares_channel channel,
                       const char* name,
                       int dnsclass,
                       int type,
                       ares_callback callback,
                       void* arg) {
    int status;
    unsigned char abuf[3] = {0};

    assert_non_null(channel);
    check_expected(name);
    assert_int_equal(dnsclass, C_IN);
    assert_int_equal(type, T_NS);
    assert_non_null(callback);
    assert_non_null(arg);

    query = (nslookup_query_t*) arg;

    if(strcmp(name, "domainnotfound") == 0) {
        status = ARES_ENOTFOUND;
    } else {
        status = ARES_SUCCESS;
    }

    callback(arg, status, 0, abuf, 3);
}

int __wrap_ares_parse_ns_reply(const unsigned char* abuf,
                               UNUSED int alen,
                               UNUSED struct hostent** host) {
    assert_non_null(abuf);
    assert_non_null(host);

    return 0;
}

nslookup_query_t* get_query(void) {
    return query;
}