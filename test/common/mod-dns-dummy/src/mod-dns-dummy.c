/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <amxb/amxb.h>

static void call_mod_unit_test(const char* function, amxc_var_t* args) {
    amxc_var_t ret;
    amxc_var_init(&ret);
    amxm_execute_function(NULL, "unit_tests", function, args, &ret);
    amxc_var_clean(&ret);
}

static int not_implemented(UNUSED const char* function_name,
                           UNUSED amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    return 0;
}

static int dummy_add_server(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    call_mod_unit_test("dns_add_server", args);
    return 0;
}

static int dummy_set_server(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    call_mod_unit_test("dns_set_server", args);
    return 0;
}

static int dummy_remove_server(UNUSED const char* function_name,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    call_mod_unit_test("dns_remove_server", args);
    return 0;
}

static int dummy_set_interface(UNUSED const char* function_name,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    call_mod_unit_test("dns_set_interface", args);
    return 0;
}

static int dummy_remove_interface(UNUSED const char* function_name,
                                  amxc_var_t* args,
                                  UNUSED amxc_var_t* ret) {
    call_mod_unit_test("dns_remove_interface", args);
    return 0;
}

static int dummy_add_host(UNUSED const char* function_name,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    call_mod_unit_test("dns_add_host", args);
    return 0;
}

static int dummy_remove_host(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    call_mod_unit_test("dns_remove_host", args);
    return 0;
}

static int dummy_remove_host_address(UNUSED const char* function_name,
                                     amxc_var_t* args,
                                     UNUSED amxc_var_t* ret) {
    call_mod_unit_test("dns_remove_host_address", args);
    return 0;
}

static int dummy_enable_changed(UNUSED const char* function_name,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    call_mod_unit_test("dns_enable_changed", args);
    return 0;
}

static int dummy_rebind_changed(UNUSED const char* function_name,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    call_mod_unit_test("dns_rebind_changed", args);
    return 0;
}

typedef struct {
    const char* name;
    amxm_callback_t callback;
} function_t;

static AMXM_CONSTRUCTOR module_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;
    function_t function[] = {
        {"add-server", dummy_add_server},
        {"set-server", dummy_set_server},
        {"remove-server", dummy_remove_server},
        {"add-host", dummy_add_host},
        {"remove-host", dummy_remove_host},
        {"remove-host-address", dummy_remove_host_address},
        {"set-interface", dummy_set_interface},
        {"remove-interface", dummy_remove_interface},
        {"enable-changed", dummy_enable_changed},
        {"debug", not_implemented},
        {"flush-cache", not_implemented},
        {"rebind-changed", dummy_rebind_changed},
        {NULL, 0}
    };

    amxm_module_register(&mod, so, "dns");
    for(size_t i = 0; function[i].name != NULL; i++) {
        amxm_module_add_function(mod, function[i].name, function[i].callback);
    }

    return 0;
}
