/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_parameter.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "tr181-dns.h"
#include "test_dns_rebind.h"
#include "../common/mock.h"
#include "common_test_dns_rebind.h"

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;

int test_setup(void** state) {
    dns_unit_test_rebind_setup();

    expect_rebind_changed_event(true, "", "");
    dns_unit_test_setup(state);
    dm = amxut_bus_dm();
    parser = amxut_bus_parser();
    return 0;
}

int test_teardown(void** state) {
    dns_unit_test_teardown(state);
    dm = NULL;
    parser = NULL;
    dns_unit_test_rebind_teardown();
    return 0;
}

static void assert_rebind_protection_enable_equals(bool enable) {
    amxd_object_t* obj = amxd_dm_findf(dm, "DNS.RebindProtection.");
    assert_int_equal(amxd_object_get_bool(obj, "Enable", NULL), enable);
}

static void set_rebind_protection_enable(bool enable) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    assert_int_equal(amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true), amxd_status_ok);
    assert_int_equal(amxd_trans_select_pathf(&trans, "%s", "DNS.RebindProtection."), amxd_status_ok);

    assert_int_equal(amxd_trans_set_bool(&trans, "Enable", enable), amxd_status_ok);

    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_rebind_is_enabled_by_default(UNUSED void** state) {
    assert_rebind_protection_enable_equals(true);
}

void test_rebind_can_be_disabled(UNUSED void** state) {
    expect_rebind_changed_event(false, "", "");

    set_rebind_protection_enable(false);
    assert_rebind_protection_enable_equals(false);
}

void test_rebind_can_be_enabled(UNUSED void** state) {
    expect_rebind_changed_event(true, "", "");

    set_rebind_protection_enable(true);
    assert_rebind_protection_enable_equals(true);
}

static amxd_status_t add_rebind_exception(const cstring_t exception, const cstring_t address) {
    amxd_status_t rv = -1;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    assert_int_equal(amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true), amxd_status_ok);

    assert_int_equal(amxd_trans_select_pathf(&trans, "DNS.RebindProtection.%s", exception), amxd_status_ok);
    assert_int_equal(amxd_trans_add_inst(&trans, 0, NULL), amxd_status_ok);
    assert_int_equal(amxd_trans_set_cstring_t(&trans, "Address", address), amxd_status_ok);

    rv = amxd_trans_apply(&trans, dm);
    amxut_bus_handle_events();

    amxd_trans_clean(&trans);
    return rv;
}

void test_rebind_protection_exceptions_can_be_added(UNUSED void** state) {
    expect_rebind_changed_event(true, "192.168.18.0/24", "");
    assert_int_equal(add_rebind_exception("IPExceptions", "192.168.18.0/24"), amxd_status_ok);

    expect_rebind_changed_event(true, "192.168.18.0/24,10.10.0.0/16", "");
    assert_int_equal(add_rebind_exception("IPExceptions", "10.10.0.0/16"), amxd_status_ok);

    expect_rebind_changed_event(true, "192.168.18.0/24,10.10.0.0/16,fd00::/8", "");
    assert_int_equal(add_rebind_exception("IPExceptions", "fd00::/8"), amxd_status_ok);

    expect_rebind_changed_event(true, "192.168.18.0/24,10.10.0.0/16,fd00::/8,fe80::/10", "");
    assert_int_equal(add_rebind_exception("IPExceptions", "fe80::/10"), amxd_status_ok);

    expect_rebind_changed_event(true, "192.168.18.0/24,10.10.0.0/16,fd00::/8,fe80::/10,::1/128", "");
    assert_int_equal(add_rebind_exception("IPExceptions", "::1/128"), amxd_status_ok);

    expect_rebind_changed_event(true, "192.168.18.0/24,10.10.0.0/16,fd00::/8,fe80::/10,::1/128,127.0.1.1/24", "");
    assert_int_equal(add_rebind_exception("IPExceptions", "127.0.1.1/24"), amxd_status_ok);

    expect_rebind_changed_event(true, "192.168.18.0/24,10.10.0.0/16,fd00::/8,fe80::/10,::1/128,127.0.1.1/24", "example.com");
    assert_int_equal(add_rebind_exception("DomainExceptions", "example.com"), amxd_status_ok);

    expect_rebind_changed_event(true, "192.168.18.0/24,10.10.0.0/16,fd00::/8,fe80::/10,::1/128,127.0.1.1/24", "example.com,example.org");
    assert_int_equal(add_rebind_exception("DomainExceptions", "example.org"), amxd_status_ok);

    assert_rebind_protection_enable_equals(true);
}

void test_rebind_protection_invalid_exceptions_are_blocked(UNUSED void** state) {
    // Range missing
    assert_int_equal(add_rebind_exception("IPExceptions", "192.16.18.0"), amxd_status_invalid_value);
    // Not a private range
    assert_int_equal(add_rebind_exception("IPExceptions", "192.167.18.0/24"), amxd_status_invalid_value);
    assert_int_equal(add_rebind_exception("IPExceptions", "fe00::/8"), amxd_status_invalid_value);

    assert_rebind_protection_enable_equals(true);
}
