/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "tr181-dns.h"
#include "test_dns_fwzone.h"
#include "mock.h"

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;

int test_fwzone_setup(void** state) {
    dns_unit_test_setup(state);
    dm = amxut_bus_dm();
    parser = amxut_bus_parser();
    return 0;
}

int test_fwzone_teardown(void** state) {
    dns_unit_test_teardown(state);
    dm = NULL;
    parser = NULL;
    return 0;
}

void test_fwzone_forwarding_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.ForwardZone.");
    amxd_trans_add_inst(&trans, 0, "fwzone-1");
    amxd_trans_set_value(cstring_t, &trans, "Name", "google.com");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".Forwarding.");
    amxd_trans_add_inst(&trans, 0, "fwzone-forwarding-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server_fwzone(true, "eth0", "8.8.8.8", "8.8.8.8", "google.com");
    expect_check_add_server_fwzone(true, "eth0", "8.8.4.4", "8.8.4.4", "google.com");
    amxut_bus_handle_events();
}

void test_fwzone_enable_change(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.ForwardZone.fwzone-1.");
    amxd_trans_set_value(bool, &trans, "Enable", false);

    expect_check_remove_server_fwzone("8.8.8.8", "google.com");
    expect_check_remove_server_fwzone("8.8.4.4", "google.com");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.ForwardZone.fwzone-1.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_check_add_server_fwzone(true, "eth0", "8.8.8.8", "8.8.8.8", "google.com");
    expect_check_add_server_fwzone(true, "eth0", "8.8.4.4", "8.8.4.4", "google.com");
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_fwzone_name_change(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.ForwardZone.fwzone-1.");
    amxd_trans_set_value(cstring_t, &trans, "Name", "reddit.com");

    expect_check_remove_server_fwzone("8.8.8.8", "google.com");
    expect_check_remove_server_fwzone("8.8.4.4", "google.com");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server_fwzone(true, "eth0", "8.8.8.8", "8.8.8.8", "reddit.com");
    expect_check_add_server_fwzone(true, "eth0", "8.8.4.4", "8.8.4.4", "reddit.com");
    amxut_bus_handle_events();
}

void test_fwzone_forwarding_server_change(UNUSED void** state) {
    change_dhcp_option("ip-wan", "2a02:1802:94:4200::10");

    expect_check_remove_server_fwzone("8.8.8.8", "reddit.com");
    expect_check_remove_server_fwzone("8.8.4.4", "reddit.com");
    expect_check_add_server_fwzone(true, "eth0", "2a02:1802:94:4200::10", "2a02:1802:94:4200::10", "reddit.com");
    amxut_bus_handle_events();
}

void test_fwzone_forwarding_remove(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.ForwardZone.fwzone-1.Forwarding.");
    amxd_trans_del_inst(&trans, 0, "fwzone-forwarding-1");
    expect_check_remove_server_fwzone("2a02:1802:94:4200::10", "reddit.com");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}