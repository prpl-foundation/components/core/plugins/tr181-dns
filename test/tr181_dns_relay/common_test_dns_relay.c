/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "tr181-dns.h"
#include "common_test_dns_relay.h"
#include "mock.h"

void test_relay_type_static_add(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add 1 instance of 'DNS.Relay.Forwarding.' (Type==Static)\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "dns-1");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "8.8.8.8");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "dns-1", "8.8.8.8");
    amxut_bus_handle_events();

    amxd_object_t* obj = NULL;
    obj = amxd_dm_findf(amxut_bus_dm(), "DNS.Relay.Forwarding.dns-1");
    assert_non_null(obj);
    amxd_param_t* param = NULL;
    param = amxd_object_get_param_def(obj, "DNSServer");
    assert_true(amxd_param_has_flag(param, "usersetting"));
    assert_true(amxd_param_is_attr_set(param, amxd_pattr_persistent));
}

void test_relay_type_static_change_enable(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: change parameter 'DNS.Relay.Forwarding.dns-1.Enable' to false\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.dns-1.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    expect_check_remove_server("dns-1");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    print_message("step: change parameter 'DNS.Relay.Forwarding.dns-1.Enable' to true\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.dns-1.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "dns-1", "8.8.8.8");
    amxut_bus_handle_events();
}

void test_relay_type_static_change_dnsserver(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: change parameter 'DNS.Relay.Forwarding.dns-1.DNSServer'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.dns-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "8.8.4.4");
    expect_check_remove_server("dns-1");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "dns-1", "8.8.4.4");
    amxut_bus_handle_events();
}

void test_relay_type_static_remove(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: remove instance 'DNS.Relay.Forwarding.dns-1.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_del_inst(&trans, 0, "dns-1");
    expect_check_remove_server("dns-1");
    print_message("expectation: 'remove_server' of the dns controller is called\n");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_relay_type_dhcpv4_add(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add 1 instance of 'DNS.Relay.Forwarding.' (Type==DHCPv4)\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "dns-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'DNS.Relay.Forwarding.dns-1.DNSServer' should have the first DHCPv4 option 6\n");
    expect_check_add_server(true, "eth0", "dns-1", "8.8.8.8");
    amxut_bus_handle_events();

    amxd_object_t* obj = NULL;
    obj = amxd_dm_findf(amxut_bus_dm(), "DNS.Relay.Forwarding.dns-1");
    assert_non_null(obj);
    amxd_param_t* param = NULL;
    param = amxd_object_get_param_def(obj, "DNSServer");
    assert_false(amxd_param_has_flag(param, "usersetting"));
    assert_false(amxd_param_is_attr_set(param, amxd_pattr_persistent));
}

void test_relay_type_dhcpv4_add_second(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add a second instance of 'DNS.Relay.Forwarding.' with the same type and interface\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "dns-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'DNS.Relay.Forwarding.dns-2.DNSServer' should have the second DHCPv4 option 6\n");
    expect_check_add_server(true, "eth0", "dns-2", "8.8.4.4");
    amxut_bus_handle_events();
}

void test_relay_change_enable(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: change parameter 'DNS.Relay.Enable' to false\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    print_message("expectation: function 'enable_changed' of dns controller is called\n");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("dns-1");
    expect_check_remove_server("dns-2");
    amxut_bus_handle_events();

    print_message("step: change parameter 'DNS.Relay.Enable' to true\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    print_message("expectation: function 'enable_changed' of dns controller is called\n");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "dns-1", "8.8.8.8");
    expect_check_add_server(true, "eth0", "dns-2", "8.8.4.4");
    amxut_bus_handle_events();
}

void test_relay_type_dhcpv4_add_third(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add a third instance of 'DNS.Relay.Forwarding.' with the same type and interface\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "dns-3");
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'DNS.Relay.Forwarding.dns-3.DNSServer' should be empty because there are only 2 addresses in DHCPv4 option 6\n");
    amxut_bus_handle_events();
}

void test_relay_type_dhcpv4_remove(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: remove instance 'DNS.Relay.Forwarding.dns-3.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_del_inst(&trans, 0, "dns-3");
    // mod-dns's remove_server is not called because DNSServer parameter was empty (only 2 dhcpv4 option 6 ip addresses, 1 for dns-1 and 1 for dns-2)
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation : nothing should change for dns-1 and dns-2\n");
    amxut_bus_handle_events();

    print_message("step: remove instance 'DNS.Relay.Forwarding.dns-1.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_del_inst(&trans, 0, "dns-1");
    expect_check_remove_server("dns-1");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation : nothing should change for dns-2\n");
    amxut_bus_handle_events();

    print_message("step: remove instance 'DNS.Relay.Forwarding.dns-2.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_del_inst(&trans, 0, "dns-2");
    expect_check_remove_server("dns-2");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_relay_type_ppp_add(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add 1 instance of 'DNS.Relay.Forwarding.' (Type==IPCP)\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "ppp-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "IPCP");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'DNS.Relay.Forwarding.dns-1.DNSServer' should have the first DHCPv4 option 6\n");
    expect_check_add_server(true, "eth0", "ppp-1", "1.2.3.4");
    amxut_bus_handle_events();
}

void test_relay_type_ppp_add_second(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add a second instance of 'DNS.Relay.Forwarding.' with the same type and interface\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "ppp-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "IPCP");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'DNS.Relay.Forwarding.ppp-2.DNSServer' should have the second dns server\n");
    expect_check_add_server(true, "eth0", "ppp-2", "5.6.7.8");
    amxut_bus_handle_events();
}

void test_relay_type_ppp_add_third(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add a third instance of 'DNS.Relay.Forwarding.' with the same type and interface\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "ppp-3");
    amxd_trans_set_value(cstring_t, &trans, "Type", "IPCP");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'DNS.Relay.Forwarding.ppp-3.DNSServer' should be empty because there are only 2 dns servers\n");
    amxut_bus_handle_events();
}

void test_relay_type_ppp_remove(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: remove instance 'DNS.Relay.Forwarding.ppp-3.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_del_inst(&trans, 0, "ppp-3");
    // mod-dns's remove_server is not called because DNSServer parameter was empty (only 2 ipcp ip addresses, 1 for ppp-1 and 1 for ppp-2)
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation : nothing should change for ppp-1 and ppp-2\n");
    amxut_bus_handle_events();

    print_message("step: remove instance 'DNS.Relay.Forwarding.ppp-1.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_del_inst(&trans, 0, "ppp-1");
    expect_check_remove_server("ppp-1");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation : nothing should change for ppp-2\n");
    amxut_bus_handle_events();

    print_message("step: remove instance 'DNS.Relay.Forwarding.ppp-2.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_del_inst(&trans, 0, "ppp-2");
    expect_check_remove_server("ppp-2");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_relay_type_dhcpv4_change_interface(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add 1 instance of 'DNS.Relay.Forwarding.' (Type==DHCPv4)\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "dns-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'DNS.Relay.Forwarding.dns-1.DNSServer' should have the first DHCPv4 option 6\n");
    expect_check_add_server(true, "eth0", "dns-1", "8.8.8.8");
    amxut_bus_handle_events();

    print_message("step: change parameter 'DNS.Relay.Forwarding.dns-1.Interface'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.dns-1.");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.3.");
    expect_check_remove_server("dns-1");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: function 'remove_server' and then 'add_server' of dns controller is called\n");
    expect_check_add_server(true, "dslite0", "dns-1", "4.4.4.4");
    amxut_bus_handle_events();

    print_message("step: remove instance 'DNS.Relay.Forwarding.dns-1.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_del_inst(&trans, 0, "dns-1");
    print_message("expectation: 'server_remove' of dns controller is called\n");
    expect_check_remove_server("dns-1");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_relay_type_dhcpv4_change_dhcp_option(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add 1 instance of 'DNS.Relay.Forwarding.' (Type==DHCPv4)\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "dns-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'DNS.Relay.Forwarding.dns-1.DNSServer' should have the first DHCPv4 option 6\n");
    expect_check_add_server(true, "eth0", "dns-1", "8.8.8.8");
    amxut_bus_handle_events();

    print_message("step: update query result for dhcp option dnsservers to '[6.6.6.6, 7.7.7.7]' (list of 2 items)\n");
    change_dhcp_option("ip-wan", "6.6.6.6,7.7.7.7");
    print_message("expectation: function 'remove_server' and then 'add_server' of dns controller is called\n");
    expect_check_remove_server("dns-1");
    expect_check_add_server(true, "eth0", "dns-1", "6.6.6.6");
    amxut_bus_handle_events();

    print_message("step: update query result for dhcp option dnsservers to '[]' (empty list)\n");
    change_dhcp_option("ip-wan", "");
    print_message("expectation: function 'remove_server' of dns controller is called\n");
    expect_check_remove_server("dns-1");
    amxut_bus_handle_events();

    print_message("step: update query result for dhcp option dnsservers to '[8.8.8.8,8.8.4.4]' (list of 2 items)\n");
    change_dhcp_option("ip-wan", "8.8.8.8,8.8.4.4");
    print_message("expectation: function 'add_server' of dns controller is called\n");
    expect_check_add_server(true, "eth0", "dns-1", "8.8.8.8");
    amxut_bus_handle_events();

    print_message("step: remove instance 'DNS.Relay.Forwarding.dns-1.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_del_inst(&trans, 0, "dns-1");
    print_message("expectation: 'server_remove' of dns controller is called\n");
    expect_check_remove_server("dns-1");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_relay_type_ppp_change_query_result_dnsserver(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add 1 instance of 'DNS.Relay.Forwarding.' (Type==IPCP)\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "ppp-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "IPCP");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'DNS.Relay.Forwarding.dns-1.DNSServer' should have the first address of netmodel's DNSServers\n");
    expect_check_add_server(true, "eth0", "ppp-1", "1.2.3.4");
    amxut_bus_handle_events();

    print_message("step: update query result for DNSServers to '6.6.6.6, 7.7.7.7'\n");
    change_dnsservers("ip-wan", "6.6.6.6,7.7.7.7");
    print_message("expectation: function 'remove_server' and then 'add_server' of dns controller is called\n");
    expect_check_remove_server("ppp-1");
    expect_check_add_server(true, "eth0", "ppp-1", "6.6.6.6");
    amxut_bus_handle_events();

    print_message("step: update query result for dhcp option dnsservers to '' (empty string)\n");
    change_dnsservers("ip-wan", "");
    print_message("expectation: function 'remove_server' of dns controller is called\n");
    expect_check_remove_server("ppp-1");
    amxut_bus_handle_events();

    print_message("step: update query result for dhcp option dnsservers to '1.2.3.4,5.6.7.8'\n");
    change_dnsservers("ip-wan", "1.2.3.4,5.6.7.8");
    print_message("expectation: function 'add_server' of dns controller is called\n");
    expect_check_add_server(true, "eth0", "ppp-1", "1.2.3.4");
    amxut_bus_handle_events();

    print_message("step: remove instance 'DNS.Relay.Forwarding.ppp-1.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_del_inst(&trans, 0, "ppp-1");
    print_message("expectation: 'server_remove' of dns controller is called\n");
    expect_check_remove_server("ppp-1");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}