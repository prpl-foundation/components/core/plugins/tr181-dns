/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "tr181-dns.h"
#include "test_dns_relay.h"
#include "common_test_dns_relay.h"
#include "mock.h"

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;

int test_setup(void** state) {
    dns_unit_test_setup(state);
    dm = amxut_bus_dm();
    parser = amxut_bus_parser();
    return 0;
}

int test_teardown(void** state) {
    dns_unit_test_teardown(state);
    dm = NULL;
    parser = NULL;
    return 0;
}

void test_relay_type_router_advertisement_add(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("step: add 1 instance of 'DNS.Relay.Forwarding.' (Type==RouterAdvertisement)\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "dns-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "RouterAdvertisement");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    print_message("expectation: 'DNS.Relay.Forwarding.dns-1.DNSServer' should have the first RDNSS option\n");
    expect_check_add_server(true, "eth0", "dns-1", "2a02:1802:94:4200::10");
    amxut_bus_handle_events();

    print_message("step: remove instance 'DNS.Relay.Forwarding.dns-1.'\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_del_inst(&trans, 0, "dns-1");
    expect_check_remove_server("dns-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_config_add(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.");
    amxd_trans_add_inst(&trans, 0, "config-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_config_change_cache(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(uint32_t, &trans, "CacheSize", 1000);
    amxd_trans_set_value(uint32_t, &trans, "CacheMinTTL", 10000);
    amxd_trans_set_value(uint32_t, &trans, "CacheMaxTTL", 20000);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_config_change_interface(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.Logical.Interface.3.");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_set_service("dns-1", "Device.Logical.Interface.3.", "17,6", true, 0, 53);
    expect_check_set_interface("br-guest");
    amxut_bus_handle_events();
}

void test_config_flush_cache(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;
    amxd_object_t* object = amxd_dm_findf(dm, "DNS.Relay.Config.config-1.");

    assert_non_null(object);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    rv = amxd_object_invoke_function(object, "FlushCache", &args, &ret);
    assert_int_equal(rv, 0);
    amxut_bus_handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_config_change_domain_name(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "DomainName", "lan");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_config_remove(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.");
    amxd_trans_del_inst(&trans, 0, "config-1");
    expect_check_remove_interface("br-guest");
    expect_check_delete_service("dns-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();
}

void test_change_dnsmode(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("add config, no DNS.Relay.Forwarding instances so expect nothing to happen\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.");
    amxd_trans_add_inst(&trans, 0, "config-1");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Any");
    amxd_trans_set_value(cstring_t, &trans, "IPv6DNSMode", "Any");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    print_message("add relays (dhcpv4, dhcpv6, ppp, ra, static) DNSMode = any IPV6DNSMode = any\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "staticv4-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "100.100.0.1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "staticv4-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "100.100.0.2");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "staticv6-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "100::1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "staticv6-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "100::2");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "dhcpv4-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "dhcpv4-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "ppp-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "IPCP");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "ppp-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "IPCP");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "ra-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "RouterAdvertisement");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "ra-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "RouterAdvertisement");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    expect_check_add_server(true, "eth0", "staticv4-1", "100.100.0.1");
    expect_check_add_server(true, "eth0", "staticv6-1", "100::1");
    expect_check_add_server(true, "eth0", "dhcpv4-1", "8.8.8.8");
    expect_check_add_server(true, "eth0", "ppp-1", "1.2.3.4");
    expect_check_add_server(true, "eth0", "ra-1", "2a02:1802:94:4200::10");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    print_message("change DNSMode = Static (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Static");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("dhcpv4-1");
    expect_check_remove_server("ppp-1");
    amxut_bus_handle_events();

    print_message("change DNSMode = Any (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Any");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "dhcpv4-1", "8.8.8.8");
    expect_check_add_server(true, "eth0", "ppp-1", "1.2.3.4");
    amxut_bus_handle_events();

    print_message("change DNSMode = Dynamic (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Dynamic");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("staticv4-1");
    amxut_bus_handle_events();

    print_message("change DNSMode = Any (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Any");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "staticv4-1", "100.100.0.1");
    amxut_bus_handle_events();

    print_message("change DNSMode = Static (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Static");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("dhcpv4-1");
    expect_check_remove_server("ppp-1");
    amxut_bus_handle_events();

    print_message("change DNSMode = Dynamic (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Dynamic");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "dhcpv4-1", "8.8.8.8");
    expect_check_add_server(true, "eth0", "ppp-1", "1.2.3.4");
    expect_check_remove_server("staticv4-1");
    amxut_bus_handle_events();

    print_message("change DNSMode = Static (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Static");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("dhcpv4-1");
    expect_check_remove_server("ppp-1");
    expect_check_add_server(true, "eth0", "staticv4-1", "100.100.0.1");
    amxut_bus_handle_events();

    print_message("change staticv6-1 DNSServer to ipv4 (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.staticv6-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "30.30.30.30");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("staticv6-1"); // because IPv6DNSMode = Any
    expect_check_add_server(true, "eth0", "staticv6-1", "30.30.30.30");
    amxut_bus_handle_events();

    print_message("change staticv6-1 DNSServer to ipv6 (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.staticv6-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "100::1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("staticv6-1");
    expect_check_add_server(true, "eth0", "staticv6-1", "100::1"); // because IPv6DNSMode = Any
    amxut_bus_handle_events();

    print_message("change DNSMode = Any (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Any");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "dhcpv4-1", "8.8.8.8");
    expect_check_add_server(true, "eth0", "ppp-1", "1.2.3.4");
    amxut_bus_handle_events();
}

void test_change_ipv6dnsmode(UNUSED void** state) {
    amxd_trans_t trans;

    print_message("add config, no DNS.Relay.Forwarding instances so expect nothing to happen\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.");
    amxd_trans_add_inst(&trans, 0, "config-1");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Any");
    amxd_trans_set_value(cstring_t, &trans, "IPv6DNSMode", "Any");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    print_message("add relays (dhcpv4, dhcpv6, ppp, ra, static) DNSMode = any IPV6DNSMode = any\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "staticv4-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "100.100.0.1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "staticv4-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "100.100.0.2");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "staticv6-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "100::1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "staticv6-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "100::2");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "dhcpv4-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "dhcpv4-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "ppp-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "IPCP");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "ppp-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "IPCP");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "ra-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "RouterAdvertisement");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "ra-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "RouterAdvertisement");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    expect_check_add_server(true, "eth0", "staticv4-1", "100.100.0.1");
    expect_check_add_server(true, "eth0", "staticv6-1", "100::1");
    expect_check_add_server(true, "eth0", "dhcpv4-1", "8.8.8.8");
    expect_check_add_server(true, "eth0", "ppp-1", "1.2.3.4");
    expect_check_add_server(true, "eth0", "ra-1", "2a02:1802:94:4200::10");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    print_message("change IPv6DNSMode = Static (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6DNSMode", "Static");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("ra-1");
    amxut_bus_handle_events();

    print_message("change IPv6DNSMode = Any (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6DNSMode", "Any");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "ra-1", "2a02:1802:94:4200::10");
    amxut_bus_handle_events();

    print_message("change IPv6DNSMode = Dynamic (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6DNSMode", "Dynamic");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("staticv6-1");
    amxut_bus_handle_events();

    print_message("change IPv6DNSMode = Any (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6DNSMode", "Any");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "staticv6-1", "100::1");
    amxut_bus_handle_events();

    print_message("change IPv6DNSMode = Static (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6DNSMode", "Static");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("ra-1");
    amxut_bus_handle_events();

    print_message("change IPv6DNSMode = Dynamic (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6DNSMode", "Dynamic");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "ra-1", "2a02:1802:94:4200::10");
    expect_check_remove_server("staticv6-1");
    amxut_bus_handle_events();

    print_message("change IPv6DNSMode = Static (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6DNSMode", "Static");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("ra-1");
    expect_check_add_server(true, "eth0", "staticv6-1", "100::1");
    amxut_bus_handle_events();

    print_message("change staticv4-1 DNSServer to ipv6 (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.staticv4-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "30::30");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("staticv4-1"); // because DNSMode = Any
    expect_check_add_server(true, "eth0", "staticv4-1", "30::30");
    amxut_bus_handle_events();

    print_message("change staticv4-1 DNSServer to ipv4 (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.staticv4-1.");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "100.100.0.1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_remove_server("staticv4-1");
    expect_check_add_server(true, "eth0", "staticv4-1", "100.100.0.1"); // because DNSMode = Any
    amxut_bus_handle_events();

    print_message("change IPv6DNSMode = Any (%s:%d)\n", __func__, __LINE__);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.config-1.");
    amxd_trans_set_value(cstring_t, &trans, "IPv6DNSMode", "Any");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "ra-1", "2a02:1802:94:4200::10");
    amxut_bus_handle_events();
}

void test_relay_type_dynamic_with_static_dnsserver(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "dhcpv4-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "8.8.8.8"); // should be the same as returned by netmodel query
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_select_pathf(&trans, ".^");
    amxd_trans_add_inst(&trans, 0, "dhcpv4-2");
    amxd_trans_set_value(cstring_t, &trans, "Type", "DHCPv4");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "dhcpv4-1", "8.8.8.8");
    expect_check_add_server(true, "eth0", "dhcpv4-2", "8.8.4.4");
    amxut_bus_handle_events();
}

void test_relay_type_static_without_dnsserver(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Config.");
    amxd_trans_add_inst(&trans, 0, "config-1");
    amxd_trans_set_value(cstring_t, &trans, "DNSMode", "Static");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "webui-1");
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", ""); // must be empty
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.webui-1");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "8.8.8.8");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "webui-1", "8.8.8.8");
    amxut_bus_handle_events();
}

void test_relay_type_ipv6_lla(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Relay.Forwarding.");
    amxd_trans_add_inst(&trans, 0, "static-ipv6");
    amxd_trans_set_value(cstring_t, &trans, "Type", "Static");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2.");
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", "fe80::ae91:9bff:fe3a:5d0c"); // should be the same as returned by netmodel query
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    expect_check_add_server(true, "eth0", "static-ipv6", "fe80::ae91:9bff:fe3a:5d0c%eth0");
    amxut_bus_handle_events();
}