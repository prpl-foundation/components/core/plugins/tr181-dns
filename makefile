include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C mod-dns-uci/src all
	$(MAKE) -C mod-dns-methods/src all
	$(MAKE) -C odl all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean
	$(MAKE) -C mod-dns-uci/src clean
	$(MAKE) -C mod-dns-methods/src clean
	$(MAKE) -C odl clean

install: all
	$(INSTALL) -D -p -m 0664 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-dns-uci.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-dns-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-dns-methods.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-dns-methods.so
	$(INSTALL) -D -p -m 0644 mod-dns-methods/odl/$(COMPONENT)_methods.odl $(DEST)/etc/amx/$(COMPONENT)/extensions/$(COMPONENT)_methods.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-dns_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_vars.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_vars.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_controllers.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_controllers.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_client.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_client.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_relay.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_relay.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_host.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_host.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_forwardzone.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_forwardzone.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_rebindprotection.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_rebindprotection.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_diagnostics.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_diagnostics.odl
	$(INSTALL) -D -p -m 0644 odl/defaults.d/00_default_ip_interfaces.odl $(DEST)/etc/amx/$(COMPONENT)/defaults.d/00_default_ip_interfaces.odl
	$(INSTALL) -D -p -m 0644 odl/defaults.d/10_tr181-dns_default.odl.uc $(DEST)/etc/amx/$(COMPONENT)/defaults.d/10_tr181-dns_default.odl.uc
ifeq ($(CONFIG_SAH_AMX_TR181_DNS_CELLULAR),y)
	$(INSTALL) -D -p -m 0644 odl/defaults.d/20_tr181-dns_cellular_default.odl.uc $(DEST)/etc/amx/$(COMPONENT)/defaults.d/20_tr181-dns_cellular_default.odl.uc
endif
	$(INSTALL) -D -p -m 0644 odl/defaults.d/20_tr181-dns_hosts_default.odl.uc $(DEST)/etc/amx/$(COMPONENT)/defaults.d/20_tr181-dns_hosts_default.odl.uc
	$(INSTALL) -D -p -m 0644 odl/defaults.d/30-rebind_protection.odl $(DEST)/etc/amx/$(COMPONENT)/defaults.d/30-rebind_protection.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -D -p -m 0644 acl/$(COMPONENT).json $(DEST)/usr/share/acl.d/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -D -p -m 0664 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-dns-uci.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-dns-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-dns-methods.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-dns-methods.so
	$(INSTALL) -D -p -m 0644 mod-dns-methods/odl/$(COMPONENT)_methods.odl $(PKGDIR)/etc/amx/$(COMPONENT)/extensions/$(COMPONENT)_methods.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-dns_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_vars.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_vars.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_controllers.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_controllers.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_client.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_client.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_relay.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_relay.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_host.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_host.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_forwardzone.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_forwardzone.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_rebindprotection.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_rebindprotection.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_diagnostics.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_diagnostics.odl
	$(INSTALL) -D -p -m 0644 odl/defaults.d/00_default_ip_interfaces.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/00_default_ip_interfaces.odl
	$(INSTALL) -D -p -m 0644 odl/defaults.d/10_tr181-dns_default.odl.uc $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/10_tr181-dns_default.odl.uc
ifeq ($(CONFIG_SAH_AMX_TR181_DNS_CELLULAR),y)
	$(INSTALL) -D -p -m 0644 odl/defaults.d/20_tr181-dns_cellular_default.odl.uc $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/20_tr181-dns_cellular_default.odl.uc
endif
	$(INSTALL) -D -p -m 0644 odl/defaults.d/20_tr181-dns_hosts_default.odl.uc $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/20_tr181-dns_hosts_default.odl.uc
	$(INSTALL) -D -p -m 0644 odl/defaults.d/30-rebind_protection.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/30-rebind_protection.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_caps.odl
	$(INSTALL) -D -p -m 0644 acl/$(COMPONENT).json $(PKGDIR)/usr/share/acl.d/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/$(COMPONENT).odl)
	$(eval ODLFILES += odl/$(COMPONENT)_definition.odl)
	$(eval ODLFILES += odl/$(COMPONENT)_vars.odl)
	$(eval ODLFILES += odl/$(COMPONENT)_client.odl)
	$(eval ODLFILES += odl/$(COMPONENT)_relay.odl)
	$(eval ODLFILES += odl/$(COMPONENT)_host.odl)
	$(eval ODLFILES += odl/$(COMPONENT)_forwardzone.odl)
	$(eval ODLFILES += odl/$(COMPONENT)_diagnostics.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C mod-dns-uci/test run
	$(MAKE) -C mod-dns-uci/test coverage
	$(MAKE) -C mod-dns-methods/test run
	$(MAKE) -C mod-dns-methods/test coverage
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test