/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "mock.h"
#include "mock_uci.h"

#include "test_module.h"

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;

static int hosts_reloaded = 0;

static void handler(int sig) {
    assert_int_equal(sig, SIGHUP);
    hosts_reloaded = 1;
}

int test_setup(void** state) {
    FILE* file = NULL;
    dns_unit_test_setup(state);
    dm = amxut_bus_dm();
    parser = amxut_bus_parser();

    // mod-dns-uci will read this file to send SIGHUP to dnsmasq to reload hosts file
    file = fopen("/tmp/dnsmasq.pid", "w");
    assert_non_null(file);
    fprintf(file, "%d\n", getpid());
    fclose(file);

    signal(SIGHUP, handler);

    return 0;
}

int test_teardown(void** state) {
    amxm_close_all();

    dns_unit_test_teardown(state);
    dm = NULL;
    parser = NULL;

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, "dns", MOD_DIR "/mod-dns-uci.so"), 0);
}

void test_add_server(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;
    amxc_var_t* set = new_uci_set("8.8.8.8@eth0");
    amxc_var_t* commit = new_uci_commit();

    amxc_var_init(&args);
    amxc_var_init(&ret);

    rv = amxm_execute_function("dns", "dns", "add-server", &args, &ret);
    assert_int_equal(rv, -1);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "dns-1");
    amxc_var_add_key(cstring_t, &args, "server", "8.8.8.8");
    amxc_var_add_key(cstring_t, &args, "interface", "eth0");

    expect_check(_set, args, uci_set_equal_check, set);
    expect_check(_commit, args, uci_commit_equal_check, commit);
    rv = amxm_execute_function("dns", "dns", "add-server", &args, &ret);
    assert_int_equal(rv, 0);

    amxut_timer_go_to_future_ms(1000);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_delete(&set); // todo remove
    amxc_var_delete(&commit);
}

void test_add_host(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* addresses = NULL;
    int rv = -1;

    hosts_reloaded = 0;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    rv = amxm_execute_function("dns", "dns", "add-host", &args, &ret);
    assert_int_equal(rv, -1);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "example.com");
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");
    addresses = amxc_var_add_key(cstring_t, &args, "ipv4_addresses", "192.168.1.100,192.168.1.101");
    amxc_var_cast(addresses, AMXC_VAR_ID_LIST);
    addresses = amxc_var_add_key(cstring_t, &args, "ipv6_addresses", "::100,::101");
    amxc_var_cast(addresses, AMXC_VAR_ID_LIST);

    rv = amxm_execute_function("dns", "dns", "add-host", &args, &ret);
    assert_int_equal(rv, 0);
    amxut_timer_go_to_future_ms(1000);
    assert_int_equal(hosts_reloaded, 1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_remove_host_address(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* addresses = NULL;
    int rv = -1;

    hosts_reloaded = 0;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    rv = amxm_execute_function("dns", "dns", "remove-host-address", &args, &ret);
    assert_int_equal(rv, -1);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "example.com");
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");
    addresses = amxc_var_add_key(cstring_t, &args, "ipv6_addresses", "::100");
    amxc_var_cast(addresses, AMXC_VAR_ID_LIST);

    rv = amxm_execute_function("dns", "dns", "remove-host-address", &args, &ret);
    assert_int_equal(rv, 0);
    amxut_timer_go_to_future_ms(1000);
    assert_int_equal(hosts_reloaded, 1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_remove_host(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    hosts_reloaded = 0;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "example.com");
    amxc_var_add_key(cstring_t, &args, "interface", "br-lan");

    rv = amxm_execute_function("dns", "dns", "remove-host", &args, &ret);
    assert_int_equal(rv, 0);
    amxut_timer_go_to_future_ms(1000);
    assert_int_equal(hosts_reloaded, 1);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_change_enable(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* set = new_uci_set("8.8.8.8@eth0");
    amxc_var_t* delete = new_uci_delete();
    amxc_var_t* commit = new_uci_commit();
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_BOOL);

    amxc_var_set(bool, &args, false);
    expect_check(_delete, args, uci_delete_equal_check, delete);
    expect_check(_commit, args, uci_commit_equal_check, commit);
    rv = amxm_execute_function("dns", "dns", "enable-changed", &args, &ret);
    assert_int_equal(rv, 0);
    amxut_timer_go_to_future_ms(1000);

    amxc_var_set(bool, &args, true);
    expect_check(_set, args, uci_set_equal_check, set);
    expect_check(_commit, args, uci_commit_equal_check, commit);
    rv = amxm_execute_function("dns", "dns", "enable-changed", &args, &ret);
    assert_int_equal(rv, 0);
    amxut_timer_go_to_future_ms(1000);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_delete(&delete);
    amxc_var_delete(&set);
    amxc_var_delete(&commit);
}

void test_remove_server(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* delete = new_uci_delete();
    amxc_var_t* commit = new_uci_commit();
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    rv = amxm_execute_function("dns", "dns", "remove-server", &args, &ret);
    assert_int_equal(rv, -1);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "dns-1");
    expect_check(_delete, args, uci_delete_equal_check, delete);
    expect_check(_commit, args, uci_commit_equal_check, commit);
    rv = amxm_execute_function("dns", "dns", "remove-server", &args, &ret);
    assert_int_equal(rv, 0);
    amxut_timer_go_to_future_ms(1000);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_delete(&delete);
    amxc_var_delete(&commit);
}

void test_change_enable_without_servers(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_BOOL);

    amxc_var_set(bool, &args, false);
    rv = amxm_execute_function("dns", "dns", "enable-changed", &args, &ret);
    assert_int_equal(rv, 0);

    amxc_var_set(bool, &args, true);
    rv = amxm_execute_function("dns", "dns", "enable-changed", &args, &ret);
    assert_int_equal(rv, 0);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}