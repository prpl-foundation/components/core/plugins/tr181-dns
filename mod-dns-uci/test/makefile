PKG_CONFIG_LIBDIR := /usr/lib/pkgconfig:/lib/pkgconfig:/usr/lib/x86_64-linux-gnu/pkgconfig:$(PKG_CONFIG_LIBDIR)
MACHINE = $(shell $(CC) -dumpmachine)
SUBDIRS := $(wildcard mod-dns-*)
OBJDIR = ../output/$(MACHINE)/mod_dns_uci_coverage
COVERREPORT = mod_dns_uci_report

run: $(OBJDIR)/ | mod-dns-uci
	@for dir in $(SUBDIRS); do make -C $$dir $@  || exit -1; done
	@rm -rf $(OBJDIR)/test* $(OBJDIR)/dummy* $(OBJDIR)/mock* $(OBJDIR)/common*

clean:
	rm -rf $(OBJDIR)
	rm -rf $(OBJDIR)/$(COVERREPORT)
	find .. -name "run_test" -delete
	make -C ../src clean
	rm -f /tmp/dnsmasq.pid
	rm -f /tmp/dnsmasq.hosts*

coverage: $(OBJDIR)/$(COVERREPORT)/
	@cd ../src/ && for i in $$(find . -type f -iname "*.c"); do \
		ln -sf ../../../src/$$i $(OBJDIR)/$$i; \
	done
	@cd $(OBJDIR) && \
	for i in $$(find . -type f -iname "*.o"); do \
		gcov -c -b -f --long-file-names --preserve-paths $$i > /dev/null; \
	done
	@cd $(OBJDIR) && for i in $$(find . -name "*.h.gcov"); do rm $$i > /dev/null; done
	cd $(OBJDIR) && gcovr -k -p -r ../../.. -s -g --html --html-details -o ./$(COVERREPORT)/index.html
	cd $(OBJDIR) && gcovr -k -p -r ../../.. -s -g | tee ./$(COVERREPORT)/gcovr_summary.txt

$(OBJDIR)/:
	mkdir -p $@

$(OBJDIR)/$(COVERREPORT)/:
	mkdir -p $@

mod-dns-uci:
	OBJDIR=$(OBJDIR) CFLAGS="-fprofile-arcs -ftest-coverage -DPIDFILE='\"/tmp/dnsmasq.pid\"' -DHOSTS_FILE='\"/tmp/dnsmasq.hosts\"'" LDFLAGS="-fprofile-arcs -ftest-coverage" make -C ../src all

.PHONY: run clean coverage mod-dns-uci
