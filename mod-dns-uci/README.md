[[_TOC_]]

mod-dns-uci
==============

This is the libamxm module that can be used by the tr181-dns plugin to use dnsmasq as resolver by using UCI as interface.

features
--------

| feature | supported | requires restart of dnsmasq daemon |
| :---    | :---:     | :---:                              |
| add server | :white_check_mark: | :white_check_mark: |
| remove server | :white_check_mark: | :white_check_mark: |
| add host | :white_check_mark: | :x: |
| remove host | :white_check_mark: | :x: |
| add interface | :white_check_mark: | :x: |
| remove interface | :white_check_mark: | :x: |
| report server failure | :x: | |

dependencies
------------

dnsmasq<br>
UCI over ubus

files
-----

>>>
The default openwrt `/etc/init.d/dnsmasq` script must be patched to use a predictable PID file `/var/run/dnsmasq/dnsmasq.pid` to reload the hosts file.
>>>

/tmp/hosts/tr181-dns-hosts<br>
/var/run/dnsmasq/dnsmasq.pid<br>
/etc/init.d/dnsmasq<br>
/var/etc/dnsmasq.conf.*

compile + (unit)test + install
------------------------

```
# compile
make

# test
make test

# install
sudo make install
```

When debugging unit tests it is possible that SIGALARM stops gdb, this can be avoided:
```
# to avoid:
# Program terminated with signal SIGALRM, Alarm clock.
# The program no longer exists.

# do (once at breakpoint):
(gdb) handle SIGALRM ignore

# undo:
(gdb) handle SIGALRM pass
```