/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <amxb/amxb.h>
#include <debug/sahtrace_macros.h>

#include "dns_uci.h"

#define ME "dns"
#ifndef HOSTS_FILE
#define HOSTS_FILE "/tmp/hosts/tr181-dns-hosts"
#endif
#ifndef PIDFILE
#define PIDFILE "/var/run/dnsmasq/dnsmasq.pid"
#endif

#define STRING_EMPTY(X) ((X == NULL) || (*X == '\0'))
#define FREE(X) do { \
        free(X); \
        X = NULL; \
} while(0);

static amxb_bus_ctx_t* _bus = NULL;
static amxc_var_t current_config;
static amxp_timer_t* delay_timer = NULL;
static amxp_timer_t* host_timer = NULL;
static bool relay_is_enabled = true;
static const char* list[] = {"ipv4_addresses", "ipv6_addresses", NULL};

static amxb_bus_ctx_t* get_bus(void) {
    if(_bus == NULL) {
        _bus = amxb_be_who_has("uci.");
    }
    return _bus;
}

static void update_status(const char* status) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* subvar = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "server", "Enabled");

    subvar = amxc_var_add_new_key(&args, "servers");
    amxc_var_set_type(subvar, AMXC_VAR_ID_HTABLE);
    amxc_var_for_each(var, GET_ARG(&current_config, "servers")) {
        amxc_var_add_key(cstring_t, subvar, GET_CHAR(var, "name"), status);
    }

    SAH_TRACEZ_INFO(ME, "mod-uci executes core's reconfigure-result");
    amxm_execute_function(NULL, "core", "reconfigure-result", &args, &ret);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}

static bool uci_commit(void) {
    amxb_bus_ctx_t* bus = get_bus();
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null_trace(bus, exit, ERROR, "Bus not found that serves 'uci.'");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "config", "dhcp");

    rv = amxb_call(bus, "uci.", "commit", &args, &ret, 5);
    if(rv != AMXB_STATUS_OK) {
        SAH_TRACEZ_ERROR(ME, "Could not call 'uci commit', reason %d", rv);
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "UCI commit");

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv == AMXB_STATUS_OK;
}

static bool ubus_call_uci(const char* method, amxc_var_t* args) {
    amxb_bus_ctx_t* bus = get_bus();
    amxc_var_t ret;
    int rv = AMXB_ERROR_UNKNOWN;

    amxc_var_init(&ret);

    when_null_trace(bus, exit, ERROR, "Bus not found that serves 'uci.'");

    rv = amxb_call(bus, "uci.", method, args, &ret, 5);
    if(rv != AMXB_STATUS_OK) {
        SAH_TRACEZ_ERROR(ME, "Failed to call uci[%s], dnsmasq might not be reconfigured, reason %d", method, rv);
        goto exit;
    }

exit:
    amxc_var_clean(&ret);
    return rv == AMXB_STATUS_OK;
}

static bool dnsmasq_args_init(amxc_var_t* args) {
    amxc_var_init(args);

    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, "config", "dhcp");
    amxc_var_add_key(cstring_t, args, "type", "dnsmasq");

    return true;
}

static bool set_option_server(amxc_var_t* servers) {
    amxc_var_t args;
    amxc_var_t* var = NULL;
    amxc_var_t* subvar = NULL;
    bool rv = false;

    dnsmasq_args_init(&args);
    var = amxc_var_add_key(amxc_htable_t, &args, "values", NULL);
    subvar = amxc_var_add_new_key(var, "server");
    amxc_var_move(subvar, servers);
    amxc_var_add_key(bool, var, "noresolv", true);
    amxc_var_add_key(uint32_t, var, "port", 53);
    amxc_var_add_key(bool, var, "localise_queries", true); // for hosts

    when_false(ubus_call_uci("set", &args), exit);

    rv = true;

exit:
    amxc_var_clean(&args);
    return rv;
}

static bool delete_option_server(void) {
    amxc_var_t args;
    bool rv = false;

    dnsmasq_args_init(&args);
    amxc_var_add_key(cstring_t, &args, "option", "server");

    rv = ubus_call_uci("delete", &args);

    amxc_var_clean(&args);
    return rv;
}

static bool in_list(amxc_var_t* servers, amxc_string_t* a_str) {
    const char* a = amxc_string_get(a_str, 0);
    bool rv = false;

    when_null(a, exit);

    amxc_var_for_each(var, servers) {
        const char* b = GET_CHAR(var, NULL);

        if((b != NULL) && (strcmp(a, b) == 0)) {
            rv = true;
            goto exit;
        }
    }

exit:
    return rv;
}

static const char* get_date(char* date, size_t date_size) {
    time_t t = {0};
    struct tm* tm = NULL;
    t = time(NULL);
    tm = localtime(&t);
    when_null_trace(tm, exit, ERROR, "localtime failure");
    strftime(date, date_size, "%F %T", tm);
exit:
    return date;
}

static void create_hosts_file(void) {
    FILE* file = NULL;
    char date[64] = "";

    file = fopen(HOSTS_FILE ".new", "w");
    when_null_trace(file, exit, ERROR, "Could not open %s.new for writing", HOSTS_FILE);

    fprintf(file, "# dnsmasq hosts file, generated by tr181-dns on %s.\n", get_date(date, sizeof(date)));

    amxc_var_for_each(interface, GET_ARG(&current_config, "interfaces")) {
        amxc_var_for_each(host, GET_ARG(interface, "hosts")) {
            const char* name = amxc_var_key(host);
            amxc_var_for_each(ipaddress, GET_ARG(host, "ipv4_addresses")) {
                fprintf(file, "%s\t%s\n", GET_CHAR(ipaddress, NULL), name);
            }
            amxc_var_for_each(ipaddress, GET_ARG(host, "ipv6_addresses")) {
                fprintf(file, "%s\t%s\n", GET_CHAR(ipaddress, NULL), name);
            }
        }
    }

    fclose(file);
    rename(HOSTS_FILE ".new", HOSTS_FILE);

exit:
    return;
}

static bool uci_set_dnsmasq_options(void) {
    amxc_var_t* forwardings = GET_ARG(&current_config, "servers");
    amxc_var_t* servers = NULL;

    amxc_var_new(&servers);
    amxc_var_set_type(servers, AMXC_VAR_ID_LIST);

    create_hosts_file();

    amxc_var_for_each(forwarding, forwardings) {
        amxc_string_t server_str;
        const char* server = GET_CHAR(forwarding, "server");
        const char* interface = GET_CHAR(forwarding, "interface");

        if(STRING_EMPTY(server)) {
            continue;
        }

        amxc_string_init(&server_str, 0);

        if(STRING_EMPTY(interface)) {
            amxc_string_set(&server_str, server);
        } else {
            amxc_string_setf(&server_str, "%s@%s", server, interface);
        }

        if(!in_list(servers, &server_str)) {
            amxc_var_t* var = NULL;

            SAH_TRACEZ_INFO(ME, "From %s use '%s'", GET_CHAR(forwarding, "name"), amxc_string_get(&server_str, 0));

            var = amxc_var_add_new(servers);
            amxc_var_push(cstring_t, var, amxc_string_take_buffer(&server_str));
        }

        amxc_string_clean(&server_str);
    }

    if(amxc_var_get_first(servers) != NULL) {
        set_option_server(servers);
    } else {
        delete_option_server(); // ubus uci doesn't like to set an empty list for option server
    }

    amxc_var_delete(&servers);
    return true;
}

static inline bool uci_clear_dnsmasq_options(void) {
    return delete_option_server();
}

static void configure_dnsmasq(UNUSED amxp_timer_t* const timer, UNUSED void* data) {
    const char* status = "Error";

    if(relay_is_enabled) {
        SAH_TRACEZ_INFO(ME, "Set dnsmasq options");
        when_false(uci_set_dnsmasq_options(), exit);
    } else {
        SAH_TRACEZ_INFO(ME, "Delete dnsmasq options");
        when_false(uci_clear_dnsmasq_options(), exit);
    }
    when_false(uci_commit(), exit); // procd will be triggered to restart dnsmasq (see /etc/init.d/dnsmasq)

    status = relay_is_enabled ? "Enabled" : "Disabled";
exit:
    update_status(status);
}

static inline void configure_dnsmasq_delayed(void) {
    amxp_timer_start(delay_timer, 100);
}

static void configure_hosts(UNUSED amxp_timer_t* const timer, UNUSED void* data) {
    create_hosts_file();

    if(relay_is_enabled) {
        FILE* file = NULL;
        int pid = 0;
        int matches = 0;

        file = fopen(PIDFILE, "r");
        when_null_trace(file, exit, ERROR, "Failed to open file " PIDFILE);
        matches = fscanf(file, "%d\n", &pid);
        fclose(file);
        file = NULL;
        when_false_trace(matches > 0, exit, ERROR, "Failed to read " PIDFILE);
        SAH_TRACEZ_INFO(ME, "send SIGHUP to pid %d", pid);
        kill(pid, SIGHUP);
    }
exit:
    return;
}

static inline void configure_hosts_delayed(void) {
    amxp_timer_start(host_timer, 100);
}

static int add_server(UNUSED const char* function_name,
                      amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    amxc_var_t* var = GETP_ARG(&current_config, "servers");
    amxc_var_t* subvar = NULL;
    const char* name = GET_CHAR(args, "name");
    const char* server = GET_CHAR(args, "server");
    const char* interface = GET_CHAR(args, "interface");
    int rv = -1;

    when_null_trace(var, exit, INFO, "No servers found");
    when_str_empty_trace(name, exit, ERROR, "Missing name");
    when_str_empty_trace(server, exit, ERROR, "Missing server");
    when_str_empty_trace(interface, exit, ERROR, "Missing interface");

    subvar = amxc_var_add_key(amxc_htable_t, var, name, NULL);
    amxc_var_move(subvar, args);

    SAH_TRACEZ_INFO(ME, "Server '%s' added", server);

    if(relay_is_enabled) {
        configure_dnsmasq_delayed(); // use timer to keep the number of dnsmasq restarts to minimum
    }

    rv = 0;
exit:
    return rv;
}

static int remove_server(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    amxc_var_t* var = GETP_ARG(&current_config, "servers");
    amxc_var_t* subvar = NULL;
    const char* name = GET_CHAR(args, "name");
    int rv = -1;

    when_null_trace(var, exit, INFO, "No servers found");

    when_str_empty_trace(name, exit, ERROR, "Missing name");

    subvar = amxc_var_get_key(var, name, AMXC_VAR_FLAG_DEFAULT);

    SAH_TRACEZ_INFO(ME, "Server '%s' removed", GET_CHAR(subvar, "server"));

    amxc_var_delete(&subvar);

    if(relay_is_enabled) {
        configure_dnsmasq_delayed(); // use timer to keep the number of dnsmasq restarts to minimum
    }

    rv = 0;
exit:
    return rv;
}

static int not_implemented(UNUSED const char* function_name,
                           UNUSED amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_INFO(ME, "Not implemented");
    return 0;
}

static int enable_changed(UNUSED const char* function_name,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    bool has_enabled_forwardings = amxc_var_get_first(GET_ARG(&current_config, "servers")) != NULL;

    relay_is_enabled = GET_BOOL(args, NULL);

    SAH_TRACEZ_INFO(ME, "Enable changed to %s", relay_is_enabled ? "true" : "false");

    when_false(has_enabled_forwardings, exit);

    configure_dnsmasq_delayed(); // use timer to keep the number of dnsmasq restarts to minimum

exit:
    return 0;
}

static int add_host(UNUSED const char* function_name,
                    amxc_var_t* args,
                    UNUSED amxc_var_t* ret) {
    amxc_var_t* interfaces = GET_ARG(&current_config, "interfaces");
    amxc_var_t* interface_var = NULL;
    amxc_var_t* hosts = NULL;
    amxc_var_t* host = NULL;
    const char* name = GET_CHAR(args, "name");
    const char* interface = GET_CHAR(args, "interface");
    int count = 0;
    int rv = -1;

    when_null_trace(interfaces, exit, ERROR, "Something bad happened");
    when_str_empty_trace(name, exit, INFO, "Missing name");
    when_str_empty_trace(interface, exit, INFO, "Missing interface");

    interface_var = amxc_var_get_key(interfaces, interface, AMXC_VAR_FLAG_DEFAULT);
    if(interface_var == NULL) {
        interface_var = amxc_var_add_key(amxc_htable_t, interfaces, interface, NULL);
        amxc_var_add_key(amxc_htable_t, interface_var, "hosts", NULL);
    }
    hosts = GET_ARG(interface_var, "hosts");
    when_null_trace(hosts, exit, ERROR, "Something bad happened");

    host = amxc_var_get_key(hosts, name, AMXC_VAR_FLAG_DEFAULT);
    if(host == NULL) {
        SAH_TRACEZ_INFO(ME, "Add new host '%s'", name);
        host = amxc_var_add_key(amxc_htable_t, hosts, name, NULL);
        for(int i = 0; list[i] != NULL; i++) {
            amxc_var_add_key(amxc_llist_t, host, list[i], NULL);
        }
    }

    for(int i = 0; list[i] != NULL; i++) {
        amxc_var_t* ip_list = GET_ARG(host, list[i]);
        amxc_var_for_each(var, GET_ARG(args, list[i])) {
            const char* ip_address = GET_CHAR(var, NULL);
            SAH_TRACEZ_INFO(ME, "Add new address '%s' to %s", ip_address, list[i]);
            amxc_var_add(cstring_t, ip_list, ip_address);
            count++;
        }
    }

    when_false_trace(count > 0, exit, ERROR, "Nothing added");

    configure_hosts_delayed();

    rv = 0;
exit:
    return rv;
}

static int remove_host(UNUSED const char* function_name,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    amxc_var_t* interfaces = GETP_ARG(&current_config, "interfaces");
    amxc_var_t* interface_var = NULL;
    amxc_var_t* host = NULL;
    amxc_var_t* hosts = NULL;
    const char* interface = GET_CHAR(args, "interface");
    const char* name = GET_CHAR(args, "name");
    int rv = -1;

    when_null_trace(interfaces, exit, ERROR, "Something bad happened");
    when_str_empty_trace(name, exit, INFO, "Missing name");
    when_str_empty_trace(interface, exit, INFO, "Missing interface");

    interface_var = amxc_var_get_key(interfaces, interface, AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(interface_var, exit, ERROR, "Interface '%s' not known, can't remove host '%s'", interface, name);
    hosts = GET_ARG(interface_var, "hosts");
    when_null_trace(hosts, exit, ERROR, "Something bad happened");
    host = amxc_var_get_key(hosts, name, AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(host, exit, ERROR, "Interface '%s' has no host '%s'", interface, name);
    SAH_TRACEZ_INFO(ME, "Remove host '%s'", name);
    amxc_var_delete(&host);

    configure_hosts_delayed();

    rv = 0;
exit:
    return rv;
}

static int remove_host_address(UNUSED const char* function_name,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    amxc_var_t* interfaces = GETP_ARG(&current_config, "interfaces");
    amxc_var_t* interface_var = NULL;
    amxc_var_t* host = NULL;
    amxc_var_t* hosts = NULL;
    const char* name = GET_CHAR(args, "name");
    const char* interface = GET_CHAR(args, "interface");
    uint32_t count = 0;
    int rv = -1;

    when_null_trace(interfaces, exit, ERROR, "Something bad happened");
    when_str_empty_trace(name, exit, INFO, "Missing name");
    when_str_empty_trace(interface, exit, INFO, "Missing interface");

    interface_var = amxc_var_get_key(interfaces, interface, AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(interface_var, exit, ERROR, "Interface '%s' not known, can't remove host '%s'", interface, name);
    hosts = GET_ARG(interface_var, "hosts");
    when_null_trace(hosts, exit, ERROR, "Something bad happened");
    host = amxc_var_get_key(hosts, name, AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(host, exit, ERROR, "Interface '%s' has no host '%s'", interface, name);

    for(int i = 0; list[i] != NULL; i++) {
        amxc_var_for_each(var_a, GET_ARG(args, list[i])) {
            const char* ip_address = GET_CHAR(var_a, NULL);

            amxc_var_for_each(var_b, GET_ARG(host, list[i])) {
                if(strcmp(GET_CHAR(var_b, NULL), ip_address) == 0) {
                    SAH_TRACEZ_INFO(ME, "Remove ip address '%s' from %s", ip_address, list[i]);
                    amxc_var_delete(&var_b);
                    count++;
                    break;
                }
            }
        }
    }

    when_false_trace(count > 0, exit, ERROR, "Nothing removed");

    configure_hosts_delayed();

    rv = 0;
exit:
    return rv;
}

typedef struct {
    const char* name;
    amxm_callback_t callback;
} function_t;

static AMXM_CONSTRUCTOR module_init(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;
    function_t function[] = {
        {"set-server", not_implemented},
        {"add-server", add_server},
        {"remove-server", remove_server},
        {"add-host", add_host},
        {"remove-host", remove_host},
        {"remove-host-address", remove_host_address},
        {"set-interface", not_implemented},
        {"remove-interface", not_implemented},
        {"enable-changed", enable_changed},
        {"debug", not_implemented},
        {"set-options", not_implemented},
        {NULL, 0}
    };
    int rv = -1;

    when_null_status(so, exit, rv = -1);

    amxc_var_init(&current_config);
    amxc_var_set_type(&current_config, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(amxc_htable_t, &current_config, "servers", NULL);
    amxc_var_add_key(amxc_htable_t, &current_config, "interfaces", NULL);

    rv = amxp_timer_new(&delay_timer, configure_dnsmasq, NULL);
    when_false(0 == rv, exit);

    rv = amxp_timer_new(&host_timer, configure_hosts, NULL);
    when_false(0 == rv, exit);

    rv = amxm_module_register(&mod, so, "dns");
    when_false(0 == rv, exit);

    for(size_t i = 0; function[i].name != NULL; i++) {
        if(0 != amxm_module_add_function(mod, function[i].name, function[i].callback)) {
            SAH_TRACEZ_WARNING(ME, "Failed to register function %s", function[i].name);
        }
    }

exit:
    return rv == 0 ? 0 : -1;
}

static AMXM_DESTRUCTOR module_exit(void) {
    amxp_timer_delete(&delay_timer);
    amxp_timer_delete(&host_timer);
    amxc_var_clean(&current_config);
    return 0;
}
