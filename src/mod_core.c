/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tr181-dns.h"
#include "dm_relay.h"

#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_expression.h>
#include <amxm/amxm.h>
#include <debug/sahtrace_macros.h>

#define ME "relay"
#define DEPRECATED_WARNING SAH_TRACEZ_WARNING(ME, "use of deprecated function")

static void list_interfaces_to_flag(amxd_object_t* parent, amxc_var_t* forwarders, amxc_var_t* pending_set, amxc_var_t* pending_clear) {
    amxd_object_for_each(instance, it, amxd_object_findf(parent, "%s", "Forwarding")) {
        amxd_object_t* obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        const char* iface = NULL;
        bool do_set = false;

        do_set = (GET_ARG(forwarders, amxd_object_get_name(obj, AMXD_OBJECT_NAMED)) != NULL); // ok if forwarders is NULL, should clear flag in this case

        iface = object_da_string(obj, "Interface");
        if(STRING_EMPTY(iface)) {
            continue;
        }

        if(do_set) {
            amxc_var_t* to_delete = GET_ARG(pending_clear, iface);
            amxc_var_delete(&to_delete);

            if(GET_ARG(pending_set, iface) == NULL) {
                amxc_var_add_key(cstring_t, pending_set, iface, "");
            } // else the flag is already going to be set
        } else {
            if((GET_ARG(pending_set, iface) == NULL) &&
               (GET_ARG(pending_clear, iface) == NULL)) {
                amxc_var_add_key(cstring_t, pending_clear, iface, "");
            } // else the flag is going to be set or cleared
        }
    }
}

int update_forwarding_status(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    amxc_var_t ifaces;
    amxc_var_t* pending_set = NULL;
    amxc_var_t* pending_clear = NULL;
    const char* flag = "dns-up";
    const char* condition = "ip";

    amxc_var_init(&ifaces);
    amxc_var_set_type(&ifaces, AMXC_VAR_ID_HTABLE);
    pending_set = amxc_var_add_key(amxc_htable_t, &ifaces, "set", NULL);
    pending_clear = amxc_var_add_key(amxc_htable_t, &ifaces, "clear", NULL);

    list_interfaces_to_flag(amxd_dm_findf(get_dm(), "DNS.Relay"), GET_ARG(args, "."), pending_set, pending_clear);

    amxd_object_for_each(instance, it, amxd_dm_findf(get_dm(), "DNS.%sForwardZone", get_prefix())) {
        amxd_object_t* obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        const char* zone_name = NULL;
        zone_name = object_da_string(obj, "Name"); // zone_name is allowed to be empty, flags should be cleared
        list_interfaces_to_flag(obj, GET_ARG(args, zone_name), pending_set, pending_clear);
    }

    amxc_var_for_each(iface, pending_clear) {
        SAH_TRACEZ_INFO(ME, "clearFlag(%s, %s)", amxc_var_key(iface), flag);
        netmodel_clearFlag(amxc_var_key(iface), flag, condition, netmodel_traverse_down);
    }

    amxc_var_for_each(iface, pending_set) {
        SAH_TRACEZ_INFO(ME, "setFlag(%s, %s)", amxc_var_key(iface), flag);
        netmodel_setFlag(amxc_var_key(iface), flag, condition, netmodel_traverse_down);
    }

    amxc_var_clean(&ifaces);

    return 0;
}

// can be called by dns controller
int server_failure(UNUSED const char* function_name,
                   amxc_var_t* args,
                   amxc_var_t* ret) {
    const char* so_name = NULL;
    when_false(supports_failover(), exit); // todo if failover is not active we could unsubscribe on these events

    so_name = failover_shared_object_name();

    SAH_TRACEZ_INFO(ME, "Execute failover's server-failure");
    amxm_execute_function(so_name, "failover", "server-failure", args, ret);

exit:
    return 0;
}

// DEPRECATED[reason = State will be removed] can be called by failover controller
int update_forwarding_state(UNUSED const char* function_name,
                            UNUSED amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    DEPRECATED_WARNING;
    return 0;
}

// can be called by failover controller (for DNS.Relay.Forwarding)
int core_set_server(UNUSED const char* function_name,
                    amxc_var_t* args,
                    amxc_var_t* ret) {
    int count = 0;
    int removed = 0;
    int rv = -1;

    // failover is not aware what info the dns module needs
    amxc_var_for_each(server, GET_ARG(args, "servers")) {
        count++;
        amxc_var_add_key(cstring_t, server, "name", amxc_var_key(server));
        if(!private_server_info(server)) {
            // maybe netdevname is missing
            SAH_TRACEZ_INFO(ME, "remove '%s' from set-server args", GET_CHAR(server, "name"));
            amxc_var_delete(&server);
            removed++;
        }
    }

    when_true_trace((count != 0) && (count == removed), exit, INFO, "none of the servers are good");

    rv = set_server(args, ret, false);
exit:
    return rv;
}

// DEPRECATED[reason = mod-failover should use set_server] can be called by failover controller (for DNS.Relay.Forwarding)
int core_add_server(UNUSED const char* function_name, // todo same as set_server, use private_server_info instead of depending on failover module for data
                    amxc_var_t* args,
                    amxc_var_t* ret) {
    DEPRECATED_WARNING;
    return add_server(args, ret, false);
}

// DEPRECATED[reason = mod-failover should use set_server] can be called by failover controller (for DNS.Relay.Forwarding)
int core_remove_server(UNUSED const char* function_name,
                       amxc_var_t* args,
                       amxc_var_t* ret) {
    DEPRECATED_WARNING;
    return remove_server(args, ret, false);
}

// can be called by failover controller (for DNS.Relay.Forwarding)
int core_failover_enable_changed(UNUSED const char* function_name,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    bool enable = GET_BOOL(args, NULL);
    SAH_TRACEZ_INFO(ME, "failover-enable-changed %d", enable);

    set_failover_active(enable);
    if(!enable) {
        relay_enable_all();
    }
    return 0;
}

int set_options(const char* soname, const char* module, amxc_var_t* options) {
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_copy(&args, options);

    rv = amxm_execute_function(soname, module, "set-options", &args, &ret);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv == 0;
}