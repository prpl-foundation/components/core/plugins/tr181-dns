/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tr181-dns.h"
#include "dm_relay.h"

#include <debug/sahtrace_macros.h>
#include <amxm/amxm.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_expression.h>

#define ME "relay"

bool enable_changed(bool is_enabled) {
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set(bool, &args, is_enabled);

    rv = amxm_execute_function("dns", "dns", "enable-changed", &args, &ret);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv == 0;
}

static inline const char* select_mod(bool use_failover) {
    return (use_failover && supports_failover()) ? "failover" : "dns";
}

static inline const char* select_soname(bool use_failover) {
    return (use_failover && supports_failover()) ? failover_shared_object_name() : "dns";
}

/**
 * UNUSED(use_failover) -> to be backward compatible, the 'common' relay code uses add_server/remove_server
 * In other words set_server is only called by the failover module.
 */
int set_server(amxc_var_t* args, amxc_var_t* ret, UNUSED bool use_failover) {
    return amxm_execute_function("dns", "dns", "set-server", args, ret);
}

int add_server(amxc_var_t* args, amxc_var_t* ret, bool use_failover) {
    return amxm_execute_function(select_soname(use_failover), select_mod(use_failover), "add-server", args, ret);
}

int remove_server(amxc_var_t* args, amxc_var_t* ret, bool use_failover) {
    return amxm_execute_function(select_soname(use_failover), select_mod(use_failover), "remove-server", args, ret);
}

int add_host(amxc_var_t* args, amxc_var_t* ret) {
    return amxm_execute_function("dns", "dns", "add-host", args, ret);
}

int remove_host(amxc_var_t* args, amxc_var_t* ret) {
    return amxm_execute_function("dns", "dns", "remove-host", args, ret);
}

int remove_host_address(amxc_var_t* args, amxc_var_t* ret) {
    return amxm_execute_function("dns", "dns", "remove-host-address", args, ret);
}

void add_lan_interface(const char* interface) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty(interface, exit);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "interface", interface);

    rv = amxm_execute_function("dns", "dns", "set-interface", &args, &ret);
    when_false_trace(rv == 0, exit, ERROR, "Failed to execute 'set-interface'");

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void remove_lan_interface(const char* interface) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_str_empty(interface, exit);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "interface", interface);

    rv = amxm_execute_function("dns", "dns", "remove-interface", &args, &ret);
    when_false_trace(rv == 0, exit, ERROR, "Failed to execute 'remove-interface'");

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void set_cache(uint32_t cache_size, uint32_t ttl_max, uint32_t ttl_min) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(uint32_t, &args, "cache_size", cache_size);
    amxc_var_add_key(uint32_t, &args, "ttl_max", ttl_max);
    amxc_var_add_key(uint32_t, &args, "ttl_min", ttl_min);

    rv = amxm_execute_function("dns", "dns", "set-cache", &args, &ret);
    when_false_trace(rv == 0, exit, ERROR, "Failed to execute 'set-cache'");

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void set_domain_name(const char* interface, const char* domain_name) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "interface", interface);
    amxc_var_add_key(cstring_t, &args, "domain_name", domain_name);

    rv = amxm_execute_function("dns", "dns", "set-domain-name", &args, &ret);
    when_false_trace(rv == 0, exit, ERROR, "Failed to execute 'set-domain-name'");

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

bool flush_cache(void) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    rv = amxm_execute_function("dns", "dns", "flush-cache", &args, &ret);
    when_false_trace(rv == 0, exit, ERROR, "Failed to execute 'flush-cache'");

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv == 0;
}