/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "tr181-dns.h"
#include "dm_common.h"
#include "dm_validators.h"

#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_action.h>
#include <amxm/amxm.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "dns"

#ifdef __GNUC__
#define IN6_ADDR_AS_UINT32(a) (((struct in6_addr*) a)->s6_addr32)
#else
#define IN6_ADDR_AS_UINT32(a) ((const uint32_t*) (a))
#endif
#define IN6_IS_ADDR_UNIQUELOCAL(a) ((IN6_ADDR_AS_UINT32(a)[0] & htonl(0xfe000000)) == htonl(0xfc000000))

static const char* IPv4_regexp =
    "^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/([1-2]?[0-9]|3[0-2])$";

static const char* IPv6_regexp =
    "^("
    "([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|"
    "([0-9a-fA-F]{1,4}:){1,7}:|"
    "([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|"
    "([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|"
    "([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|"
    "([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|"
    "[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|"
    ":((:[0-9a-fA-F]{1,4}){1,7}|:)|"
    "fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|"
    "::(ffff(:0{1,4}){0,1}:){0,1}"
    "((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}"
    "(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|"
    "([0-9a-fA-F]{1,4}:){1,4}:"
    "((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}"
    "(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])"
    ")/(12[0-8]|1[01][0-9]|[0-9][0-9]?)$";

static bool str_matches_regex(const cstring_t str, const cstring_t rgx) {
    bool rv = false;
    regex_t regexp;

    when_failed_trace(regcomp(&regexp, rgx, REG_EXTENDED | REG_NOSUB), exit, ERROR, "Failed to compile regex");

    rv = regexec(&regexp, str, 0, NULL, 0) == 0;
    regfree(&regexp);
exit:
    return rv;
}

amxd_status_t _is_valid_ip_range(amxd_object_t* object,
                                 amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    char* value_str = NULL;

    when_null(param, exit);
    when_true_status(reason != action_param_validate, exit,
                     status = amxd_status_function_not_implemented);
    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    status = amxd_status_invalid_value;

    value_str = amxc_var_dyncast(cstring_t, args);
    when_str_empty_status(value_str, exit, status = amxd_status_ok);

    when_true_status(str_matches_regex(value_str, IPv4_regexp), exit, status = amxd_status_ok);
    when_true_status(str_matches_regex(value_str, IPv6_regexp), exit, status = amxd_status_ok);
exit:
    free(value_str);
    return status;
}

static bool parse_ip_range(cstring_t address, bool* v6, int* range) {
    bool rv = false;
    cstring_t range_token = strpbrk(address, "/");
    when_null(range_token, exit);
    *range_token = '\0';
    range_token++;

    *v6 = strpbrk(address, ":") != NULL;
    *range = atoi(range_token);
    rv = *range > 0 && *range <= (*v6 ? 128 : 32);
exit:
    return rv;
}

static bool is_private_ipv4_range(const cstring_t address, int range) {
    bool rv = false;
    struct in_addr in;
    when_false(inet_aton(address, &in) != 0, exit);
    uint32_t addr = ntohl(in.s_addr);

    if((addr & 0xFF000000) == 0x7F000000) {        // 127.0.0.0/8 (loopback)
        rv = range >= 8;
    } else if((addr & 0xFF000000) == 0x0A000000) { // 10.0.0.0/8 (site-local)
        rv = range >= 8;
    } else if((addr & 0xFFF00000) == 0xAC100000) { // 172.16.0.0/12 (site-local)
        rv = range >= 12;
    } else if((addr & 0xFFFF0000) == 0xC0A80000) { // 192.168.0.0/16 (site-local)
        rv = range >= 16;
    } else if((addr & 0xFFFF0000) == 0xA9FE0000) { // 169.254.0.0/16 (link-local)
        rv = range >= 16;
    }
exit:
    return rv;
}

static bool is_private_ipv6_range(UNUSED const cstring_t address, UNUSED int range) {
    bool rv = false;
    struct in6_addr in;
    when_false(inet_pton(AF_INET6, address, &in) == 1, exit);

    if(IN6_IS_ADDR_LOOPBACK(&in)) {           // ::1/128 (loopback)
        rv = range == 128;
    } else if(IN6_IS_ADDR_UNIQUELOCAL(&in)) { // fc00::/7 (unique-local)
        rv = range >= 7;
    } else if(IN6_IS_ADDR_SITELOCAL(&in)) {   // fec0::/10 (site-local) deprecated => replaced by "unique local"
        rv = range >= 10;
    } else if(IN6_IS_ADDR_LINKLOCAL(&in)) {   // fe80::/10 (link-local)
        rv = range >= 10;
    } else if(IN6_IS_ADDR_V4MAPPED(&in)) {    // ::ffff:0:0/96 (IPv4 mapped on IPv6)
        rv = range >= 96;
    }
exit:
    return rv;
}

amxd_status_t _is_private_ip_range(amxd_object_t* object,
                                   amxd_param_t* param,
                                   amxd_action_t reason,
                                   const amxc_var_t* const args,
                                   amxc_var_t* const retval,
                                   void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    char* value_str = NULL;
    int range = 0;
    bool v6 = false;

    when_null(param, exit);
    when_true_status(reason != action_param_validate, exit,
                     status = amxd_status_function_not_implemented);
    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    status = amxd_status_invalid_value;

    value_str = amxc_var_dyncast(cstring_t, args);
    when_str_empty_status(value_str, exit, status = amxd_status_ok);

    when_false(parse_ip_range(value_str, &v6, &range), exit);
    if(v6) {
        when_false(is_private_ipv6_range(value_str, range), exit);
    } else {
        when_false(is_private_ipv4_range(value_str, range), exit);
    }

    status = amxd_status_ok;
exit:
    free(value_str);
    return status;
}
