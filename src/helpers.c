/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tr181-dns.h"

#include <amxd/amxd_transaction.h>
#include <debug/sahtrace_macros.h>

#define ME "dns"

inline const char* object_da_string(amxd_object_t* object, const char* name) {
    return GET_CHAR(amxd_object_get_param_value(object, name), NULL);
}

static const char* to_string(status_t statust) {
    const char* status = NULL;

    switch(statust) {
    case ENABLED:
        status = "Enabled";
        break;
    case DISABLED:
        status = "Disabled";
        break;
    case ERROR:
        status = "Error";
        break;
    }

    return status;
}

void set_status(amxd_object_t* obj, status_t status) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_value(cstring_t, &trans, "Status", to_string(status));

    when_failed_trace(amxd_trans_apply(&trans, get_dm()), exit, INFO, "Transaction failed");
exit:
    amxd_trans_clean(&trans);
}

bool unset_param_persistency(amxd_param_t* param) {
    bool rv = false;

    when_null(param, exit);

    amxd_param_set_attr(param, amxd_pattr_persistent, false);
    amxd_param_unset_flag(param, "upc");
    amxd_param_unset_flag(param, "usersetting");

    rv = true;
exit:
    return rv;
}

bool unset_object_persistency(amxd_object_t* obj) {
    amxd_object_set_attr(obj, amxd_oattr_persistent, false);

    amxd_object_for_each(parameter, it, obj) {
        amxd_param_t* param = amxc_llist_it_get_data(it, amxd_param_t, it);
        unset_param_persistency(param);
    }

    return true;
}

int strcmp_safe(const char* a, const char* b) {
    if(a == NULL) {
        a = "";
    }
    if(b == NULL) {
        b = "";
    }

    return strcmp(a, b);
}