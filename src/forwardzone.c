/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include "dm_common.h"
#include "dm_fwzone.h"
#include <amxd/amxd_transaction.h>
#include <amxc/amxc_llist.h>
#include <debug/sahtrace_macros.h>

#define ME "fwzone"

static int fwzone_add_servers(const amxc_var_t* server_list, const char* zone_name, const char* netdevname) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = 1;

    amxc_var_init(&ret);
    amxc_var_init(&args);

    when_null(server_list, exit);
    when_str_empty(zone_name, exit);

    amxc_var_for_each(server_ip, server_list) {
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "server", GET_CHAR(server_ip, NULL));
        amxc_var_add_key(cstring_t, &args, "interface", netdevname);
        amxc_var_add_key(bool, &args, "enable", true);
        amxc_var_add_key(cstring_t, &args, "zone_name", zone_name);
        amxc_var_add_key(cstring_t, &args, "name", GET_CHAR(server_ip, NULL));
        rv &= add_server(&args, &ret, false);
        amxc_var_clean(&args);
    }
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv;
}

bool fwzone_enable(common_t* core) {
    amxc_var_t servers;
    const char* netdevname = NULL;
    const char* iface = NULL;
    const char* zone_name = NULL;
    amxd_object_t* parent_fwzone = NULL;
    bool rv = false;

    amxc_var_init(&servers);

    parent_fwzone = amxd_object_findf(core->obj, ".^.^");

    zone_name = object_da_string(parent_fwzone, "Name");
    when_str_empty_trace(zone_name, exit, ERROR, "Name is empty");

    iface = object_da_string(core->obj, "Interface");
    if(!STRING_EMPTY(iface)) {
        netdevname = core->netdevname;
        when_str_empty_trace(netdevname, exit, ERROR, "Wait for netdevname");
    }

    SAH_TRACEZ_INFO(ME, "Enabling forwarding [%s]", core->name);

    amxd_object_get_param(core->obj, "DNSServer", &servers);
    amxc_var_cast(&servers, AMXC_VAR_ID_LIST);

    if(fwzone_add_servers(&servers, zone_name, netdevname) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add any server from '%s'", core->name);
        goto exit;
    }

    rv = true;

exit:
    amxc_var_clean(&servers);
    return rv;
}

static void fwzone_disable_forwarding(const char* zone_name, const char* dnsservers) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t servers;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&servers);

    amxc_var_set(cstring_t, &servers, dnsservers);

    SAH_TRACEZ_INFO(ME, "Disabling forward zone (%s)", zone_name);
    amxc_var_cast(&servers, AMXC_VAR_ID_LIST);

    amxc_var_for_each(server_ip, &servers) {
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "name", GET_CHAR(server_ip, NULL));
        amxc_var_add_key(cstring_t, &args, "zone_name", zone_name);
        remove_server(&args, &ret, false);
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&servers);
}

void fwzone_disable(common_t* core, const char* dnsservers) {
    amxd_object_t* parent_fwzone = NULL;
    const char* zone_name = NULL;

    parent_fwzone = amxd_object_findf(core->obj, ".^.^");
    zone_name = object_da_string(parent_fwzone, "Name");
    when_str_empty_trace(zone_name, exit, ERROR, "Unable to retrieve forward zone name");

    fwzone_disable_forwarding(zone_name, dnsservers);
exit:
    return;
}

static void fwzone_clear_name(amxd_object_t* fwzone_instance, const char* zone_name) {
    amxd_object_for_each(instance, it, amxd_object_findf(fwzone_instance, "%s", "Forwarding")) {
        amxd_object_t* forwarding = amxc_llist_it_get_data(it, amxd_object_t, it);
        common_t* core = (common_t*) forwarding->priv;
        if((core == NULL) || !core->is_enabled) {
            continue;
        }
        fwzone_disable_forwarding(zone_name, object_da_string(forwarding, "DNSServer"));
        core->is_enabled = false;
        set_status(core->obj, DISABLED);
    }
}

amxd_status_t _fwzone_cleanup(amxd_object_t* obj,
                              UNUSED amxd_param_t* param,
                              amxd_action_t reason,
                              UNUSED const amxc_var_t* const args,
                              UNUSED amxc_var_t* const retval,
                              UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    when_null(obj, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    // I assume that amx destroyed all Forwarding instances but just to be sure:
    fwzone_clear_name(obj, object_da_string(obj, "Name"));

    status = amxd_status_ok;
exit:
    return status;
}

void _fwzone_name_changed(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(get_dm(), data);
    const char* zone_name = NULL;

    when_null(obj, exit);

    zone_name = GETP_CHAR(data, "parameters.Name.from");
    if(!STRING_EMPTY(zone_name)) {
        fwzone_clear_name(obj, zone_name);
    }

    zone_name = GETP_CHAR(data, "parameters.Name.to");
    if(!STRING_EMPTY(zone_name)) {
        amxd_object_for_each(instance, it, amxd_object_findf(obj, "%s", "Forwarding")) {
            amxd_object_t* forwarding = amxc_llist_it_get_data(it, amxd_object_t, it);
            common_t* core = (common_t*) forwarding->priv;
            if(core == NULL) {
                continue;
            }
            if(core->is_enabled) {
                SAH_TRACEZ_ERROR(ME, "%s should not be enabled", core->name);
                continue;
            }
            core->is_enabled = fwzone_enable(core);
            set_status(core->obj, core->is_enabled ? ENABLED : ERROR);
        }
    }
exit:
    return;
}

void _fwzone_added(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(get_dm(), data);
    amxd_object_t* obj = amxd_object_get_instance(templ, NULL, GET_UINT32(data, "index"));

    when_null(obj, exit);

    parent_enable_changed(obj);

exit:
    return;
}

void fwzone_start(void) {
    amxd_object_t* fwzone_templ = amxd_dm_findf(get_dm(), "DNS.%sForwardZone.", get_prefix());

    amxd_object_for_each(instance, it, fwzone_templ) {
        amxd_object_t* obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        parent_enable_changed(obj);
    }
}