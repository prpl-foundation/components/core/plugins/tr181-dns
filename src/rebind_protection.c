/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tr181-dns.h"
#include "dm_common.h"
#include "dm_rebind.h"

#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>
#include <amxm/amxm.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "dns"

#define EMPTY_STRING(str) ((str == NULL) || (str[0] == '\0'))

static int rebind_add_exceptions_to_args(amxd_object_t* rebind, const cstring_t name, amxc_var_t* args) {
    amxd_object_t* tmpl = amxd_object_get(rebind, name);
    amxc_string_t str;
    int rv = -1;

    amxc_string_init(&str, 0);

    when_null_trace(tmpl, exit, ERROR, "Failed to get template object of %s", name);

    amxd_object_for_each(instance, it, tmpl) {
        amxd_object_t* obj = amxc_container_of(it, amxd_object_t, it);
        const cstring_t address = GET_CHAR(amxd_object_get_param_value(obj, "Address"), NULL);
        if(EMPTY_STRING(address)) {
            continue;
        }
        if(!amxc_string_is_empty(&str)) {
            amxc_string_append(&str, ",", strlen(","));
        }
        amxc_string_append(&str, address, strlen(address));
    }
    amxc_var_add_key(csv_string_t, args, name, amxc_string_get(&str, 0));
    rv = 0;
exit:
    amxc_string_clean(&str);
    return rv;
}

void _rebind_config_event(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(get_dm(), data);
    int rv = -1;
    amxc_var_t ret;
    amxc_var_t args;

    when_null(obj, exit_no_clean);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);

    amxc_var_add_key(bool, &args, "Enable", amxd_object_get_bool(obj, "Enable", NULL));
    rv = rebind_add_exceptions_to_args(obj, "IPExceptions", &args);
    when_failed_trace(rv, exit, ERROR, "Failed to get IP exceptions: %d", rv);
    rv = rebind_add_exceptions_to_args(obj, "DomainExceptions", &args);
    when_failed_trace(rv, exit, ERROR, "Failed to get Domain exceptions: %d", rv);

    rv = amxm_execute_function("dns", "dns", "rebind-changed", &args, &ret);
    when_failed_trace(rv, exit, ERROR, "Failed to execute rebind-changed dm function: %d", rv);
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
exit_no_clean:
    return;
}

static amxd_object_t* rebind_get(void) {
    return amxd_dm_findf(get_dm(), "DNS.%sRebindProtection", get_prefix());
}

static void rebind_emit_config_event(void) {
    amxd_object_t* rebind = rebind_get();
    amxd_object_emit_signal(rebind, "Config", NULL);
}

void rebind_start(void) {
    rebind_emit_config_event();
}

void _rebind_changed(UNUSED const char* const sig_name,
                     UNUSED const amxc_var_t* const data,
                     UNUSED void* const priv) {
    amxc_var_t* params = GET_ARG(data, "parameters");
    amxc_var_t* ipex_numofentries = GET_ARG(params, "IPExceptionsNumberOfEntries");
    amxc_var_t* domainex_numofentries = GET_ARG(params, "DomainExceptionsNumberOfEntries");
    const amxc_htable_t* hparams = amxc_var_constcast(amxc_htable_t, params);

    amxc_var_take_it(ipex_numofentries);
    amxc_var_take_it(domainex_numofentries);
    if(amxc_htable_is_empty(hparams)) {
        goto exit;
    }

    rebind_emit_config_event();

exit:
    amxc_var_set_key(params, "IPExceptionsNumberOfEntries", ipex_numofentries, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(params, "DomainExceptionsNumberOfEntries", domainex_numofentries, AMXC_VAR_FLAG_DEFAULT);
}
