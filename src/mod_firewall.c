/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace_macros.h>

#include "tr181-dns.h"
#include "dm_relay.h"

#include <amxm/amxm.h>
#include <amxd/amxd_path.h>

#define ME "dns"

static uint32_t count = 0;

bool open_firewall(const char* interface, char** alias) {
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    if(*alias == NULL) {
        int wbytes = asprintf(alias, "dns-%u", ++count);
        when_false_trace(wbytes > (int) strlen("dns-"), exit, ERROR, "Failed to create alias");
    }

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", *alias);
    amxc_var_add_key(cstring_t, &args, "interface", interface);
    amxc_var_add_key(uint32_t, &args, "destination_port", 53);
    amxc_var_add_key(cstring_t, &args, "protocol", "17,6");
    amxc_var_add_key(uint32_t, &args, "ipversion", 0);
    amxc_var_add_key(bool, &args, "enable", true);

    rv = amxm_execute_function("fw", "fw", "set_service", &args, &ret);
    if(rv != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to add firewall service[%s]", *alias);
        FREE(*alias);
        goto exit;
    }

    SAH_TRACEZ_NOTICE(ME, "Firewall service[%s] added", *alias);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv == 0;
}

void close_firewall(const char* alias) {
    amxc_var_t ret;
    amxc_var_t args;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "id", alias);

    rv = amxm_execute_function("fw", "fw", "delete_service", &args, &ret);
    when_false_trace(rv == 0, exit, WARNING, "Firewall service[%s] could not be removed", alias);

    SAH_TRACEZ_NOTICE(ME, "Firewall service[%s] removed", alias);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
