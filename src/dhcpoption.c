/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tr181-dns.h"
#include "dhcpoption.h"

#include <debug/sahtrace_macros.h>
#include <amxd/amxd_object_event.h>

#define ME "dns"

#define RA_RECURSIVE_DNS_SERVER 25
#define DHCP_DOMAIN_SERVER 6
#define DNS_DHCPv6_OPTION_DNS_SERVERS 23

netmodel_query_t* query_dnsservers(const char* tr181_interface, dns_type_t type, netmodel_callback_t query_cb, void* priv) {
    netmodel_query_t* query = NULL;

    switch(type) {
    case DNS_DHCPv4:
        query = netmodel_openQuery_getDHCPOption(tr181_interface, "DNS", "req", DHCP_DOMAIN_SERVER, netmodel_traverse_down, query_cb, priv);
        break;
    case DNS_DHCPv6:
        query = netmodel_openQuery_getDHCPOption(tr181_interface, "DNS", "req6", DNS_DHCPv6_OPTION_DNS_SERVERS, netmodel_traverse_down, query_cb, priv);
        break;
    case DNS_RA:
        query = netmodel_openQuery_getDHCPOption(tr181_interface, "DNS", "ra", RA_RECURSIVE_DNS_SERVER, netmodel_traverse_down, query_cb, priv);
        break;
    case DNS_IPCP:
        query = netmodel_openQuery_getFirstParameter(tr181_interface, "DNS", "DNSServers", "ppp-up", netmodel_traverse_down, query_cb, priv);
        break;
    case DNS_3GPP_NAS:
        query = netmodel_openQuery_getFirstParameter(tr181_interface, "DNS", "DNSServers", "cellular-up", netmodel_traverse_down, query_cb, priv);
        break;
    default:
        SAH_TRACEZ_WARNING(ME, "Type[%d] is not supported", type);
    }

    return query;
}
