/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ares.h>
#include <ares_dns.h>
#include <arpa/nameser.h>
#include <arpa/nameser_compat.h>
#include <netdb.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "nslookup.h"
#include "tr181-dns.h"

#define ME "nslookup"

#define IP_RECORD_MAX 10
#define REPETITION_DELAY 500

static amxc_var_t* list_get(amxc_var_t* list, int value) {
    amxc_var_t* found_var = NULL;
    amxc_var_for_each(var, list) {
        int val = GET_INT32(var, NULL);
        if(val == value) {
            found_var = var;
            break;
        }
    }
    return found_var;
}

static void nslookup_eventloop_cb(int fd, void* data) {
    nslookup_query_t* query = (nslookup_query_t*) data;
    when_null_trace(query, exit, ERROR, "NS lookup query is NULL");
    ares_process_fd(query->channel, fd, ARES_SOCKET_BAD);
exit:
    return;
}

static void nslookup_sock_cb(void* data, ares_socket_t socket_fd, int readable, int writable) {
    nslookup_query_t* query = (nslookup_query_t*) data;
    when_null_trace(query, exit, ERROR, "NS lookup query is NULL");

    if(readable + writable > 0) {
        if(query->ares_fds == NULL) {
            amxc_var_new(&query->ares_fds);
            amxc_var_set_type(query->ares_fds, AMXC_VAR_ID_LIST);
        }
        if(list_get(query->ares_fds, socket_fd) == NULL) {
            amxc_var_add(int32_t, query->ares_fds, socket_fd);
            SAH_TRACEZ_INFO(ME, "Adding fd %d to event loop", socket_fd);
            amxp_connection_add(socket_fd, nslookup_eventloop_cb, NULL, AMXO_CUSTOM, query);
            // Start new timer for timeout in case of DNS server issue
            amxp_timer_start(query->timer_timeout_server, query->timeout);
        } else {
            SAH_TRACEZ_INFO(ME, "Ares fd list already contains %d", socket_fd);
        }
    } else {
        amxc_var_t* found_var = list_get(query->ares_fds, socket_fd);
        if(found_var != NULL) {
            SAH_TRACEZ_INFO(ME, "Remove fd %d from event loop", socket_fd);
            amxp_connection_remove(socket_fd);
            amxc_var_delete(&found_var);
        } else {
            SAH_TRACEZ_INFO(ME, "%d not in fd list", socket_fd);
        }
    }
exit:
    return;
}

static void nslookup_addrinfo_cb(void* arg, int status, UNUSED int timeouts, struct ares_addrinfo* result) {
    nslookup_query_t* query = (nslookup_query_t*) arg;
    amxc_var_t ip_address_list;
    int ip_address_count = 0;
    char ip_address[INET6_ADDRSTRLEN] = {'\0'};
    const char* status_str = NULL;
    const char* domain = "";
    struct sockaddr_in* sa = NULL;
    struct sockaddr_in6* sa6 = NULL;
    int response_time;

    when_true_trace(status == ARES_EDESTRUCTION, exit, INFO, "Channel is being destroyed");
    when_null_trace(query, exit, ERROR, "NS lookup query is NULL");
    when_null_trace(query->cb_fn, exit, ERROR, "NS lookup query callback function is NULL");

    amxp_timers_calculate();
    response_time = query->timeout - amxp_timer_remaining_time(query->timer_timeout_server);
    amxp_timer_stop(query->timer_timeout_server);
    amxp_timer_stop(query->timer_timeout_network);

    amxc_var_init(&ip_address_list);
    amxc_var_set_type(&ip_address_list, AMXC_VAR_ID_LIST);

    SAH_TRACEZ_INFO(ME, "NS lookup test finished : '%s'", ares_strerror(status));

    switch(status) {
    case ARES_SUCCESS:
        status_str = "Success";
        for(struct ares_addrinfo_node* ai = result->nodes; ai != NULL && ip_address_count < IP_RECORD_MAX; ai = ai->ai_next) {
            if(ai->ai_family == AF_INET) {
                sa = (struct sockaddr_in*) ai->ai_addr;
                inet_ntop(AF_INET, &sa->sin_addr, ip_address, INET6_ADDRSTRLEN);
            } else if(ai->ai_family == AF_INET6) {
                sa6 = (struct sockaddr_in6*) ai->ai_addr;
                inet_ntop(AF_INET6, &sa6->sin6_addr, ip_address, INET6_ADDRSTRLEN);
            } else {
                continue;
            }

            amxc_var_add(cstring_t, &ip_address_list, ip_address);
            ip_address_count++;
            SAH_TRACEZ_INFO(ME, "Resolved IP address in %d ms (%d): %s", response_time, ip_address_count, ip_address);
        }

        if(result->cnames != NULL) {
            domain = result->cnames->name;
            SAH_TRACEZ_INFO(ME, "Resolved hostname : %s", domain);
        } else {
            domain = query->domain;
        }
        break;

    case ARES_ENOTFOUND:
        status_str = "Error_HostNameNotResolved";
        break;

    case ARES_ECONNREFUSED:
    case ARES_ESERVFAIL:
    case ARES_EREFUSED:
        status_str = "Error_DNSServerNotAvailable";
        break;

    case ARES_ETIMEOUT:
        status_str = "Error_Timeout";
        break;

    default:
        SAH_TRACEZ_WARNING(ME, "Cancel remaining test");
        status_str = "Error_Other";
        query->repetition_left = 0;
        break;
    }

    query->cb_fn(status_str, domain, &ip_address_list, query->server, status == ARES_SUCCESS ? response_time : 0, query->authoritative, query->repetition_left);

    if(query->repetition_left) {
        amxp_timer_start(query->timer_repetition, REPETITION_DELAY);
    } else {
        SAH_TRACEZ_INFO(ME, "Clean timer started");
        amxp_timer_start(query->timer_clean, 100);
    }

exit:
    if(result != NULL) {
        ares_freeaddrinfo(result);
    }
    amxc_var_clean(&ip_address_list);
    return;
}

static void nslookup_ns_cb(void* arg, int status, UNUSED int timeouts,
                           unsigned char* abuf, int alen) {
    nslookup_query_t* query = (nslookup_query_t*) arg;
    struct hostent* host = NULL;

    when_null_trace(query, exit, ERROR, "NS lookup query is NULL");

    amxp_timer_stop(query->timer_timeout_server);
    amxp_timer_stop(query->timer_timeout_network);

    when_failed_trace(status, exit, ERROR, "Step 1 : NS query failed because %s", ares_strerror(status));
    status = ares_parse_ns_reply(abuf, alen, &host);
    when_failed_trace(status, exit, ERROR, "Step 1 : Failed to parse NS record because %s", ares_strerror(status));

    query->authoritative = DNS_HEADER_AA(abuf);

    SAH_TRACEZ_INFO(ME, "Step 1 : NS record received with authoritative flag %d\n", query->authoritative);

    amxp_timer_start(query->timer_next_query, 100);

exit:
    if(status != ARES_SUCCESS) {
        nslookup_addrinfo_cb(query, status, 0, NULL);
    }
}

static void nslookup_timeout_server_cb(UNUSED amxp_timer_t* timer, void* priv) {
    nslookup_query_t* query = (nslookup_query_t*) priv;
    when_null_trace(query, exit, ERROR, "NS lookup query is NULL");
    when_null_trace(query->channel, exit, ERROR, "Ares Channel is NULL");
    SAH_TRACEZ_WARNING(ME, "NS lookup server timeout");
    ares_process_fd(query->channel, ARES_SOCKET_BAD, ARES_SOCKET_BAD);
exit:
    return;
}

static void nslookup_timeout_network_cb(UNUSED amxp_timer_t* timer, void* priv) {
    nslookup_query_t* query = (nslookup_query_t*) priv;
    when_null_trace(query, exit, ERROR, "NS lookup query is NULL");
    when_null_trace(query->channel, exit, ERROR, "Ares Channel is NULL");
    SAH_TRACEZ_WARNING(ME, "NS lookup network timeout : cancel remaining test");
    query->repetition_left = 0;
    nslookup_addrinfo_cb(query, ARES_ETIMEOUT, 0, NULL);
exit:
    return;
}

static char* nslookup_resolve_server(const char* name) {
    struct addrinfo hints;
    struct addrinfo* result = NULL;
    struct in_addr* ipv4addr = NULL;
    struct in6_addr* ipv6addr = NULL;
    struct sockaddr_in sa;
    struct sockaddr_in6 sa6;
    char* ip_address = NULL;
    int rv = -1;

    when_str_empty_trace(name, exit, ERROR, "Server hostname to resolve is empty");

    if((inet_pton(AF_INET, name, &(sa.sin_addr)) == 1) || (inet_pton(AF_INET6, name, &(sa6.sin6_addr)) == 1)) {
        ip_address = strdup(name);
    } else {
        memset(&hints, 0, sizeof hints);
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_DGRAM;

        rv = getaddrinfo(name, NULL, &hints, &result);
        when_failed_trace(rv, exit, ERROR, "Failed to resolve hostname '%s'", name);

        for(struct addrinfo* rp = result; rp != NULL; rp = rp->ai_next) {
            if(rp->ai_family == AF_INET) {
                ipv4addr = &((struct sockaddr_in*) rp->ai_addr)->sin_addr;
                ip_address = calloc(1, INET_ADDRSTRLEN);
                when_null_trace(ip_address, exit, ERROR, "Failed to allocate memory");
                inet_ntop(AF_INET6, ipv4addr, ip_address, INET_ADDRSTRLEN);
                break;
            } else if(rp->ai_family == AF_INET6) {
                ipv6addr = &((struct sockaddr_in6*) rp->ai_addr)->sin6_addr;
                ip_address = calloc(1, INET6_ADDRSTRLEN);
                when_null_trace(ip_address, exit, ERROR, "Failed to allocate memory");
                inet_ntop(AF_INET6, ipv6addr, ip_address, INET6_ADDRSTRLEN);
                break;
            }
        }
    }

exit:
    if(result != NULL) {
        freeaddrinfo(result);
    }
    return ip_address;
}

static void nslookup_getaddrinfo(UNUSED amxp_timer_t* timer, void* priv) {
    struct ares_addrinfo_hints hints;
    nslookup_query_t* query = (nslookup_query_t*) priv;

    when_null_trace(query, exit, ERROR, "NS lookup query is NULL");

    memset(&hints, 0, sizeof(struct ares_addrinfo_hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = ARES_AI_NOSORT;

    SAH_TRACEZ_INFO(ME, "Step 2 : Sending queries to get A & AAAA record and resolve address");

    // Start new timer for timeout in case of global network issue
    amxp_timer_start(query->timer_timeout_network, query->timeout + 1000);
    ares_getaddrinfo(query->channel, query->domain, NULL, &hints, nslookup_addrinfo_cb, query);
exit:
    return;
}

static void nslookup_getauthorityinfo(UNUSED amxp_timer_t* timer, void* priv) {
    nslookup_query_t* query = (nslookup_query_t*) priv;

    when_null_trace(query, exit, ERROR, "NS lookup query is NULL");

    query->repetition_left--;

    SAH_TRACEZ_INFO(ME, "New try : %d repetition left", query->repetition_left);
    SAH_TRACEZ_INFO(ME, "Step 1 : Sending query to get NS record and get authority flag");

    // Start new timer for timeout in case of global network issue
    amxp_timer_start(query->timer_timeout_network, query->timeout + 1000);
    // Initial query to get authoritative flag
    ares_query(query->channel, query->domain, C_IN, T_NS, nslookup_ns_cb, query);
exit:
    return;
}

static void nslookup_clean(UNUSED amxp_timer_t* timer, void* priv) {
    nslookup_query_t* query = (nslookup_query_t*) priv;

    when_null_trace(query, exit, ERROR, "NS lookup query is NULL");

    SAH_TRACEZ_INFO(ME, "Cleaning NS loookup objects");
    free(query->domain);
    free(query->server);
    amxp_timer_delete(&query->timer_next_query);
    amxp_timer_delete(&query->timer_timeout_server);
    amxp_timer_delete(&query->timer_timeout_network);
    amxp_timer_delete(&query->timer_repetition);
    amxp_timer_delete(&query->timer_clean);
    if(query->ares_fds != NULL) {
        amxc_var_for_each(var, query->ares_fds) {
            amxp_connection_remove(GET_INT32(var, NULL));
        }
        amxc_var_delete(&query->ares_fds);
    }
    ares_destroy(query->channel);
    free(query);

exit:
    return;
}

bool nslookup_initialize(void) {
    int rv = -1;
    rv = ares_library_init(ARES_LIB_INIT_NONE);
    when_failed_trace(rv, exit, ERROR, "ares_library_init: %s", ares_strerror(rv));
exit:
    return rv == 0;
}

void nslookup_deinit(void) {
    ares_library_cleanup();
}

nslookup_status_t nslookup_start(const char* interface, const char* server, const char* domain_name, int timeout, int repetition, nslookup_query_cb callback_fn) {
    int rv = ARES_ENOMEM;
    struct ares_options options;
    struct ares_addr_node* server_list = NULL;
    int optmask = 0;
    nslookup_query_t* query = NULL;
    nslookup_status_t status = NSLOOKUP_OTHER;

    when_null_trace(interface, exit, ERROR, "Interface is NULL");
    when_null_trace(server, exit, ERROR, "DNS server is NULL");
    when_str_empty_trace(domain_name, exit, ERROR, "Domain name is empty");
    when_false_trace(repetition > 0, exit, ERROR, "Repetition is equal to 0");
    when_false_trace(timeout > 0, exit, ERROR, "Timeout is equal to 0");

    SAH_TRACEZ_INFO(ME, "Starting NS lookup diagnostic via interface '%s' and DNS server '%s' on '%s' (%d repetitions with timeout %d ms)", interface, server, domain_name, repetition, timeout);

    query = calloc(1, sizeof(nslookup_query_t));
    when_null_trace(query, exit, ERROR, "Failed to allocate memory for NS lookup query");

    query->cb_fn = callback_fn;
    query->domain = strdup(domain_name);
    query->repetition_left = repetition;
    query->timeout = timeout;

    rv = amxp_timer_new(&query->timer_timeout_network, nslookup_timeout_network_cb, query);
    rv |= amxp_timer_new(&query->timer_timeout_server, nslookup_timeout_server_cb, query);
    rv |= amxp_timer_new(&query->timer_repetition, nslookup_getauthorityinfo, query);
    rv |= amxp_timer_new(&query->timer_clean, nslookup_clean, query);
    rv |= amxp_timer_new(&query->timer_next_query, nslookup_getaddrinfo, query);
    when_failed_trace(rv, exit, ERROR, "Failed to allocate timers");

    options.sock_state_cb = nslookup_sock_cb;
    options.sock_state_cb_data = query;
    options.tries = 1;
    options.timeout = query->timeout;
    optmask = ARES_OPT_SOCK_STATE_CB | ARES_OPT_TIMEOUTMS | ARES_OPT_TRIES;

    rv = ares_init_options(&query->channel,
                           &options,
                           optmask);
    when_failed_trace(rv, exit, ERROR, "Failed to set options for ares : %s", ares_strerror(rv));

    if(!STRING_EMPTY(server)) {
        query->server = nslookup_resolve_server(server);
        if(query->server == NULL) {
            SAH_TRACEZ_ERROR(ME, "Failed to resolve DNS server hostname");
            status = NSLOOKUP_SRVFAIL;
            goto exit;
        } else {
            SAH_TRACEZ_INFO(ME, "Resolved server IP : %s", query->server);
        }
        rv = ares_set_servers_csv(query->channel, query->server);
        when_failed_trace(rv, exit, ERROR, "Failed to set DNS server for ares : %s", ares_strerror(rv));
    } else {
        rv = ares_get_servers(query->channel, &server_list);
        when_failed_trace(rv, exit, ERROR, "Failed to read DNS server for ares : %s", ares_strerror(rv));
        if(server_list->family == AF_INET) {
            query->server = calloc(1, INET_ADDRSTRLEN);
            when_null_trace(query->server, exit, ERROR, "Failed to allocate memory for DNS server string");
            inet_ntop(AF_INET, &server_list->addr.addr4, query->server, INET_ADDRSTRLEN);
        } else if(server_list->family == AF_INET6) {
            query->server = calloc(1, INET6_ADDRSTRLEN);
            when_null_trace(query->server, exit, ERROR, "Failed to allocate memory for DNS server string");
            inet_ntop(AF_INET6, &server_list->addr.addr6, query->server, INET6_ADDRSTRLEN);
        } else {
            query->server = strdup("");
            SAH_TRACEZ_ERROR(ME, "No default DNS server configured");
            goto exit;
        }
        SAH_TRACEZ_INFO(ME, "Default DNS server for test : %s", query->server);
    }

    if(!STRING_EMPTY(interface)) {
        ares_set_local_dev(query->channel, interface);
    }

    nslookup_getauthorityinfo(NULL, query);
    status = NSLOOKUP_OK;

exit:
    if(server_list != NULL) {
        ares_free_data(server_list);
    }

    if((status != NSLOOKUP_OK) && (query != NULL)) {
        nslookup_clean(NULL, query);
    }
    return status;
}