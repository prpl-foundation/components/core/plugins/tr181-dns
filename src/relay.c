/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tr181-dns.h"
#include "dm_common.h"
#include "dm_relay.h"
#include "netdevname.h"

#include <amxm/amxm.h>
#include <amxd/amxd_transaction.h>
#include <debug/sahtrace_macros.h>
#include <ipat/ipat.h>

#define ME "relay"

static amxd_object_t* cache_object = NULL;

static amxd_object_t* relay_get_config_object(void) {
    return cache_object;
}

static bool type_is_ipv4(dns_type_t type) {
    return ((type == DNS_DHCPv4) ||
            (type == DNS_IPCP) || (type == DNS_3GPP_NAS));
}

bool relay_is_blocked(common_t* core) {
    amxd_object_t* config_obj = NULL;
    const char* param = NULL;
    const char* expected_value = NULL;
    const char* dnsmode = NULL;
    bool is_blocked = true;

    if(core->type != CORE_IS_RELAY) {
        SAH_TRACEZ_ERROR(ME, "%s type %d is not blockable by this function", core->name, core->type);
        is_blocked = false;
        goto exit;
    }

    config_obj = relay_get_config_object();
    if(config_obj == NULL) {
        SAH_TRACEZ_INFO(ME, "%s failed to find config object", core->name);
        is_blocked = false; // I allow it because object DNS.Relay.X_PRPL-COM_Config has a vendor prefix.
        goto exit;
    }

    when_true(core->origin == DNS_TYPE_ERROR, exit);

    if(core->origin == DNS_STATIC) {
        const char* ipaddr = NULL;
        int ipaddr_family = AF_INET_INVALID;

        ipaddr = object_da_string(core->obj, "DNSServer");
        when_str_empty_trace(ipaddr, exit, ERROR, "%s misconfigured because DSNServer is empty", core->name);

        ipaddr_family = ipat_text_family(ipaddr);
        when_false_trace(ipaddr_family, exit, ERROR, "%s is misconfigured because bad value of DNSServer", core->name);

        param = (ipaddr_family == AF_INET) ? "DNSMode" : "IPv6DNSMode";
        expected_value = "Static";
    } else {
        param = type_is_ipv4(core->origin) ? "DNSMode" : "IPv6DNSMode";
        expected_value = "Dynamic";
    }

    dnsmode = object_da_string(config_obj, param);

    is_blocked = ((dnsmode == NULL) || ((strcmp(dnsmode, "Any") != 0) && (strcmp(dnsmode, expected_value) != 0)));
    when_true_trace(is_blocked, exit, INFO, "%s is blocked because %s (%s != %s)", core->name, param, dnsmode, expected_value);

exit:
    return is_blocked;
}

bool private_server_info(amxc_var_t* server) {
    amxd_object_t* forwarding = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t params;
    common_t* core = NULL;
    amxc_string_t ipaddr;
    const char* tag = NULL;
    const char* netdevname = "";
    const char* tr181_interface = NULL;
    bool rv = false;

    uint32_t common = IPAT_TYPE_NONE;
    uint32_t family = IPAT_TYPE_NONE;
    ipat_t ip = IPAT_INITIALIZE;

    amxc_var_init(&params);
    amxc_string_init(&ipaddr, 0);

    forwarding = amxd_dm_findf(get_dm(), "DNS.Relay.Forwarding.%s.", GET_CHAR(server, "name"));
    when_null_trace(forwarding, exit, ERROR, "object '%s' not found", GET_CHAR(server, "name"));

    when_null_trace(forwarding->priv, exit, ERROR, "object %s has no private data", GET_CHAR(server, "name"));
    core = (common_t*) forwarding->priv;
    when_true_trace(core->is_blocked, exit, INFO, "object %s is blocked", GET_CHAR(server, "name"));

    status = amxd_object_get_params(forwarding, &params, amxd_dm_access_protected);
    when_false_trace(status == amxd_status_ok, exit, ERROR, "Failed to access parameters: %d", status);

    tr181_interface = GET_CHAR(&params, "Interface");
    if(!STRING_EMPTY(tr181_interface)) {
        netdevname = core->netdevname;
        when_str_empty_trace(netdevname, exit, INFO, "Wait for netdevname");
    }

    when_str_empty_trace(GET_CHAR(&params, "DNSServer"), exit, INFO, "Wait for DNSServer");
    amxc_string_setf(&ipaddr, "%s", GET_CHAR(&params, "DNSServer"));

    // Check if ip address is link local. If so append the interfacename to it.
    ipat_pton(&ip, AF_INET6, amxc_string_get(&ipaddr, 0));
    common = ipat_type(&ip, &family);
    if(common & IPAT_TYPE_LINKLOCAL && family & IPAT_TYPE_IPV6_LLA) {
        amxc_string_appendf(&ipaddr, "%%%s", netdevname);
    }

    amxc_var_add_key(cstring_t, server, "server", amxc_string_get(&ipaddr, 0));
    amxc_var_add_key(cstring_t, server, "interface", netdevname); // needed for mod-dns-uci (dnsmasq)
    tag = GET_CHAR(&params, "Tag");                               // for backwards compatibility reason. should be removed
    amxc_var_add_key(cstring_t, server, "tag", STRING_EMPTY(tag) ? "" : tag);
    amxc_var_add_key(bool, server, "enable", GET_BOOL(&params, "Enable"));
    rv = true;
exit:
    amxc_string_clean(&ipaddr);
    amxc_var_clean(&params);
    return rv;
}

static bool add_object_path_to_args(amxd_object_t* obj, amxc_var_t* args) {
    amxc_var_t* var = NULL;
    char* path = NULL;
    bool rv = false;

    path = amxd_object_get_path(obj, AMXD_OBJECT_INDEXED | AMXD_OBJECT_TERMINATE);
    when_str_empty(path, exit);

    var = amxc_var_add_new_key(args, "path");
    when_null_trace(var, exit, ERROR, "failed to add key");
    when_failed_trace(amxc_var_push(cstring_t, var, path), exit, ERROR, "failed to push string");
    path = NULL; // should not be freed at end of scope of this function (var is the new owner)

    rv = true;

exit:
    free(path);
    return rv;
}

bool relay_enable(common_t* core) {
    amxc_var_t* args = NULL;
    amxc_var_t ret;
    int amxm_rv = -1;
    bool rv = false;

    amxc_var_new(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, args, "name", core->name);
    when_false(add_object_path_to_args(core->obj, args), exit); // add object path for failover module
    when_false(private_server_info(args), exit);                // for backward compatibility, info is still added here (it is also added if core's set-server is called)

    amxm_rv = add_server(args, &ret, true);
    when_false_trace(amxm_rv == 0, exit, INFO, "Failed to add server '%s'", GET_CHAR(args, "name"));

    rv = true;

exit:
    amxc_var_delete(&args);
    amxc_var_clean(&ret);
    return rv;
}

void relay_enable_all(void) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* list = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    list = amxc_var_add_key(amxc_htable_t, &args, "servers", NULL);

    amxd_object_for_each(instance, it, amxd_dm_findf(get_dm(), "%s", "DNS.Relay.Forwarding.")) {
        amxd_object_t* forwarding = amxc_container_of(it, amxd_object_t, it);
        common_t* core = (common_t*) forwarding->priv;
        amxc_var_t* server = NULL;
        const char* name = NULL;

        name = amxd_object_get_name(forwarding, AMXD_OBJECT_NAMED);
        server = amxc_var_add_key(amxc_htable_t, list, name, NULL);
        amxc_var_add_key(cstring_t, server, "name", name);
        if(!private_server_info(server)) {
            SAH_TRACEZ_INFO(ME, "remove '%s' from set-server args", name);
            amxc_var_delete(&server);
            if(core != NULL) {
                if(!core->is_blocked) {
                    core->is_enabled = false;
                    set_status(core->obj, ERROR);
                } // else is blocked and status will be set already
            }
        } else {
            if(core != NULL) {
                core->is_enabled = true;
                set_status(core->obj, ENABLED);
            }
        }
    }

    amxc_var_add_key(uint32_t, &args, "timeout", 0);
    amxc_var_add_key(uint32_t, &args, "repeat", 0);

    if(sahTraceZoneLevel(sahTraceGetZone(ME)) >= TRACE_LEVEL_INFO) {
        SAH_TRACEZ_INFO(ME, "set-server args:");
        amxc_var_log(&args);
    }

    // potentially a reconfiguration of backend for each object so use set-server instead of add-server
    set_server(&args, &ret, false);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void relay_disable(common_t* core, UNUSED const char* dnsserver) {
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "name", core->name);
    when_false(add_object_path_to_args(core->obj, &args), exit); // add object path for failover module

    rv = remove_server(&args, &ret, true);
    when_false_trace(rv == 0, exit, INFO, "Failed to disable '%s'", core->name);
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void update_cache_object(amxd_object_t* object) {
    if(cache_object == object) {
        uint32_t ttl_max = 86400;
        uint32_t ttl_min = 0;
        uint32_t cache_size = 0;
        uint32_t old_ttl_max = amxd_object_get_value(uint32_t, object, "CacheMaxTTL", NULL);
        uint32_t old_ttl_min = amxd_object_get_value(uint32_t, object, "CacheMinTTL", NULL);
        uint32_t old_cache_size = amxd_object_get_value(uint32_t, object, "CacheSize", NULL);

        cache_object = NULL;
        amxd_object_for_each(instance, it, amxd_object_get_parent(object)) {
            amxd_object_t* next_object = amxc_llist_it_get_data(it, amxd_object_t, it);
            if(next_object != object) {
                cache_object = next_object;
                break;
            }
        }

        if(cache_object != NULL) {
            ttl_max = amxd_object_get_value(uint32_t, cache_object, "CacheMaxTTL", NULL);
            ttl_min = amxd_object_get_value(uint32_t, cache_object, "CacheMinTTL", NULL);
            cache_size = amxd_object_get_value(uint32_t, cache_object, "CacheSize", NULL);
        }

        if((ttl_max != old_ttl_max) ||
           (ttl_min != old_ttl_min) ||
           (cache_size != old_cache_size)) {
            if(cache_object != NULL) {
                SAH_TRACEZ_INFO(ME, "Limitation: cache is configured with instance[%s]", amxd_object_get_name(cache_object, AMXD_OBJECT_NAMED));
            } else {
                SAH_TRACEZ_INFO(ME, "Limitation: cache is disabled");
            }
            set_cache(cache_size, ttl_max, ttl_min);
        }
    }
}

static void update_domain_name(relay_config_t* config) {
    amxd_object_t* conf_obj = NULL;
    when_null_trace(config, exit, ERROR, "Config is NULL");
    conf_obj = config->obj;
    when_null_trace(conf_obj, exit, ERROR, "No object belongs to config");

    if(!STRING_EMPTY(config->netdevname)) {
        const char* domain_name = object_da_string(conf_obj, "DomainName");
        set_domain_name(config->netdevname, domain_name);
    }
exit:
    return;
}

static void config_netdevname_changed(UNUSED const char* sig_name, const amxc_var_t* result, void* priv) {
    relay_config_t* config = (relay_config_t*) priv;
    const char* netdevname = GET_CHAR(result, NULL);
    char* old_netdevname = NULL;

    when_true(strcmp_safe(config->netdevname, netdevname) == 0, exit);

    SAH_TRACEZ_INFO(ME, "%s netdevname query result changed %s -> %s", amxd_object_get_name(config->obj, AMXD_OBJECT_NAMED), config->netdevname, netdevname);

    old_netdevname = config->netdevname;
    config->netdevname = netdevname != NULL ? strdup(netdevname) : NULL;

    if(config->query_netdevname != NULL) {
        remove_lan_interface(old_netdevname);
        add_lan_interface(config->netdevname);
        update_domain_name(config);
    }  // else called from openQuery
    free(old_netdevname);
exit:
    return;
}

static void config_iface_changed(relay_config_t* config) {
    const char* iface = NULL;

    if(config->query_netdevname != NULL) {
        remove_lan_interface(config->netdevname);
        CLOSE_QUERY(config->query_netdevname);
        FREE(config->netdevname);
    }

    iface = object_da_string(config->obj, "Interface");
    if(!STRING_EMPTY(iface)) {
        config->query_netdevname = query_netdevname(iface, config_netdevname_changed, config);
        when_null_trace(config->query_netdevname, exit, ERROR, "Failed to open query");
        if(!STRING_EMPTY(config->netdevname)) {
            add_lan_interface(config->netdevname);
            update_domain_name(config);
        } // else waiting for query result
    }
exit:
    return;
}

static void config_init(amxd_object_t* object) {
    relay_config_t* config = NULL;
    const char* tr181_interface = object_da_string(object, "Interface");

    when_false(object->priv == NULL, exit);
    config = (relay_config_t*) calloc(1, sizeof(relay_config_t));
    object->priv = config;
    when_null(object->priv, exit);
    config->obj = object;

    /* For now don't configure cache per lan interface.
       Warn if more then one cache configuration.
     */
    if(cache_object == NULL) {
        uint32_t ttl_max = amxd_object_get_value(uint32_t, object, "CacheMaxTTL", NULL);
        uint32_t ttl_min = amxd_object_get_value(uint32_t, object, "CacheMinTTL", NULL);
        uint32_t cache_size = amxd_object_get_value(uint32_t, object, "CacheSize", NULL);
        cache_object = object;
        SAH_TRACEZ_INFO(ME, "Limitation: cache is configured with instance[%s]", amxd_object_get_name(cache_object, AMXD_OBJECT_NAMED));
        set_cache(cache_size, ttl_max, ttl_min);
    } else {
        SAH_TRACEZ_WARNING(ME, "Limitation: ignoring cache configuration of instance[%s]", amxd_object_get_name(object, AMXD_OBJECT_NAMED));
    }

    when_str_empty_trace(tr181_interface, exit, INFO, "Interface is empty");

    open_firewall(tr181_interface, &config->firewall);

    config_iface_changed(config);

exit:
    return;
}

void _config_added(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(get_dm(), data);
    amxd_object_t* object = amxd_object_get_instance(templ, NULL, GET_UINT32(data, "index"));
    when_null(object, exit);
    config_init(object);
exit:
    return;
}

void _config_changed(UNUSED const char* const sig_name,
                     const amxc_var_t* const data,
                     UNUSED void* const priv) {
    amxd_object_t* object = amxd_dm_signal_get_object(get_dm(), data);
    amxc_var_t* params = GET_ARG(data, "parameters");
    amxc_var_t* var = NULL;
    relay_config_t* config = NULL;

    when_null(object, exit);
    when_null_trace(object->priv, exit, ERROR, "priv data is NULL");
    config = (relay_config_t*) object->priv;

    if(cache_object == object) {
        amxc_var_t* var2 = NULL;
        amxc_var_t* var3 = NULL;

        var = GET_ARG(params, "CacheSize");
        var2 = GET_ARG(params, "CacheMinTTL");
        var3 = GET_ARG(params, "CacheMaxTTL");
        if((var != NULL) ||
           (var2 != NULL) ||
           (var3 != NULL)) {
            uint32_t cache_size = amxd_object_get_value(uint32_t, object, "CacheSize", NULL);
            uint32_t ttl_min = amxd_object_get_value(uint32_t, object, "CacheMinTTL", NULL);
            uint32_t ttl_max = amxd_object_get_value(uint32_t, object, "CacheMaxTTL", NULL);
            set_cache(cache_size, ttl_max, ttl_min);
        }
    }

    var = GET_ARG(params, "Interface");
    if(var != NULL) {
        open_firewall(GET_CHAR(var, "to"), &config->firewall);
        config_iface_changed(config);
    }

    var = GET_ARG(params, "DomainName");
    if((var != NULL)) {
        update_domain_name(config);
    }

exit:
    return;
}

void _dnsmode_changed(UNUSED const char* const sig_name,
                      const amxc_var_t* const data,
                      UNUSED void* const priv) {
    amxd_object_t* config_obj = amxd_dm_signal_get_object(get_dm(), data);

    when_null_trace(config_obj, exit, INFO, "object is NULL");
    when_false_trace(config_obj == relay_get_config_object(), exit, WARNING, "Limitation: only the [IPv6]DNSMode of %s is used", amxd_object_get_name(relay_get_config_object(), AMXD_OBJECT_NAMED))

    amxd_object_for_each(instance, it, amxd_object_findf(config_obj, ".^.^.Forwarding.")) {
        amxd_object_t* forwarding_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        common_t* core = NULL;

        if(forwarding_obj->priv == NULL) {
            SAH_TRACEZ_INFO(ME, "%s has no private data", amxd_object_get_name(forwarding_obj, AMXD_OBJECT_NAMED));
            continue;
        }
        core = (common_t*) forwarding_obj->priv;

        if(relay_is_blocked(core)) {
            core_block(core);
        } else {
            core_unblock(core, true);
        }
    }

exit:
    return;
}

amxd_status_t _config_on_destroy(amxd_object_t* obj,
                                 UNUSED amxd_param_t* param,
                                 amxd_action_t reason,
                                 UNUSED const amxc_var_t* const args,
                                 UNUSED amxc_var_t* const retval,
                                 UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    relay_config_t* config = NULL;

    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    when_null_status(obj->priv, exit, status = amxd_status_ok);
    config = (relay_config_t*) obj->priv;

    if(config->firewall != NULL) {
        close_firewall(config->firewall);
        free(config->firewall);
    }

    if(config->netdevname != NULL) {
        remove_lan_interface(config->netdevname);
        FREE(config->netdevname);
    }

    update_cache_object(obj);

    status = amxd_status_ok;
exit:
    FREE(obj->priv);
    return status;
}

amxd_status_t _FlushCache(UNUSED amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          UNUSED amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    return flush_cache() ? amxd_status_ok : amxd_status_unknown_error;
}

void relay_start(void) {
    amxc_var_t* options = NULL;

    amxd_object_for_each(instance, it, amxd_dm_findf(get_dm(), "DNS.Relay.%sConfig.", get_prefix())) {
        amxd_object_t* object = amxc_llist_it_get_data(it, amxd_object_t, it);
        config_init(object);
    }

    options = GETP_ARG(get_config(), "modules.failover-options"); // leaving this in for backward compatibility
    if(options != NULL) {
        SAH_TRACEZ_WARNING(ME, "deprecated use of modules.failover-options, it will be removed");
        set_options(failover_shared_object_name(), "failover", options);
    }

    options = GETP_ARG(get_config(), "modules.dns-options");
    if(options != NULL) {
        SAH_TRACEZ_INFO(ME, "load modules.dns-options");
        set_options("dns", "dns", options);
    }

    parent_enable_changed(amxd_object_get_parent(amxd_dm_findf(get_dm(), "%s", "DNS.Relay.Forwarding.")));
}
