/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tr181-dns.h"
#include "dm_host.h"
#include "dm_relay.h"
#include "dm_common.h"
#include "netdevname.h"

#include <amxd/amxd_transaction.h>
#include <debug/sahtrace_macros.h>
#include <ipat/ipat.h>

#define ME "host"

typedef struct {
    const char* name;
    char* netdevname;
    char* old_hostname;
    bool is_enabled;
    bool is_static;
    amxd_object_t* obj;
    netmodel_query_t* query_getaddrs;
} host_t;

static bool host_remove_host(const char* interface, const char* name) {
    amxc_var_t args;
    amxc_var_t ret;
    bool rv = false;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    amxc_var_add_key(cstring_t, &args, "interface", interface);

    when_failed_trace(remove_host(&args, &ret), exit, ERROR, "Failed to remove %s", name);

    SAH_TRACEZ_INFO(ME, "remove %s", name);

    rv = true;

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv;
}

static host_addr_t* new_host_addr(amxd_object_t* obj) {
    host_addr_t* host_addr = NULL;
    when_null(obj, exit);
    host_addr = (host_addr_t*) calloc(1, sizeof(host_addr_t));
    when_null(host_addr, exit);
    host_addr->name = amxd_object_get_name(obj, AMXD_OBJECT_NAMED);
    host_addr->obj = obj;
    obj->priv = host_addr;
exit:
    return host_addr;
}

static bool host_add_host(const char* interface, const char* name, amxd_object_t* obj) {
    amxd_object_t* templ = amxd_object_get(obj, "IPAddress");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* ipv4 = NULL;
    amxc_var_t* ipv6 = NULL;
    uint32_t count = 0;
    bool rv = false;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    amxc_var_add_key(cstring_t, &args, "interface", interface);
    ipv4 = amxc_var_add_key(amxc_llist_t, &args, "ipv4_addresses", NULL);
    ipv6 = amxc_var_add_key(amxc_llist_t, &args, "ipv6_addresses", NULL);

    amxd_object_for_each(instance, it, templ) {
        amxd_object_t* ipaddress_obj = amxc_container_of(it, amxd_object_t, it);
        host_addr_t* host_addr = NULL;
        const char* ip_address = object_da_string(ipaddress_obj, "IPAddress");
        int family = AF_INET_INVALID;

        if(ipaddress_obj->priv == NULL) { // maybe amx called Host-added event before IPAddress-added event
            host_addr = new_host_addr(ipaddress_obj);
            if(host_addr == NULL) {
                continue;
            }
        } else {
            host_addr = (host_addr_t*) ipaddress_obj->priv;
        }

        family = ipat_text_family(ip_address);
        if(family == AF_INET_INVALID) {
            SAH_TRACEZ_INFO(ME, "skip bad ip address '%s'", ip_address);
            host_addr->is_enabled = false;
            continue;
        }

        amxc_var_add(cstring_t, family == AF_INET ? ipv4 : ipv6, ip_address);
        host_addr->is_enabled = true;
        count++;
    }

    when_false_trace(count > 0, exit, INFO, "don't add %s because it has no addresses", name);
    when_failed_trace(add_host(&args, &ret), exit, ERROR, "Failed to add %s", name);

    SAH_TRACEZ_INFO(ME, "add %s", name);

    rv = true;

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv;
}

static bool host_remove_host_address(const char* interface, const char* name, const char* ip_address, int family) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* ipv4 = NULL;
    amxc_var_t* ipv6 = NULL;
    bool rv = false;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    amxc_var_add_key(cstring_t, &args, "interface", interface);
    ipv4 = amxc_var_add_key(amxc_llist_t, &args, "ipv4_addresses", NULL);
    ipv6 = amxc_var_add_key(amxc_llist_t, &args, "ipv6_addresses", NULL);
    amxc_var_add(cstring_t, family == AF_INET ? ipv4 : ipv6, ip_address);

    when_failed_trace(remove_host_address(&args, &ret), exit, ERROR, "Failed to remove address of %s", name);

    SAH_TRACEZ_INFO(ME, "remove %s from %s", ip_address, name);
    rv = true;

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv;
}

static bool host_add_host_address(const char* interface, const char* name, const char* ip_address, int family) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* ipv4 = NULL;
    amxc_var_t* ipv6 = NULL;
    bool rv = false;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    amxc_var_add_key(cstring_t, &args, "interface", interface);
    ipv4 = amxc_var_add_key(amxc_llist_t, &args, "ipv4_addresses", NULL);
    ipv6 = amxc_var_add_key(amxc_llist_t, &args, "ipv6_addresses", NULL);
    amxc_var_add(cstring_t, family == AF_INET ? ipv4 : ipv6, ip_address);

    when_failed_trace(add_host(&args, &ret), exit, ERROR, "Failed to add %s", name);

    SAH_TRACEZ_INFO(ME, "add %s for %s", ip_address, name);

    rv = true;

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv;
}

static void host_disable(host_t* host, bool cleanup) {
    when_null(host, exit);
    if(host->is_enabled) {
        const char* hostname = host->old_hostname;
        if(STRING_EMPTY(hostname)) {
            hostname = object_da_string(host->obj, "Name");
        }
        host_remove_host(host->netdevname, hostname);
        host->is_enabled = false;
    }
    if(cleanup) {
        CLOSE_QUERY(host->query_getaddrs);
        FREE(host->netdevname);
    }
    FREE(host->old_hostname);
exit:
    return;
}

static bool host_enable(host_t* host) {
    const char* hostname = NULL;

    when_null(host, exit);
    when_true_trace(host->is_enabled, exit, INFO, "%s is already enabled", host->name);

    if(STRING_EMPTY(host->netdevname)) {
        SAH_TRACEZ_INFO(ME, "%s is waiting for query result", host->name);
        host->is_enabled = false; // for sanity?
        goto exit;
    }

    hostname = object_da_string(host->obj, "Name");
    host->is_enabled = host_add_host(host->netdevname, hostname, host->obj);

    // no need to loop IPAddress instances because host_add_host also adds the ip addresses

exit:
    return host->is_enabled;
}

static bool host_can_enable(host_t* host) {
    bool can_enable = false;
    const char* name = NULL;
    const char* iface = NULL;

    when_null(host, exit);
    when_true(!amxd_object_get_value(bool, host->obj, "Enable", NULL), exit);

    name = object_da_string(host->obj, "Name");
    when_str_empty_trace(name, exit, ERROR, "%s is misconfigured because Name is empty", host->name);

    iface = object_da_string(host->obj, "Interface");
    when_str_empty_trace(iface, exit, ERROR, "%s is misconfigured because Interface is empty", host->name);

    can_enable = true;
exit:
    return can_enable;
}

static void host_add_ipaddr(host_addr_t* host_addr) {
    const char* ipaddr = NULL;
    const char* hostname = NULL;
    int ipaddr_family = AF_INET_INVALID;
    host_t* host = NULL;
    amxd_object_t* host_obj = NULL;

    when_null(host_addr, exit);
    when_true_trace(host_addr->is_enabled, exit, INFO, "%s is already enabled", host_addr->name);

    host_obj = amxd_object_findf(host_addr->obj, ".^.^");
    when_null_trace(host_obj, exit, ERROR, "Host object not found");
    when_null_trace(host_obj->priv, exit, ERROR, "Host object has no priv data");
    host = (host_t*) host_obj->priv;

    if(!host->is_enabled) { // maybe host was added without IPAddress instances
        when_false_trace(host_can_enable(host), exit, INFO, "%s is not added because host is disabled", host_addr->name);
        host_enable(host);
        goto exit;
    }

    hostname = object_da_string(host->obj, "Name");
    when_str_empty_trace(hostname, exit, ERROR, "%s is not added because Name is empty", host_addr->name);

    ipaddr = object_da_string(host_addr->obj, "IPAddress");
    when_str_empty_trace(ipaddr, exit, INFO, "%s is not added because IPAddress is empty", host_addr->name);

    ipaddr_family = ipat_text_family(ipaddr);
    when_false_trace(ipaddr_family != AF_INET_INVALID, exit, INFO, "%s is not added because IPAddress is bad", host_addr->name);

    host_addr->is_enabled = host_add_host_address(host->netdevname, hostname, ipaddr, ipaddr_family);
exit:
    return;
}

static void host_remove_ipaddr(host_addr_t* host_addr) {
    const char* hostname = NULL;
    const char* ipaddr = NULL;
    int ipaddr_family = AF_INET_INVALID;
    host_t* host = NULL;
    amxd_object_t* host_obj = NULL;

    when_null(host_addr, exit);
    when_false_trace(host_addr->is_enabled, exit, INFO, "%s is already disabled", host_addr->name);

    host_obj = amxd_object_findf(host_addr->obj, ".^.^");
    when_null_trace(host_obj, exit, ERROR, "Host object not found");
    when_null_trace(host_obj->priv, exit, ERROR, "Host object has no priv data");
    host = (host_t*) host_obj->priv;
    when_false_trace(host->is_enabled, exit, INFO, "%s is not removed because host is disabled", host_addr->name);

    ipaddr = host_addr->old_ipaddr;
    if(ipaddr == NULL) {
        ipaddr = object_da_string(host_addr->obj, "IPAddress");
    }
    when_null(ipaddr, exit);

    hostname = object_da_string(host->obj, "Name");
    when_str_empty_trace(hostname, exit, ERROR, "%s is not removed because Name is empty", host_addr->name);

    ipaddr_family = ipat_text_family(ipaddr);
    when_false_trace(ipaddr_family != AF_INET_INVALID, exit, INFO, "%s is not removed because IPAddress is bad", host_addr->name);

    host_remove_host_address(host->netdevname, hostname, ipaddr, ipaddr_family);
exit:
    host_addr->is_enabled = false;
    FREE(host_addr->old_ipaddr);
}

static void update_ipaddress_objects(host_t* host, const amxc_var_t* result) {
    amxd_trans_t trans;
    amxc_var_t addresses_to_add;

    amxd_trans_init(&trans);
    amxc_var_init(&addresses_to_add);
    amxc_var_set_type(&addresses_to_add, AMXC_VAR_ID_HTABLE);

    amxc_var_for_each(var, result) {
        amxc_var_add_new_key(&addresses_to_add, GET_CHAR(var, "Address"));
    }

    // try to reuse existing objects
    amxd_object_for_each(instance, it, amxd_object_findf(host->obj, ".IPAddress")) {
        amxd_object_t* ipaddr_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        const char* ipaddr = NULL;
        amxc_var_t* addresses_var = NULL;

        ipaddr = object_da_string(ipaddr_obj, "IPAddress");

        addresses_var = GET_ARG(&addresses_to_add, ipaddr);
        if(addresses_var == NULL) {
            amxd_trans_select_object(&trans, ipaddr_obj);
            amxd_trans_select_pathf(&trans, ".^");
            amxd_trans_del_inst(&trans, amxd_object_get_index(ipaddr_obj), NULL);
            if(ipaddr_obj->priv != NULL) {
                host_addr_t* host_addr = (host_addr_t*) ipaddr_obj->priv;
                host_addr->is_enabled = false; // the destroy handler should not let the backend remove the host address
            }
            continue;
        }

        amxc_var_delete(&addresses_var); // remove from list so no new object is created
    }

    amxc_var_for_each(var, &addresses_to_add) {
        amxd_trans_select_object(&trans, host->obj);
        amxd_trans_select_pathf(&trans, ".IPAddress");
        amxd_trans_add_inst(&trans, 0, NULL);
        amxd_trans_set_value(cstring_t, &trans, "IPAddress", amxc_var_key(var));
    }

    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }

    amxd_trans_clean(&trans);
    amxc_var_clean(&addresses_to_add);
}

static void host_getaddrs_changed(UNUSED const char* sig_name, const amxc_var_t* result, void* priv) {
    host_t* host = (host_t*) priv;
    when_null(host, exit);
    const char* netdevname = GETP_CHAR(result, "0.NetDevName");
    char* old_netdevname = NULL;

    if(netdevname == NULL) {
        netdevname = "";
    }

    // hack: If the netdevname didn't change then some other value of getAddrs query changed.
    // If new ip addresses are added then Unbound has to restart to listen on them.
    if((host->netdevname == NULL) || (strcmp(host->netdevname, netdevname) != 0)) {
        old_netdevname = host->netdevname;
        host->netdevname = strdup(netdevname);
    }

    SAH_TRACEZ_INFO(ME, "%s getaddrs query result changed", host->name);

    if(amxd_object_get_value(bool, host->obj, "QueryAddresses", NULL)) {
        update_ipaddress_objects(host, result);
    }

    if(host->query_getaddrs != NULL) {
        if(old_netdevname != NULL) {
            char* tmp = host->netdevname;
            host->netdevname = old_netdevname;
            host_disable(host, false);
            host->netdevname = tmp;
        } else {
            host_disable(host, false);
        }
        host_enable(host);
    } // else called from openQuery
    free(old_netdevname);

exit:
    return;
}

static bool host_iface_changed(host_t* host) {
    bool rv = false;
    const char* iface = NULL;

    when_null(host, exit);
    when_false_status(host->query_getaddrs == NULL, exit, rv = true);

    iface = object_da_string(host->obj, "Interface");
    when_str_empty_trace(iface, exit, ERROR, "%s misconfigured because Interface is empty", host->name);

    host->query_getaddrs = query_getaddrs(iface, host_getaddrs_changed, host);
    when_null_trace(host->query_getaddrs, exit, ERROR, "Failed to open query");

    rv = true;
exit:
    return rv;
}

static void host_changed(amxd_object_t* obj) {
    host_t* host = NULL;

    when_null(obj, exit);
    if(obj->priv == NULL) {
        const char* origin = NULL;

        host = (host_t*) calloc(1, sizeof(host_t));
        when_null(host, exit);

        host->obj = obj;
        host->name = amxd_object_get_name(obj, AMXD_OBJECT_NAMED);
        obj->priv = host;

        origin = object_da_string(obj, "Origin");
        if((origin == NULL) || (strcmp(origin, "Static") == 0)) {
            host->is_static = true;
        } else {
            unset_object_persistency(obj);
        }
    } else {
        host = (host_t*) obj->priv;
        host_disable(host, false); // close queries only if Interface changed (not if Name changed)
    }

    if(!host_can_enable(host)) {
        host_disable(host, true); // close queries
        goto exit;
    }
    when_false(host_iface_changed(host), exit);

    host_enable(host);

exit:
    return;
}

void _host_added(UNUSED const char* const sig_name,
                 const amxc_var_t* const data,
                 UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(get_dm(), data);
    amxd_object_t* obj = amxd_object_get_instance(templ, NULL, GET_UINT32(data, "index"));
    when_null(obj, exit);
    host_changed(obj);
exit:
    return;
}

amxd_status_t _host_removed(amxd_object_t* obj,
                            UNUSED amxd_param_t* param,
                            amxd_action_t reason,
                            UNUSED const amxc_var_t* const args,
                            UNUSED amxc_var_t* const retval,
                            UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    host_t* host = NULL;

    when_null(obj, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    host = (host_t*) obj->priv;
    when_null_status(host, exit, status = amxd_status_ok);

    SAH_TRACEZ_INFO(ME, "Object[%s] is about to be destroyed", host->name);

    host_disable(host, true);
    FREE(obj->priv);

    status = amxd_status_ok;
exit:
    return status;
}

void _host_changed(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(get_dm(), data);
    host_t* host = NULL;
    const char* old_hostname = NULL;

    when_null(obj, exit);
    when_null(obj->priv, exit);
    host = (host_t*) obj->priv;

    if(host->old_hostname != NULL) {
        SAH_TRACEZ_ERROR(ME, "old_hostname should be NULL");
        FREE(host->old_hostname);
    }

    old_hostname = GETP_CHAR(data, "parameters.Name.from");
    if(!STRING_EMPTY(old_hostname)) {
        host->old_hostname = strdup(old_hostname);
    }

    if(GETP_ARG(data, "parameters.Interface.to")) {
        host_disable(host, true);
    }

    host_changed(obj);
exit:
    return;
}

void _host_address_added(UNUSED const char* const sig_name,
                         const amxc_var_t* const data,
                         UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(get_dm(), data);
    amxd_object_t* ipaddress_obj = amxd_object_get_instance(templ, NULL, GET_UINT32(data, "index"));
    amxd_object_t* host_obj = NULL;
    host_addr_t* host_addr = NULL;
    host_t* host = NULL;
    bool address_is_dynamic = false;

    when_null(ipaddress_obj, exit);
    host_obj = amxd_object_findf(ipaddress_obj, ".^.^");
    host = (host_t*) host_obj->priv;
    when_null_trace(host, exit, ERROR, "Host %s priv data is empty", amxd_object_get_name(host_obj, AMXD_OBJECT_NAMED));
    address_is_dynamic = amxd_object_get_value(bool, host_obj, "QueryAddresses", NULL);

    if((address_is_dynamic || !host->is_static)) {
        unset_object_persistency(ipaddress_obj);
    }

    when_true_trace(address_is_dynamic, exit, INFO, "%s change ignored", amxd_object_get_name(ipaddress_obj, AMXD_OBJECT_NAMED));

    if(ipaddress_obj->priv == NULL) {
        host_addr = new_host_addr(ipaddress_obj);
        when_null(host_addr, exit);
    } else {
        host_addr = (host_addr_t*) ipaddress_obj->priv; // maybe amx handled Host-added event before IPAddress-added event
    }

    host_add_ipaddr(host_addr);
exit:
    return;
}

amxd_status_t _host_address_removed(amxd_object_t* obj,
                                    UNUSED amxd_param_t* param,
                                    amxd_action_t reason,
                                    UNUSED const amxc_var_t* const args,
                                    UNUSED amxc_var_t* const retval,
                                    UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    host_addr_t* host_addr = NULL;

    when_null(obj, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    host_addr = (host_addr_t*) obj->priv;
    when_null_status(host_addr, exit, status = amxd_status_ok);

    SAH_TRACEZ_INFO(ME, "Object[%s] is about to be destroyed", host_addr->name);

    host_remove_ipaddr(host_addr);
    FREE(obj->priv);

    status = amxd_status_ok;
exit:
    return status;
}

void _host_address_changed(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    amxd_object_t* ipaddress_obj = amxd_dm_signal_get_object(get_dm(), data);
    amxd_object_t* host_obj = NULL;
    host_addr_t* host_addr = NULL;
    const char* from = NULL;

    when_null(ipaddress_obj, exit);

    host_obj = amxd_object_findf(ipaddress_obj, ".^.^");
    when_true_trace(amxd_object_get_value(bool, host_obj, "QueryAddresses", NULL), exit, INFO, "%s change ignored", amxd_object_get_name(ipaddress_obj, AMXD_OBJECT_NAMED));

    when_null_trace(ipaddress_obj->priv, exit, ERROR, "%s priv data is NULL", amxd_object_get_name(ipaddress_obj, AMXD_OBJECT_NAMED));
    host_addr = (host_addr_t*) ipaddress_obj->priv;

    from = GETP_CHAR(data, "parameters.IPAddress.from");
    if(!STRING_EMPTY(from)) {
        host_addr->old_ipaddr = strdup(from);
        when_null(host_addr->old_ipaddr, exit);
        host_remove_ipaddr(host_addr);
    }
    host_add_ipaddr(host_addr);
exit:
    return;
}

void hosts_start(void) {
    amxd_object_for_each(instance, it, amxd_dm_findf(get_dm(), "DNS.%sHost.", get_prefix())) {
        amxd_object_t* obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        host_changed(obj);
    }
}
