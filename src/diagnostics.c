/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_transaction.h>
#include <debug/sahtrace_macros.h>

#include "dm_common.h"
#include "dm_diagnostics.h"
#include "nslookup.h"

#define ME "diagnostics"

static void diagnostic_reset(const char* new_state) {
    amxd_trans_t trans;
    amxd_status_t rv;
    amxd_object_t* diagnostic_obj = NULL;
    amxd_object_t* result_templ = NULL;

    SAH_TRACEZ_INFO(ME, "Reset diagnostic object because %s", new_state);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    diagnostic_obj = amxd_dm_findf(get_dm(), "DNS.Diagnostics.NSLookupDiagnostics");
    when_null_trace(diagnostic_obj, exit, ERROR, "Diagnostic root datamodel not found.");

    amxd_trans_select_object(&trans, diagnostic_obj);
    amxd_trans_set_value(cstring_t, &trans, "DiagnosticsState", new_state);
    amxd_trans_set_value(uint32_t, &trans, "SuccessCount", 0);
    result_templ = amxd_object_findf(diagnostic_obj, ".Result.");
    amxd_trans_select_object(&trans, result_templ);
    amxd_object_for_each(instance, it, result_templ) {
        amxd_object_t* obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        amxd_trans_del_inst(&trans, amxd_object_get_index(obj), NULL);
    }
    rv = amxd_trans_apply(&trans, get_dm());
    when_failed_trace(rv, exit, ERROR, "Transaction failed: %d", rv);

exit:
    amxd_trans_clean(&trans);
}

static void diagnostic_result_cb(const char* status, const char* hostname, amxc_var_t* ip_addresses, const char* server, int time, bool authoritative, int repetition_left) {
    amxd_status_t rv = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_object_t* templ = NULL;
    amxd_object_t* root_diag = NULL;
    uint32_t instance_count = 0;
    uint32_t success_count = 0;

    amxd_trans_init(&trans);

    when_null_trace(status, exit, ERROR, "Diagnostic status is NULL");
    when_null_trace(hostname, exit, ERROR, "Diagnostic hostname is NULL");
    when_null_trace(ip_addresses, exit, ERROR, "Diagnostic ip addresses is NULL");
    when_null_trace(server, exit, ERROR, "Diagnostic server is NULL");

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    templ = amxd_dm_findf(get_dm(), "DNS.Diagnostics.NSLookupDiagnostics.Result");
    when_null_trace(templ, exit, ERROR, "Diagnostic result object not found.");

    root_diag = amxd_object_get_parent(templ);
    when_null_trace(root_diag, exit, ERROR, "Cannot retrieve root Diagnostic datamodel.");

    instance_count = amxd_object_get_value(uint32_t, root_diag, "ResultNumberOfEntries", NULL) + 1;
    success_count = amxd_object_get_value(uint32_t, root_diag, "SuccessCount", NULL);

    amxd_trans_select_object(&trans, templ);

    amxd_trans_add_inst(&trans, instance_count, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Status", status);
    amxd_trans_set_value(cstring_t, &trans, "AnswerType", strcmp_safe(status, "Success") == 0 ? (authoritative ? "Authoritative" : "NonAuthoritative" ) : "None");
    amxd_trans_set_value(cstring_t, &trans, "HostNameReturned", hostname);

    amxc_var_cast(ip_addresses, AMXC_VAR_ID_CSTRING);
    amxd_trans_set_value(cstring_t, &trans, "IPAddresses", GET_CHAR(ip_addresses, NULL));
    amxd_trans_set_value(cstring_t, &trans, "DNSServerIP", server);
    amxd_trans_set_value(uint32_t, &trans, "ResponseTime", time);

    amxd_trans_select_object(&trans, root_diag);

    if(strcmp_safe(status, "Success") == 0) {
        success_count++;
        amxd_trans_set_value(uint32_t, &trans, "SuccessCount", success_count);
    }

    if(repetition_left == 0) {
        amxd_trans_set_value(cstring_t, &trans, "DiagnosticsState", success_count > 0 ? "Complete" : "Error_Other");
    }

    rv = amxd_trans_apply(&trans, get_dm());
    when_failed_trace(rv, exit, ERROR, "Transaction failed: %d", rv);

exit:
    amxd_trans_clean(&trans);
    if(rv != amxd_status_ok) {
        diagnostic_reset("Error_Internal");
    }
}

static void diagnostic_start(void) {
    amxc_var_t* interface_name = NULL;
    nslookup_status_t rv = NSLOOKUP_OTHER;
    amxd_object_t* diagnostic_obj = amxd_dm_findf(get_dm(), "DNS.Diagnostics.NSLookupDiagnostics");
    const char* interface = object_da_string(diagnostic_obj, "Interface");
    const char* hostname = object_da_string(diagnostic_obj, "HostName");
    const char* server = object_da_string(diagnostic_obj, "DNSServer");
    uint32_t timeout = amxd_object_get_value(uint32_t, diagnostic_obj, "Timeout", NULL);
    uint32_t repetition = amxd_object_get_value(uint32_t, diagnostic_obj, "NumberOfRepetitions", NULL);

    if(!STRING_EMPTY(interface)) {
        interface_name = netmodel_getFirstParameter(interface, "NetDevName", "netdev-up", netmodel_traverse_down);
        when_null_trace(interface_name, exit, ERROR, "Netmodel variant is NULL");
        interface = GET_CHAR(interface_name, NULL);
        when_str_empty_trace(interface, exit, ERROR, "Interface name is not found");
        SAH_TRACEZ_INFO(ME, "Netdev name from interface path : %s", interface);
    } else {
        SAH_TRACEZ_WARNING(ME, "Interface is empty : using routing policy to determine the appropriate interface");
    }

    rv = nslookup_start(interface, server, hostname, timeout, repetition, diagnostic_result_cb);
    when_failed_trace(rv, exit, ERROR, "Cannot start NS lookup test");

exit:
    if(rv == NSLOOKUP_SRVFAIL) {
        diagnostic_reset("Error_DNSServerNotResolved");
    } else if(rv != NSLOOKUP_OK) {
        diagnostic_reset("Error_Internal");
    }
    amxc_var_delete(&interface_name);
}

void _state_changed(UNUSED const char* const sig_name,
                    const amxc_var_t* const data,
                    UNUSED void* const priv) {
    const char* state = GETP_CHAR(data, "parameters.DiagnosticsState.to");

    if(strcmp_safe(state, "Requested") == 0) {
        diagnostic_reset("Requested");
        diagnostic_start();

    } else if(strcmp_safe(state, "None") == 0) {
        diagnostic_reset("None");
    }
}

void _option_changed(UNUSED const char* const sig_name,
                     UNUSED const amxc_var_t* const data,
                     UNUSED void* const priv) {
    diagnostic_reset("None");
}

amxd_status_t _NSLookupDiagnostics(UNUSED amxd_object_t* object,
                                   UNUSED amxd_function_t* func,
                                   amxc_var_t* args,
                                   UNUSED amxc_var_t* ret) {
    amxd_trans_t trans;
    amxd_status_t rv;
    const char* interface = GET_CHAR(args, "Interface");
    const char* hostname = GET_CHAR(args, "HostName");
    const char* server = GET_CHAR(args, "DNSServer");
    uint32_t timeout = GET_UINT32(args, "Timeout");
    uint32_t repetition = GET_UINT32(args, "NumberOfRepetitions");

    if(interface == NULL) {
        interface = "";
    }

    if(server == NULL) {
        server = "";
    }

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "DNS.Diagnostics.NSLookupDiagnostics");
    amxd_trans_set_value(cstring_t, &trans, "DiagnosticsState", "Requested");
    amxd_trans_set_value(cstring_t, &trans, "Interface", interface);
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", server);
    amxd_trans_set_value(cstring_t, &trans, "HostName", hostname);
    amxd_trans_set_value(uint32_t, &trans, "NumberOfRepetitions", repetition);
    amxd_trans_set_value(uint32_t, &trans, "Timeout", timeout);
    rv = amxd_trans_apply(&trans, get_dm());

    amxd_trans_clean(&trans);
    return rv;
}
