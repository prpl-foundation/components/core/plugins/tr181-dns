/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tr181-dns.h"
#include "dm_common.h"
#include "dm_client.h"

#include <amxd/amxd_transaction.h>
#include <debug/sahtrace_macros.h>

#define ME "client"
#define RESOLV_CONF "/etc/resolv.conf"
#define TMP_RESOLV_CONF "/tmp/resolv.conf"

static amxd_object_t* server_template(void) {
    return amxd_dm_findf(get_dm(), "DNS.Client.Server.");
}

void client_start(void) {
    amxd_object_t* server = server_template();
    parent_enable_changed(amxd_object_get_parent(server));
}

typedef struct {
    FILE* file;
    uint32_t count;
    amxd_trans_t trans;
} ccc_t;

static int client_config_status_disabled_cb(UNUSED amxd_object_t* object, amxd_object_t* mobject, void* priv) {
    amxd_trans_t* trans = (amxd_trans_t*) priv;

    amxd_trans_select_object(trans, mobject);
    amxd_trans_set_value(cstring_t, trans, "Status", "Disabled");
    return 0;
}

static int client_config_cb(UNUSED amxd_object_t* object, amxd_object_t* mobject, void* priv) {
    ccc_t* ctx = (ccc_t*) priv;
    const char* nameserver = object_da_string(mobject, "DNSServer");

    fprintf(ctx->file, "nameserver %s\n", nameserver);

    amxd_trans_select_object(&ctx->trans, mobject);
    amxd_trans_set_value(cstring_t, &ctx->trans, "Status", "Enabled");

    ctx->count++;

    return 0;
}

static bool client_config_changed(void) {
    ccc_t ctx;
    amxd_status_t trans_rv = amxd_status_unknown_error;
    amxd_object_t* templ = server_template();
    amxc_string_t expr;
    amxd_dm_t* dm = get_dm();
    bool rv = false;

    memset(&ctx, 0, sizeof(ccc_t));

    amxd_trans_init(&ctx.trans);
    amxd_trans_set_attr(&ctx.trans, amxd_tattr_change_ro, true);

    when_null(dm, exit); // e.g. because the destroy action handler calls this function which happens after entry-point received stop

    unlink(TMP_RESOLV_CONF);

    ctx.file = fopen(TMP_RESOLV_CONF, "w+");
    when_null_trace(ctx.file, exit, ERROR, "Failed to open file[%s]", TMP_RESOLV_CONF);

    amxc_string_init(&expr, 0);
    amxc_string_set(&expr, "[Enable==true && DNSServer!=''].");
    amxd_object_for_all(templ, amxc_string_get(&expr, 0), client_config_cb, &ctx);
    amxc_string_clean(&expr);

    fflush(ctx.file);
    fclose(ctx.file);
    ctx.file = NULL;

    rename(TMP_RESOLV_CONF, RESOLV_CONF);

    SAH_TRACEZ_INFO(ME, "Updated file[%s] with %d nameserver(s)", RESOLV_CONF, ctx.count);

    amxd_object_for_all(templ, "[Enable==false].", client_config_status_disabled_cb, &ctx.trans);
    trans_rv = amxd_trans_apply(&ctx.trans, dm);
    when_failed_trace(trans_rv, exit, ERROR, "Transaction failed: %d", trans_rv);

    rv = true;
exit:
    amxd_trans_clean(&ctx.trans);
    return rv;
}

bool client_enable(UNUSED common_t* core) {
    return client_config_changed();
}

void client_disable(UNUSED common_t* core, UNUSED const char* dnsserver) {
    client_config_changed();
}
