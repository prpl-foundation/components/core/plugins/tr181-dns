/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ipat/ipat.h>

#include "tr181-dns.h"
#include "dm_common.h"
#include "dm_relay.h"
#include "dhcpoption.h"
#include "netdevname.h"

#include <amxd/amxd_transaction.h>
#include <debug/sahtrace_macros.h>

#define ME "dns"

#define DNSSERVER_DEFAULT ""

static bool core_can_enable(common_t* core);
static bool core_iface_changed(common_t* core);

static core_type_t set_core_type(common_t* core) {
    amxd_object_t* unknown_obj = NULL;
    core_type_t core_type = CORE_DONT_CARE;

    unknown_obj = amxd_object_findf(core->obj, ".^.^");
    if(unknown_obj == amxd_dm_findf(get_dm(), "DNS.Client.")) {
        core_type = CORE_IS_CLIENT;
        goto exit;
    }

    if(unknown_obj == amxd_dm_findf(get_dm(), "DNS.Relay.")) {
        core_type = CORE_IS_RELAY;
        goto exit;
    }

    unknown_obj = amxd_object_findf(core->obj, ".^.^.^");
    if(unknown_obj == amxd_dm_findf(get_dm(), "DNS.%sForwardZone.", get_prefix())) {
        core_type = CORE_IS_FWZONE;
        goto exit;
    }

    SAH_TRACEZ_ERROR(ME, "Failed to identify object %s", core->name);

exit:
    core->type = core_type;
    return core_type;
}

static dns_type_t set_core_origin(common_t* core) {
    dns_type_t origin = DNS_TYPE_ERROR;
    const char* type = NULL;

    type = object_da_string(core->obj, "Type");
    when_str_empty_trace(type, exit, ERROR, "Type of %s is empty", core->name);

    if(strcmp(type, "Static") == 0) {
        origin = DNS_STATIC;
    } else if(strcmp(type, "DHCPv4") == 0) {
        origin = DNS_DHCPv4;
    } else if(strcmp(type, "DHCPv6") == 0) {
        origin = DNS_DHCPv6;
    } else if(strcmp(type, "RouterAdvertisement") == 0) {
        origin = DNS_RA;
    } else if(strcmp(type, "IPCP") == 0) {
        origin = DNS_IPCP;
    } else if(strcmp(type, "3GPP-NAS") == 0) {
        origin = DNS_3GPP_NAS;
    } else {
        SAH_TRACEZ_ERROR(ME, "%s has unsupported type: '%s'", core->name, type);
    }

exit:
    core->origin = origin;
    return origin;
}

static bool core_set_dnsserver(common_t* core, const char* server) {
    amxd_trans_t trans;
    bool rv = false;

    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, core->obj);
    amxd_trans_set_value(cstring_t, &trans, "DNSServer", server);
    when_failed_trace(amxd_trans_apply(&trans, get_dm()), exit, INFO, "Transaction failed");

    rv = true;
exit:
    amxd_trans_clean(&trans);
    return rv;
}

static void core_close_queries(common_t* core) {
    if(core->query_netdevname != NULL) {
        FREE(core->netdevname);
        CLOSE_QUERY(core->query_netdevname);
    }

    if(core->query_dnsserver != NULL) {
        CLOSE_QUERY(core->query_dnsserver);
        core_set_dnsserver(core, DNSSERVER_DEFAULT);
    } // else don't set DNSServer because type might be Static
}

static bool core_enable(common_t* core) {
    when_true_trace(core->is_enabled, exit, INFO, "%s is already enabled", core->name);

    if(core->type == CORE_IS_RELAY) {
        if(relay_is_blocked(core)) {
            core_block(core);
        } else {
            core_unblock(core, false);
        }
        when_true_trace(core->is_blocked, exit, INFO, "%s is blocked", core->name);
    } // else object it not blockable

    if(!core->is_validated) {
        core->is_validated = core_can_enable(core);
        when_false(core->is_validated, skip_status); // core_can_enable sets the status if it returned false
        when_false(core_iface_changed(core), exit);
    }

    if(!STRING_EMPTY(core->netdevname) && (object_da_string(core->obj, "DNSServer") != NULL)) {
        if(core->type == CORE_IS_RELAY) {
            core->is_enabled = relay_enable(core);
        } else if(core->type == CORE_IS_CLIENT) {
            core->is_enabled = client_enable(core);
        } else if(core->type == CORE_IS_FWZONE) {
            core->is_enabled = fwzone_enable(core);
        } else {
            SAH_TRACEZ_ERROR(ME, "Failed to enable %s", core->name);
            core->is_enabled = false; // for sanity?
        }
        if(core->is_enabled) {
            SAH_TRACEZ_INFO(ME, "enabled %s", core->name);
        }
    } else {
        SAH_TRACEZ_INFO(ME, "%s is waiting for query result", core->name);
        core->is_enabled = false; // for sanity?
    }

exit:
    set_status(core->obj, core->is_enabled ? ENABLED : ERROR);
skip_status:
    return core->is_enabled;
}

static void core_disable(common_t* core, bool cleanup) {
    if(core->is_enabled) {
        const char* dnsserver = core->old_dnsserver;
        if(dnsserver == NULL) {
            dnsserver = object_da_string(core->obj, "DNSServer");
        }
        if(core->type == CORE_IS_RELAY) {
            relay_disable(core, dnsserver);
        } else if(core->type == CORE_IS_CLIENT) {
            client_disable(core, dnsserver);
        } else if(core->type == CORE_IS_FWZONE) {
            fwzone_disable(core, dnsserver);
        } else {
            SAH_TRACEZ_ERROR(ME, "Failed to disable %s", core->name);
        }
        core->is_enabled = false;
    }
    if(cleanup) {
        core_close_queries(core);
    }
    FREE(core->old_dnsserver);
    set_status(core->obj, DISABLED);
}

static void core_netdevname_changed(UNUSED const char* sig_name, const amxc_var_t* result, void* priv) {
    common_t* core = (common_t*) priv;
    const char* netdevname = GET_CHAR(result, NULL);

    when_true(strcmp_safe(core->netdevname, netdevname) == 0, exit);

    SAH_TRACEZ_INFO(ME, "%s netdevname query result changed %s -> %s", core->name, core->netdevname, netdevname);

    free(core->netdevname);
    core->netdevname = netdevname != NULL ? strdup(netdevname) : NULL;

    if(core->query_netdevname != NULL) {
        core_disable(core, false);
        core_enable(core);
    } // else called from openQuery
exit:
    return;
}

static void core_dnsserver_changed(UNUSED const char* sig_name, const amxc_var_t* result, void* priv) {
    common_t* core = (common_t*) priv;
    amxd_object_t* dns_obj = NULL;
    amxc_var_t* servers = NULL;
    amxc_var_t server_list;
    const char* type = NULL;
    const char* rel_path = NULL;
    const char* dnsserver = NULL;

    amxc_var_init(&server_list);
    servers = GET_ARG(result, "Servers"); // In case of RouterAdvertisement
    if(servers != NULL) {
        result = servers;
    }

    SAH_TRACEZ_INFO(ME, "%s dnsserver query result changed", core->name);

    if(core->type == CORE_IS_FWZONE) {
        amxc_var_convert(&server_list, result, AMXC_VAR_ID_CSV_STRING);
        dnsserver = GET_CHAR(&server_list, NULL);
        goto update_dnsserver;
    } else if(core->type == CORE_IS_RELAY) {
        rel_path = "Relay.Forwarding.";
    } else {
        rel_path = "Client.Server.";
    }

    amxc_var_convert(&server_list, result, AMXC_VAR_ID_LIST);
    type = object_da_string(core->obj, "Type");

    dns_obj = amxd_dm_findf(get_dm(), "DNS.");
    when_null_trace(dns_obj, exit, ERROR, "Could not find root object");

    amxc_var_for_each(var, &server_list) {
        amxd_object_t* object = NULL;
        char* object_path = NULL;

        dnsserver = GET_CHAR(var, NULL);
        if(STRING_EMPTY(dnsserver)) {
            continue;
        }

        object = amxd_object_findf(dns_obj, "%s.[Type=='%s' && DNSServer=='%s'].", rel_path, type, dnsserver);
        if(object == NULL) {
            break;
        }

        when_true_trace(object == core->obj, exit, INFO, "%s already uses dnsserver '%s'", core->name, dnsserver);

        object_path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
        SAH_TRACEZ_INFO(ME, "dnsserver %s already in use by %s", dnsserver, object_path);
        dnsserver = NULL;
        free(object_path);
    }

update_dnsserver:
    if(core->is_enabled) {
        core->old_dnsserver = amxd_object_get_value(cstring_t, core->obj, "DNSServer", NULL); // store to be able to disable
        SAH_TRACEZ_INFO(ME, "dnsserver to be removed is '%s'", core->old_dnsserver);
    }

    if(dnsserver == NULL) {
        dnsserver = DNSSERVER_DEFAULT;
    }

    core_set_dnsserver(core, dnsserver);

    SAH_TRACEZ_INFO(ME, "%s uses dnsserver '%s'", core->name, dnsserver);

    if(core->query_dnsserver != NULL) {
        core_disable(core, false);
        core_enable(core);
    } // else called from openQuery
exit:
    amxc_var_clean(&server_list);
}

static bool core_can_enable(common_t* core) {
    bool can_enable = false;
    const char* iface = NULL;

    if(!amxd_object_get_value(bool, core->obj, "Enable", NULL)) {
        set_status(core->obj, DISABLED);
        goto exit;
    } else {
        if(!amxd_object_get_value(bool, amxd_object_findf(core->obj, ".^.^"), "Enable", NULL)) {
            SAH_TRACEZ_ERROR(ME, "%s misconfigured because parent object is disabled", core->name);
            set_status(core->obj, ERROR);
            goto exit;
        }
    }

    iface = object_da_string(core->obj, "Interface");
    if(STRING_EMPTY(iface)) {
        SAH_TRACEZ_ERROR(ME, "%s misconfigured because Interface is empty", core->name);
        set_status(core->obj, ERROR);
        goto exit;
    }

    if(core->origin == DNS_STATIC) {
        const char* ipaddr = NULL;
        int ipaddr_family = AF_INET_INVALID;

        ipaddr = object_da_string(core->obj, "DNSServer");
        when_str_empty_trace(ipaddr, exit, ERROR, "%s misconfigured because DSNServer is empty", core->name);

        ipaddr_family = ipat_text_family(ipaddr);
        when_false_trace(ipaddr_family, exit, ERROR, "%s is misconfigured because bad value of DNSServer", core->name);
    }

    can_enable = true;
exit:
    return can_enable;
}

static bool core_iface_changed(common_t* core) {
    bool rv = false;
    const char* iface = NULL;

    if(core->query_netdevname != NULL) {
        SAH_TRACEZ_INFO(ME, "%s already has open queries", core->name);
        rv = true;
        goto exit;
    }

    iface = object_da_string(core->obj, "Interface");
    when_str_empty_trace(iface, exit, ERROR, "%s misconfigured because Interface is empty", core->name);

    core->query_netdevname = query_netdevname(iface, core_netdevname_changed, core);
    when_null_trace(core->query_netdevname, exit, ERROR, "Failed to open query");

    if(core->origin != DNS_STATIC) {
        core_set_dnsserver(core, DNSSERVER_DEFAULT);
        core->query_dnsserver = query_dnsservers(iface, core->origin, core_dnsserver_changed, core);
        when_null_trace(core->query_dnsserver, exit, ERROR, "Failed to open query");
    }

    rv = true;
exit:
    return rv;
}

void core_block(common_t* core) {
    core->is_blocked = true;
    core_disable(core, false); // sets status to Disabled
}

void core_unblock(common_t* core, bool enable) {
    core->is_blocked = false;
    if(enable) {
        core_enable(core);
    }
}

static bool core_changed(amxd_object_t* obj) {
    common_t* core = NULL;
    bool rv = false;

    if(obj->priv == NULL) {
        core = (common_t*) calloc(1, sizeof(common_t));
        when_null(core, exit);

        core->obj = obj;
        obj->priv = core;
        core->name = amxd_object_get_name(obj, AMXD_OBJECT_NAMED);
        if((set_core_type(core) == CORE_DONT_CARE) ||
           (set_core_origin(core) == DNS_TYPE_ERROR)) {
            FREE(obj->priv);
            goto exit;
        }
        if(core->origin != DNS_STATIC) {
            unset_param_persistency(amxd_object_get_param_def(core->obj, "DNSServer"));
        }
    } else {
        core = (common_t*) obj->priv;
        core_disable(core, true);
    }

    core->is_validated = false;
    when_false(core_enable(core), skip_status); // core_enable sets the status
    rv = true;

exit:
    if(!rv) {
        set_status(obj, ERROR);
    }
skip_status:
    return rv;
}

void _core_added(UNUSED const char* const sig_name,
                 const amxc_var_t* const data,
                 UNUSED void* const priv) {
    amxd_object_t* templ = amxd_dm_signal_get_object(get_dm(), data);
    amxd_object_t* obj = amxd_object_get_instance(templ, NULL, GET_UINT32(data, "index"));

    when_null_trace(obj, exit, INFO, "object is NULL");
    when_false(obj->priv == NULL, exit); // already handled

    core_changed(obj);
exit:
    return;
}

amxd_status_t _common_cleanup(amxd_object_t* obj,
                              UNUSED amxd_param_t* param,
                              amxd_action_t reason,
                              UNUSED const amxc_var_t* const args,
                              UNUSED amxc_var_t* const retval,
                              UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    common_t* c = NULL;

    when_null(obj, exit);
    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    c = (common_t*) obj->priv;
    when_null_status(c, exit, status = amxd_status_ok);

    SAH_TRACEZ_INFO(ME, "Object[%s] is about to be destroyed", c->name);

    core_disable(c, true);
    FREE(obj->priv);

    status = amxd_status_ok;
exit:
    return status;
}

void parent_enable_changed(amxd_object_t* parent_obj) { // is parent object DNS.Relay or DNS.Client or ... (not Server or Forwarding or ...)
    amxd_object_t* child = NULL;
    bool rv = false;

    child = amxd_object_findf(parent_obj, "%s", "Server");
    if(child == NULL) {
        child = amxd_object_findf(parent_obj, "%s", "Forwarding"); // same for DNS.Relay and DNS.ForwardZone
    }
    when_null_trace(child, exit, ERROR, "Failed to get child template");

    amxd_object_for_each(instance, it, child) {
        amxd_object_t* core_obj = amxc_container_of(it, amxd_object_t, it);
        core_changed(core_obj);
    }

    rv = true;
exit:
    if(rv) {
        set_status(parent_obj, amxd_object_get_value(bool, parent_obj, "Enable", NULL) ? ENABLED : DISABLED);
    } else {
        set_status(parent_obj, ERROR);
    }
}

void _enable_changed(UNUSED const char* const sig_name,
                     const amxc_var_t* const data,
                     UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(get_dm(), data);
    when_null(obj, exit);
    parent_enable_changed(obj);
exit:
    return;
}

static bool is_only_change(amxc_var_t* params, const char* key) {
    bool only_change = false;
    amxc_var_t* var = GET_ARG(params, key);
    const amxc_htable_t* hparams = amxc_var_constcast(amxc_htable_t, params);

    when_null(var, exit);
    amxc_var_take_it(var);
    only_change = amxc_htable_is_empty(hparams);
    amxc_var_set_key(params, key, var, AMXC_VAR_FLAG_DEFAULT);

exit:
    return only_change;
}

void _core_changed(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(get_dm(), data);
    common_t* core = NULL;
    const char* old_dnsserver = GETP_CHAR(data, "parameters.DNSServer.from");

    when_null(obj, exit);
    when_null(obj->priv, exit);
    core = (common_t*) obj->priv;

    if(is_only_change(GET_ARG(data, "parameters"), "DNSServer")) {
        when_true_trace(core->origin != DNS_STATIC, exit, INFO, "Ignore DNSServer change because Type!=Static");
    }

    if(core->old_dnsserver != NULL) {
        SAH_TRACEZ_ERROR(ME, "should not have old_dnsserver");
        FREE(core->old_dnsserver);
    }
    if(!STRING_EMPTY(old_dnsserver)) {
        if(core->is_enabled) {
            core->old_dnsserver = strdup(old_dnsserver);
            SAH_TRACEZ_INFO(ME, "dnsserver to be removed is '%s'", core->old_dnsserver);
        }
    }
    core_changed(obj);

exit:
    return;
}
