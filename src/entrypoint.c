/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/client.h>

#include "tr181-dns.h"
#include "dm_common.h"
#include "dm_relay.h"
#include "dm_client.h"
#include "dm_fwzone.h"
#include "dm_rebind.h"
#include "nslookup.h"

#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>

#define ME "dns"

typedef struct {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    struct {
        bool present;
        bool active;
        const char* soname;
    } modfailover;
} dns_t;

static dns_t dns;

amxd_dm_t* get_dm(void) {
    return dns.dm;
}

static amxo_parser_t* get_parser(void) {
    return dns.parser;
}

void set_failover_active(bool active) {
    dns.modfailover.active = active;
}

bool supports_failover(void) {
    if(!dns.modfailover.active) {
        SAH_TRACEZ_INFO(ME, "failover is not active");
    }
    return dns.modfailover.present && dns.modfailover.active;
}

const char* failover_shared_object_name(void) {
    return dns.modfailover.soname;
}

amxc_var_t* get_config(void) {
    amxc_var_t* config = NULL;
    if(dns.parser != NULL) {
        config = &(dns.parser->config);
    }
    return config;
}

const char* get_prefix(void) {
    amxc_var_t* var = amxo_parser_get_config(get_parser(), "prefix_");
    const char* prefix = GET_CHAR(var, NULL);
    return (prefix != NULL) ? prefix : "";
}

static bool load_module(const char* dir, const char* module, const char* controller) {
    const char* path = NULL;
    amxc_string_t path_str;
    int rv = -1;
    amxm_shared_object_t* shared_object = NULL;

    amxc_string_init(&path_str, 0);

    when_str_empty(module, exit);
    when_str_empty(dir, exit);

    amxc_string_setf(&path_str, "%s/%s.so", dir, module);
    path = amxc_string_get(&path_str, 0);

    SAH_TRACEZ_NOTICE(ME, "Load module[%s] for %s", path, controller);

    rv = amxm_so_open(&shared_object, controller, path);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to load module[%s]", path);
        goto exit;
    }

exit:
    amxc_string_clean(&path_str);
    return rv == 0;
}

static bool load_dns_module(amxd_object_t* obj) {
    const char* module = amxc_var_constcast(cstring_t, amxd_object_get_param_value(obj, "DNSController"));
    const char* dir = GETP_CHAR(get_config(), "modules.dns-directory");

    return load_module(dir, module, "dns");
}

static bool load_firewall_module(amxd_object_t* obj) {
    const char* module = amxc_var_constcast(cstring_t, amxd_object_get_param_value(obj, "FWController"));
    const char* dir = GETP_CHAR(get_config(), "modules.fw-directory");

    return load_module(dir, module, "fw");
}

static bool load_failover_module(void) {
    bool rv = true; // the failover module is optional

    if(NULL != amxm_get_module(NULL, "failover")) {
        amxc_var_t args;
        amxc_var_t ret;

        amxc_var_init(&args);
        amxc_var_init(&ret);

        dns.modfailover.present = true;
        dns.modfailover.soname = NULL; // self
        amxm_execute_function(dns.modfailover.soname, "failover", "is-failover-activated", &args, &ret);
        dns.modfailover.active = GET_BOOL(&ret, NULL);
        rv = true;
        SAH_TRACEZ_INFO(ME, "failover module imported (enable=%d)", dns.modfailover.active);

        amxc_var_clean(&args);
        amxc_var_clean(&ret);
    } else {
        // kept for backward compatibility
        amxc_var_t* var = get_config();
        const char* module = GETP_CHAR(var, "modules.failover-controller");
        const char* dir = GETP_CHAR(var, "modules.dns-directory");
        when_str_empty(module, exit);
        rv = load_module(dir, module, "failover");
        dns.modfailover.present = rv;
        dns.modfailover.active = rv;
        dns.modfailover.soname = "failover";
        SAH_TRACEZ_INFO(ME, "%sload amxm failover module", dns.modfailover.active ? "" : "failed to ");
    }

exit:
    return rv;
}

static bool load_modules(amxd_object_t* obj) {
    amxm_module_t* mod = NULL;
    bool rv = true;

    // methods that can be called from modules
    amxm_module_register(&mod, NULL, "core");
    amxm_module_add_function(mod, "update-forwarding-status", update_forwarding_status);
    amxm_module_add_function(mod, "server-failure", server_failure);
    amxm_module_add_function(mod, "set-server", core_set_server);
    amxm_module_add_function(mod, "add-server", core_add_server);
    amxm_module_add_function(mod, "remove-server", core_remove_server);
    amxm_module_add_function(mod, "failover-enable-changed", core_failover_enable_changed);

    rv &= load_dns_module(obj);
    rv &= load_firewall_module(obj);
    rv &= load_failover_module();

    return rv;
}

static int init(amxd_dm_t* dm, amxo_parser_t* parser) {
    memset(&dns, 0, sizeof(dns));
    dns.dm = dm;
    dns.parser = parser;

    if(!netmodel_initialize()) {
        SAH_TRACEZ_ERROR(ME, "Failed to initialize libnetmodel!");
        goto exit;
    }

    if(!nslookup_initialize()) {
        SAH_TRACEZ_ERROR(ME, "Failed to initialize c-ares library.");
        goto exit;
    }

exit:
    return 0;
}

static int cleanup(void) {
    amxm_close_all();
    netmodel_cleanup();
    nslookup_deinit();

    dns.dm = NULL;
    dns.parser = NULL;
    return 0;
}

int _tr181_dns_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = 0;
    switch(reason) {
    case AMXO_START:
        SAH_TRACEZ_NOTICE(ME, "entrypoint - start");
        retval = init(dm, parser);
        break;

    case AMXO_STOP:
        SAH_TRACEZ_NOTICE(ME, "entrypoint - stop");
        retval = cleanup();
        break;
    }

    return retval;
}

void _app_start(UNUSED const char* const sig_name,
                UNUSED const amxc_var_t* const data,
                UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "app-start");
    load_modules(amxd_dm_findf(get_dm(), "DNS"));

    hosts_start();
    client_start();
    relay_start();
    fwzone_start();
    rebind_start();
}