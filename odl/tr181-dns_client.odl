%define {
    select DNS {
        /**
         * Client properties for Domain Name Service (DNS). The DNS client resolves FQDN on behalf of device internal (client) applications.
         * @version 1.0
         */
        %persistent object Client {
            /**
             * Enables or disables the DNS client.
             * @version 1.0
             */
            %persistent bool Enable = true;

            /**
             * The status of the DNS client.
             * Enumeration of: Disabled, Enabled, Error.
             * @version 1.0
             */
            %read-only string Status = "Disabled" {
                on action validate call check_enum ["Disabled", "Enabled", "Error"];
            }

            /**
             * This table contains the DNS Server IP address to be used by the DHCP Client (it does not model a DNS Server). Entries are either automatically created as result of DHCP (v4 or v6), IPCP, 3GPP-NAS, or RA received DNS server information, or are statically configured by the ACS.
             * At most one entry in this table can exist with a given value for DNSServer, or with a given value for Alias.
             * @version 1.0
             */
            %persistent object Server[] {
                counted with ServerNumberOfEntries;
                on action destroy call common_cleanup;

                /**
                 * A non-volatile unique key used to reference this instance.
                 * @version 1.0
                 */
                %persistent %unique %key string Alias {
                    on action validate call check_maximum_length 64;
                }

                /**
                 * Enables or disables this entry.
                 * @version 1.0
                 */
                %persistent bool Enable = false;

                /**
                 * The status of this entry.
                 * Enumeration of: Disabled, Enabled, Error.
                 * @version 1.0
                 */
                %read-only string Status = "Disabled" {
                    on action validate call check_enum ["Disabled", "Enabled", "Error"];
                }

                /**
                 * DNS server IP addresses.
                 * @version 1.0
                 */
                %persistent string DNSServer {
                    on action validate call check_maximum_length 45;
                }

                /**
                 * The value MUST be the Path Name of a row in the IP.Interface table. If the referenced object is deleted, the parameter value MUST be set to an empty string. This parameter specifies the IP interface over which the DNS query is sent.
                 * @version 1.0
                 */
                %persistent string Interface {
                    on action validate call check_maximum_length 256;
                }

                /**
                 * Method used to assign the DNSServer address.
                 * Enumeration of: DHCPv4, DHCPv6, RouterAdvertisement, IPCP, 3GPP-NAS, Static
                 * @version 1.0
                 */
                %persistent %read-only string Type = "Static";
            }
        }
    }
}

%populate {
    on event "dm:instance-added" call core_added
        filter 'path matches "DNS\.Client\.Server\.$"';

    on event "dm:object-changed" call core_changed
        filter 'path matches "DNS\.Client\.Server\.[0-9]+.$" && (
                   contains("parameters.Enable") ||
                   contains("parameters.Interface") ||
                   contains("parameters.DNSServer"))
               ';

    on event "dm:object-changed" call enable_changed
        filter 'path matches "DNS\.Client\.$" && contains("parameters.Enable")';
}
