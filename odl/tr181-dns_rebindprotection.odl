%define {
    select DNS {
        %persistent object '${prefix_}RebindProtection' {
            /**
             * Enables or Disables protection against DNS rebind attacks
             * @version 1.0
             */
            %persistent bool Enable = true {
                userflags %usersetting;
            }

            /**
             * IP adress spaces that should not be protected against rebind attacks
             * @version 1.0
             */
            %persistent object IPExceptions[] {
                counted with IPExceptionsNumberOfEntries;

                %persistent %unique string Address {
                    on action validate call is_valid_ip_range;
                    on action validate call is_private_ip_range;
                    userflags %usersetting;
                }
            }

            /**
             * Domains that should not be protected against rebind attacks
             * @version 1.0
             */
            %persistent object DomainExceptions[] {
                counted with DomainExceptionsNumberOfEntries;

                %persistent %unique string Address{
                    userflags %usersetting;
                }
            }

            event Config;
        }
    }
}

%populate {
    on event "dm:instance-added" call rebind_changed
        filter 'path matches "^DNS\.[a-zA-Z0-9_-]*RebindProtection\."';
    on event "dm:instance-removed" call rebind_changed
        filter 'path matches "^DNS\.[a-zA-Z0-9_-]*RebindProtection\."';
    on event "dm:object-changed" call rebind_changed
        filter 'path matches "^DNS\.[a-zA-Z0-9_-]*RebindProtection\."';

    on event "Config" call rebind_config_event;
}
