%populate {
{% if (BDfn.hasAnyUpstream()) : %}
    object DNS {
        object Relay {
            object Forwarding {
                instance add("cellular-1") {
                    parameter Type = "3GPP-NAS";
                    parameter Interface = "Device.Logical.Interface.1.";
                    parameter Enable = true;
                }
                instance add("cellular-2") {
                    parameter Type = "3GPP-NAS";
                    parameter Interface = "Device.Logical.Interface.1.";
                    parameter Enable = true;
                }
            }
        }
    }
{% endif; %}
}
