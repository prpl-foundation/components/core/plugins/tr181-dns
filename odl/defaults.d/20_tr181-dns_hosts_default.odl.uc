%populate {
    object DNS {
        object '${prefix_}Host' {
{% let i=1 %}
{% for (let Bridge in BD.Bridges) : %}
{% i++ %}
            instance add("deviceinfo-{{lc(Bridge)}}") {
                parameter Interface = "Device.Logical.Interface.{{i}}.";
                parameter QueryAddresses = true;
            }
{% endfor %}
        }
    }
}
