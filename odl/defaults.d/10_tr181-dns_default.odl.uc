%populate {
    object DNS {
        object Relay {
            object '${prefix_}Config' {
{% if (BDfn.hasAnyUpstream()) : %}
{% let i=1 %}
{% for (let Bridge in BD.Bridges) : %}
{% i++ %}
                instance add({{lc(Bridge)}}) {
                    parameter Interface = "Device.Logical.Interface.{{i}}.";
                }
{% endfor; %}
{% endif; %}
                instance add("lo") {
                    parameter Interface = "${ip_intf_lo}";
                }
            }
            object Forwarding {
                instance add("dhcpv4-1") {
                    parameter Type = "DHCPv4";
                    parameter Interface = "Device.Logical.Interface.1.";
                    parameter Enable = true;
                }
                instance add("dhcpv4-2") {
                    parameter Type = "DHCPv4";
                    parameter Interface = "Device.Logical.Interface.1.";
                    parameter Enable = true;
                }
                instance add("dhcpv6-1") {
                    parameter Type = "DHCPv6";
                    parameter Interface = "Device.Logical.Interface.1.";
                    parameter Enable = true;
                }
                instance add("dhcpv6-2") {
                    parameter Type = "DHCPv6";
                    parameter Interface = "Device.Logical.Interface.1.";
                    parameter Enable = true;
                }
                instance add("ra-1") {
                    parameter Type = "RouterAdvertisement";
                    parameter Interface = "Device.Logical.Interface.1.";
                    parameter Enable = true;
                }
                instance add("ra-2") {
                    parameter Type = "RouterAdvertisement";
                    parameter Interface = "Device.Logical.Interface.1.";
                    parameter Enable = true;
                }
{% if (BDfn.hasAnyUpstream()) : %}
                instance add("ppp-1") {
                    parameter Type = "IPCP";
                    parameter Interface = "Device.Logical.Interface.1.";
                    parameter Enable = true;
                }
                instance add("ppp-2") {
                    parameter Type = "IPCP";
                    parameter Interface = "Device.Logical.Interface.1.";
                    parameter Enable = true;
                }
{% endif; %}
            }
        }
        object Client {
            object Server {
                instance add("lo") {
                    parameter Type = "Static";
                    parameter Interface = "${ip_intf_lo}";
                    parameter DNSServer = "127.0.0.1";
                    parameter Enable = true;
                }
            }
        }
    }
}
