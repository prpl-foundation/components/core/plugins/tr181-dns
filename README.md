[TOC]

# tr181-dns

The DNS manager with tr181 data model. It is made modular with the Ambiorix libamxm library.

## module configuration

>>>
Most of the module configurations are applied only at start of this plugin. Keep in mind the reboot persistency file located at `/etc/config/tr181-dns/odl/tr181-dns.odl`.
>>>

### Prefix for non standardized tr181 data model objects/parameters
Public non standardized objects and parameters must have a vondor prefix. This prefix is configurable in ODL:
```
%config {
    prefix_ = "X_PRPL-COM_";
}
```

All other non standardized objects and parameters will have ODL attribute `%protected`. They will be hidden and can be viewed with ubus-cli like this:
```
ubus-cli
> protected
> DNS.?
```

### DNS Controller

By default `mod-dns-uci` is used. [README](mod-dns-uci/README.md)<br>
<br>
The DNS controller is configurable in ODL:
```
%config {
    modules.dns-directory = "/usr/lib/amx/tr181-dns/modules";
    //modules.dns-options = {};
}

%populate {
    DNS.SupportedDNSControllers = "mod-dns-uci";
    DNS.DNSController = "mod-dns-uci";
}
```

### Failover Controller

A failover controller is optional. The DNS controller needs to be able to detect and report server failures. The failover controller can be configured in ODL:

```
%config {
    modules.dns-directory = "/usr/lib/amx/tr181-dns/modules";
    modules.failover-controller = "mod-dns-failover";
    //modules.failover-options = {};
}
```

Parameter `DNS.Relay.Forwarding.i.Tag` can give the failover algorithm extra information about the server.<br>
<br>
Parameter `DNS.Relay.Forwarding.i.State` can be used by the failover algorithm to report what it is doing with the server. E.g. Used, Error, Standby.

### Firewall controller

The plugin uses `mod-fw-amx` to open and close DNS ports.

```
%config {
    modules.fw-directory = "/usr/lib/amx/modules";
}

%populate {
    DNS.SupportedFWControllers = "mod-fw-amx";
    DNS.FWController = "mod-fw-amx";
}
```

## compile + install
```
# compile
make

# install
sudo make install
```

## (unit)tests
```
# run tests
make test
```

```mermaid
flowchart TB
    classDef todo fill:#FF6347
    classDef implemented fill:#7FFF00

    Start -.-> Client
    Client -.-> Relay
    Relay -.-> Hosts
    Hosts -.-> Config
    Config -.-> End

    subgraph Client[" "]
    direction LR
    A1[add/change/remove DNS.Client. instances]:::todo
    end

    subgraph Relay[" "]
    direction TB
    B1[Type Static] --> B11[Add DNS.Relay. instance]:::implemented
    B11 --> B12["Change DNS.Relay.{i}.Enable"]:::implemented
    B12 --> B13["Change DNS.Relay.{i}.DNSServer"]:::implemented
    B13 --> B14[Remove DNS.Relay. instance]:::implemented

    B2[Type DHCPv4] --1--> B21[Add DNS.Relay. instance]:::implemented
    B21 --2--> B21
    B21 --3--> B22[Change DNS.Relay.Enable]:::implemented
    B22 --4--> B21
    B21 --5--> B23[Remove DNS.Relay.3.]:::implemented
    B23 --> B24[Change DNS.Relay.1.Interface]:::implemented
    B24 --> B25["Change (netmodel) netdevname"]:::implemented
    B25 --> B26["Change (netmodel) dhcp option"]:::implemented

    B3[Type IPCP] --1--> B31[Add DNS.Relay. instance]:::implemented
    B31 --2 and 3--> B31
    B31 --4--> B32[Remove DNS.Relay.3.]:::implemented
    B32 --> B33["Change (netmodel) dnsservers"]:::implemented

    B4[Type RouterAdvertisement] --> B41[Add DNS.Relay. instance]:::implemented
    end

    subgraph Hosts[" "]
    direction TB
    C1[Firewall is open]:::implemented --> C2[Add DNS.Host. instance]:::implemented
    C2 --1--> C3["Add DNS.Host.{i}.IPAddress. instance"]:::implemented
    C3 --2--> C4["Change DNS.Host.{i}.Enable"]:::implemented
    C4 --> C5["Change DNS.Host.{i}.Name"]:::implemented
    C5 --> C6["Change DNS.Host.{i}.Interface"]:::implemented
    C6 --3--> C3
    C3 --4--> C7["Change DNS.Host.{i}.IPAddress.2.IPaddress"]:::implemented
    C7 --> C8["Remove DNS.Host.{i}.IPAddress.2. instance"]:::implemented
    C8 --> C9["Remove DNS.Host.{i}. instance"]:::implemented
    C9 --> C10["Add 2 DNS.Host.{i}. instances with same Interface"]:::implemented
    C10 --> C11[Firewall is closed]:::implemented
    end

    subgraph Config[" "]
    direction TB
    D1[Add DNS.Config. instance]:::implemented --> D2["Change DNS.Config.{i}.Interface"]:::implemented
    D2 --> D3["Change DNS.Config.{i}.CacheSize"]:::implemented
    D3 --> D4["Change DNS.Config.{i}.CacheTTLMax"]:::implemented
    D4 --> D5["Change DNS.Config.{i}.CacheTTLMin"]:::implemented
    D5 --> D6["Call DNS.Config.{i}.FlushCache()"]:::implemented
    D6 --> D7["Remove DNS.Config.{i}. instance"]:::implemented
    end
```