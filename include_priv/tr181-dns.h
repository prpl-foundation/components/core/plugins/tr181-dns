/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __TR181_DNS_H__
#define __TR181_DNS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <debug/sahtrace.h>
#include <netmodel/common_api.h>

#define STRING_EMPTY(X) ((X == NULL) || (*X == '\0'))

#define FREE(X) do { \
        free(X); \
        X = NULL; \
} while(0);

#define CLOSE_QUERY(X) do { \
        if(X != NULL) { \
            netmodel_closeQuery(X); \
            X = NULL; \
        } \
} while(0);

typedef enum {
    ENABLED,
    DISABLED,
    ERROR,
} status_t;

typedef enum {
    DNS_TYPE_ERROR = 0,
    DNS_STATIC,
    DNS_DHCPv4,
    DNS_DHCPv6,
    DNS_RA,
    DNS_IPCP,
    DNS_3GPP_NAS,
} dns_type_t;

int _tr181_dns_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);
amxd_dm_t* get_dm(void);
amxc_var_t* get_config(void);
const char* get_prefix(void);
bool supports_failover(void);
void set_failover_active(bool active);
const char* failover_shared_object_name(void);
void relay_enable_all(void);

// module 'core' handlers
int update_forwarding_status(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int server_failure(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int update_forwarding_state(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int core_set_server(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int core_add_server(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int core_remove_server(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int core_failover_enable_changed(const char* function_name, amxc_var_t* args, amxc_var_t* ret);

// module 'dns' handlers
bool enable_changed(bool is_enabled);
int set_server(amxc_var_t* args, amxc_var_t* ret, bool use_failover);
int add_server(amxc_var_t* args, amxc_var_t* ret, bool use_failover);
int remove_server(amxc_var_t* args, amxc_var_t* ret, bool use_failover);
int add_host(amxc_var_t* args, amxc_var_t* ret);
int remove_host(amxc_var_t* args, amxc_var_t* ret);
int remove_host_address(amxc_var_t* args, amxc_var_t* ret);
int set_options(const char* soname, const char* module, amxc_var_t* options);
void add_lan_interface(const char* interface);
void remove_lan_interface(const char* interface);
void set_cache(uint32_t cache_size, uint32_t ttl_max, uint32_t ttl_min);
bool flush_cache(void);
void set_domain_name(const char* interface, const char* domain_name);

// module 'fw' handlers
bool open_firewall(const char* interface, char** alias);
void close_firewall(const char* alias);

// helpers
const char* object_da_string(amxd_object_t* obj, const char* name);
void set_status(amxd_object_t* obj, status_t status);
void hosts_start(void);
bool private_server_info(amxc_var_t* args);
bool unset_param_persistency(amxd_param_t* param);
bool unset_object_persistency(amxd_object_t* obj);
int strcmp_safe(const char* a, const char* b);

#ifdef __cplusplus
}
#endif

#endif // __TR181_DNS_H__
