#!/bin/sh

. /usr/lib/amx/scripts/amx_init_functions.sh

name="tr181-dns"
datamodel_root="DNS"

case $1 in
    boot)
        process_boot ${name} -D
        ;;
    start)
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name}
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        process_debug_info ${datamodel_root}
        if [ -f /etc/unbound/unbound.conf ]; then
            printf "\ncat /etc/unbound/unbound.conf\n"
            cat /etc/unbound/unbound.conf
        fi
        printf "\nuci show dhcp\n"
        uci show dhcp
        for f in /var/etc/dnsmasq* /tmp/hosts/*; do
            printf "\ncat %s\n" "$f"
            cat "$f"
        done
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|restart]"
        ;;
esac
